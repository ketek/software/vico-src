# Changelog

All notable changes to the software components below will be documented in this file.

All VICO software tools are compatible with each other, if the main version number is identical. The installer is not affected by this rule.

## VICODaemon

### [2.4.0.0] - 2025-01-21

#### Added

- The source code has been adjusted to ensure the correct dependencies are loaded for the 32-bit version.
- Ensured `pkg-config` can locate `systemd` `.pc` files during cross-compilation of [QtService](https://gitlab.com/ketek/software/qtservice), especially in Docker environments, by adding symbolic links and `PKG_CONFIG_PATH` exports to the ARM compilation documentation.

#### Fixed

- The VICODaemon log file now correctly outputs the CRC of MCU datagrams at LogLevel `Debug`.
- Fixed a bug on Linux where the VICODaemon could not start automatically after using the installer and required a reboot. Now, the VICODaemon is active immediately after installation.

### [2.3.0.0] - 2024-08-23

#### Added

- Added handle `getDeviceIPFromDaemon` for new VICOLib command `getDaemonConnection(const CharType* serialNumber, const UInt8Type ipAddressSize, CharType* ipAddress)`.

### [2.2.0.0] - 2024-04-18

#### Added

- Added handle for new VICOLib command `removeDeviceConnection(const CharType* serialNumber)`.

#### Changed

- Maintenance: Updated from Qt 5.15.2 to Qt 6.5.2.
- Updated the ARM compilation documentation to support the compilation using Qt 6.5.2.

### [2.1.0.0] - 2023-11-21

#### Added

- `FT_SetVIDPID` used prior to `FT_Open` (Linux only)

#### Fixed

- Changed the `stop.sh` file format from DOS to UNIX (Linux only)
- Qt plugin path added to `run_service.sh` (Linux and ARM only)

### [2.0.0.0] - 2023-08-02

#### Added

- Extended logging with adjustable log level `NONE`, `ERROR`, `WARNING`, `INFO`, and `DEBUG`.

#### Changed

- Compression of data sent between local sockets to avoid data loss.

#### Fixed

- Connection stability (VICOScope stopping VICODaemon occasionally).
- Fixed preferred interface functionality.
- VICODaemon can run now with spaces within the directory path.

## VICOLib

### [2.3.2.0] - 2025-01-21

#### Changed

- Renamed argument `logLevel` to `level` in `setLogLevel` and `getLogLevel` of the Java and Python wrapper.
- Renamed argument `triggerDuration` to `triggerTime` in `calculateEventRate` of the Python wrapper.
- Renamed argument `length` to `len` in `flashRead`, `getEEP` and `setEEP` of the Python wrapper.

#### Fixed

- Renamed incorrect argument name `string` to `serialNumber` in `getFirmwareVersion` of the Java wrapper.
- Renamed incorrect argument name `peakingTime` to `gapTime` in `setSlowFilterGapTime` of the Python wrapper.
- Removed unnecessary argument `hostIpAddress` in `getHostIpAddress` of the Python wrapper.

### [2.3.1.0] - 2024-09-05

#### Fixed

- Fixed a bug that not the full range of values could be set when using the command `setStopCondition`.

#### Changed

- Added `VICO_COMMAND_NOT_SUPPORTED` as a possible return code of `getDaemonConnection`.

### [2.3.0.0] - 2024-08-23

#### Added

- Literal `VICO_COMMAND_NOT_SUPPORTED_BY_DEVICE` to enum `VICOStatusType`.
- Added API `VICOStatusType getDaemonConnection(const CharType* serialNumber, const UInt8Type ipAddressSize, CharType* ipAddress);` which reads the ethernet connection information of the VICODaemon.

#### Changed

- The rounding applied to `stopConditionValue` varies depending on the `stopConditionType`.

#### Removed

- Error handling in the API (`flashRead()`, `getEEP()`, `setEEP()`, and `twi()`) that is managed by the device itself has been removed.

### [2.2.0.0] - 2024-04-18

#### Added

- Added `VICOStatusType::VICO_COMMAND_NOT_SUPPORTED` that is returned whenever a command executed is not supported by the respective `VICODaemon` (from version `2.2.0.0` on).
- Added `VICOStatusType removeDeviceConnection(const CharType* serialNumber)` which, given a serial number, removes the connection from `VICODaemon`.

#### Changed

- Maintenance: Updated from Qt 5.15.2 to Qt 6.5.2.
- Updated the ARM compilation documentation to support the compilation using Qt 6.5.2.
- Updated the names of parameters in the Java wrapper to camel-case notation:
  - `LiveInfo1VICOType#getvIn` to `LiveInfo1VICOType#getVIn`
  - `LiveInfo2VICOType#getvIn` to `LiveInfo2VICOType#getVIn`
  - `LiveInfoBoundariesVICOType#getvInMin` to `LiveInfoBoundariesVICOType#getVInMin`
  - `LiveInfoBoundariesVICOType#getvInMax` to `LiveInfoBoundariesVICOType#getVInMax`
  - `LiveInfo2VIAMPType#isaRdy` to `LiveInfo2VIAMPType#isARdy`
  - `LiveInfo2VIAMPType#isiPartLimit` to `LiveInfo2VIAMPType#isIPartLimit`
  - `LiveInfo2VIAMPType#getpPart` to `LiveInfo2VIAMPType#getPPart`
  - `LiveInfo2VIAMPType#getiPart` to `LiveInfo2VIAMPType#getIPart`
  - `LiveInfo2VIAMPType#getdPart` to `LiveInfo2VIAMPType#getDPart`
  - `TempType#isaRdy` to `TempType#isARdy`
  - `DbgClpType#getpPart` to `DbgClpType#getPPart`
  - `DbgClpType#getiPart` to `DbgClpType#getIPart`
  - `DbgClpExtType#getpPart` to `DbgClpExtType#getPPart`
  - `DbgClpExtType#getiPart` to `DbgClpExtType#getIPart`
  - `DbgClpExtType#isiPartLimit` to `DbgClpExtType#isIPartLimit`
  - `DbgClpExtType#getdPart` to `DbgClpExtType#getDPart`
  - `DbgClpExtType#isaRdy` to `DbgClpExtType#isARdy`

#### Fixed

- Documentation errors within `VICOStatusType` fixed.

### [2.1.0.0] - 2023-11-21

#### Added

- Literal `CFT` to enum `ResetDetectionType`.
- Added API `setHostIpAddress(const CharType* deviceIpAddress, const CharType* hostIpAddress);` which sets the IP address of the interface to be bound during `scanTcp` and `scanUdp`.
- Added API `getHostIpAddress(const CharType* deviceIpAddress, const UInt8Type hostAddressSize, CharType* hostIpAddress);` IP address of the interface to be bound.

#### Changed

- Changed the element `BooleanType ardy;` to `BooleanType aRdy;` of the structs `LiveInfo2VIAMPType` and `TempType`.
- Changed API `scanTcpDevices(const CharType* fromIpAddress, const CharType* toIpAddress);` to `scanTcpDevices(const CharType* fromIpAddress, const CharType* toIpAddress, const BooleanType useBinding = false);` to optionally bind the interface host address (see `setHostIpAddress`) before performing the TCP Scan.
- Changed API `scanUdpDevices(const CharType* netaddress, const UInt8Type netmask);` to `scanUdpDevices(const CharType* netaddress, const UInt8Type netmask, const BooleanType useBinding = false);` to optionally bind the interface host address (see `setHostIpAddress`) before performing the UDP Scan.

#### Fixed

- Documentation extended and errors fixed.

### [2.0.0.0] - 2023-08-02

#### Added

- `setLogLevel` and `getLogLevel` API.
- Literal `VPERR_07` to enum `LiveInfoVIAMPErrorType`.
- Literal `MCU_GETADC_MUTEX_NOT_AVAILABLE` to enum `MCUStatusType`.
- Added `setLogLevel` and `getLogLevel` API for Java and Python wrapper.

#### Changed

- Decompression of data received from VICODaemon to avoid data loss.
- Examples updated according to current VICOLib version.

#### Fixed

- Python wrapper `scanUdpDevices` fix.

