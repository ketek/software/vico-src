#include "EthConnector.h"


EthConnector::EthConnector(QAbstractSocket* socket, QObject *parent) : QObject(parent) {
	this->socket = socket;
	if (socket->socketType() == QAbstractSocket::TcpSocket) {
		this->connectionType = INTERFACE_ETHERNET_TCP;
	}
	else if (socket->socketType() == QAbstractSocket::UdpSocket) {
		this->connectionType = INTERFACE_ETHERNET_UDP;
	}
	else {
		this->connectionType = INTERFACE_UNKNOWN;
	}

	initializeConnection();
}

EthConnector::~EthConnector() {

}

void EthConnector::closeConnection() {
	shutdown();
	if(socket) {
		connect(this, &QObject::destroyed, this, [] {
		});
	}
	socket->deleteLater();
}

QHostAddress EthConnector::getPeerAddress() {
	if(socket) {
		return socket->peerAddress();
	} else {
		return QHostAddress();
	}
}

QAbstractSocket *EthConnector::getSocket() const {
	return socket;
}

void EthConnector::sendMsg(const QByteArray& msg, const quint16, const int &) {
	// Size is unused here. We could use it to get a complete eth datastream, but we implement it in QueryServer. Size is only needed in USBConnector

	// Advanced TCP OUT logging
	QString msgOut (QStringLiteral("[DEV]\t[REQ]\t["));
	for (int i = 0; i < msg.size(); ++i) {
		msgOut.append(QString::number((quint8)msg[i]));
		if (i != msg.size()-1) {
			msgOut.append(QStringLiteral(","));
		}
	}
	msgOut.append(QStringLiteral("]"));
	qDebug().noquote() << msgOut;

	socket->write(msg);
	socket->flush();
}

// This function will be called once QIODevice::readyRead signal is emitted, then emits msgReceived
void EthConnector::readMsg() {
	QByteArray msg = socket->readAll();


	emit msgReceived(msg);
}

InterfaceType EthConnector::getConnectionType() const {
	return this->connectionType;
}

void EthConnector::initializeConnection() {
	in.setDevice(socket);
	in.setVersion(QDataStream::Qt_5_12);
	in.setByteOrder((QDataStream::BigEndian));
	connect(socket, &QIODevice::readyRead, this, &EthConnector::readMsg);
	connect(socket, &QAbstractSocket::disconnected, this, &EthConnector::onSocketDisconnected);
}

void EthConnector::closeConnection(QAbstractSocket *socket) {
	socket->disconnect();
	socket->close();
	socket->abort();
	if(socket->state() == QAbstractSocket::SocketState::ConnectedState) {
		QEventLoop eventLoop;
		connect(socket, &QAbstractSocket::disconnected, &eventLoop, &QEventLoop::quit);
		socket->disconnectFromHost();
		eventLoop.exec();
	}
}

void EthConnector::shutdown() {
	closeConnection(this->socket);
}

void EthConnector::onSocketDisconnected() {
	emit disconnected();
}
