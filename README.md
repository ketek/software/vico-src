# VICO-src

VICOLib and VICODaemon are required to control KETEK devices within your specific application. The source code can be used to compile for any target device.

## Documentation

A detailed documentation for the VICOLib (incl. VICODaemon) and an example compilation for an ARM system can be found in the doc folder.

## Releases

Select **Deploy > Releases** on the left sidebar to access pre-compiled ARM binaries and their respective changelog.

The most recent release can be found [here](https://gitlab.com/ketek/software/vico-src/-/releases/permalink/latest).

## Support

If you have questions, feedback or encounter any problems regarding this repository, please do not hesitate to contact us via e-mail or phone:

KETEK GmbH  
Hofer Str. 3  
81737 Munich  
Germany

Phone: +49 (0) 89 673467 70  
E-Mail: <info@ketek.net>  
Homepage: [www.ketek.net](https://www.ketek.net)
