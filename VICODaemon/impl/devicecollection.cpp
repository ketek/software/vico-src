#include "devicecollection.h"
#include "DeviceRegistry.h"

DeviceCollection::DeviceCollection() {

}

DeviceCollection::~DeviceCollection() {
	removeDevices();
}

void DeviceCollection::addDevice(VicoDevice *device) {
	if(device && device->getConnector()) {
		InterfaceType interfaceType = device->getConnector()->getConnectionType();
		interfaceToDevices.value(interfaceType)->append(device);
	}
}

void DeviceCollection::removeDevice(VicoDevice *device) {
	if(device) {
		QList<InterfaceType> keys = interfaceToDevices.keys();
		for(const InterfaceType &interfaceType : qAsConst(keys)) {
			if(interfaceToDevices.value(interfaceType)->contains(device)) {
				interfaceToDevices.value(interfaceType)->removeAll(device);
				removeConnection(device->getConnector());
				device->setConnector(nullptr);
			}
		}
		device->deleteLater();
	}
}

void DeviceCollection::removeDeviceWithInterface(VicoDevice *device, const InterfaceType interfaceType) {
	if(device) {
		QVector<VicoDevice*> *devices = interfaceToDevices.value(interfaceType);
		for(VicoDevice *connectedDevice : *devices) {
			if(connectedDevice->getDeviceId() == device->getDeviceId()) {
				interfaceToDevices.value(interfaceType)->removeAll(device);
				removeConnection(device->getConnector());
				device->setConnector(nullptr);
			}
		}
		device->deleteLater();
	}
}

void DeviceCollection::removeDevices(const quint64 deviceId) {
	QVector<VicoDevice*> devicesToBeRemoved = getDevices(deviceId);
	for(VicoDevice *device : qAsConst(devicesToBeRemoved)) {
		removeDevice(device);
	}
}

void DeviceCollection::removeDevices(const QVector<InterfaceType> interfaceTypes) {
	QVector<VicoDevice*> devicesToBeRemoved = getDevices(interfaceTypes);
	for(VicoDevice *device : qAsConst(devicesToBeRemoved)) {
		removeDevice(device);
	}
}

bool DeviceCollection::contains(const quint64 deviceId) const {
	return getSerialNumbers().contains(deviceId);
}

bool DeviceCollection::contains(VicoDevice *device) const {
	if(!device) return false;
	QList<InterfaceType> keys = interfaceToDevices.keys();
	for(const InterfaceType &interfaceType : qAsConst(keys)) {
		if(interfaceToDevices.value(interfaceType)->contains(device)) {
			return true;
		}
	}
	return false;
}

const QVector<quint64> DeviceCollection::getSerialNumbers() const {
	QSet<quint64> serialNumbers;
	QList<InterfaceType> keys = interfaceToDevices.keys();
	for(const InterfaceType &interfaceType : qAsConst(keys)) {
		QVector<VicoDevice*> *devices = interfaceToDevices.value(interfaceType);
		for(VicoDevice *device : *devices) {
			serialNumbers.insert(DeviceRegistry::convertDeviceID(device->getDeviceId()));
		}
	}
	return serialNumbers.values().toVector();
}

const QVector<VicoDevice*> DeviceCollection::getDevices(const QVector<InterfaceType> interfaceTypes) const {
	QVector<InterfaceType> keys;
	if(interfaceTypes.empty()) {
		// get all
		keys = interfaceToDevices.keys().toVector();
	} else {
		keys = interfaceTypes;
	}
	QVector<VicoDevice*> devicesOfInterface;
	for(const InterfaceType &interfaceType : qAsConst(keys)) {
		QVector<VicoDevice*> *devices = interfaceToDevices.value(interfaceType);
		devicesOfInterface.append(*devices);
	}
	return devicesOfInterface;
}

VicoDevice* DeviceCollection::getDevice(const quint64 deviceId, const InterfaceType interfaceType) const {
	const QVector<VicoDevice*> devices = getDevices(deviceId);

	if(devices.isEmpty()) {
		return nullptr;
	} else {
		for(VicoDevice *device : devices) {
			if(device->getConnector() && device->getConnector()->getConnectionType() == interfaceType) {
				return device;
			}
		}
		return devices.first();
	}
}

const QVector<VicoDevice *> DeviceCollection::getDevices(const quint64 deviceId) const {
	QVector<VicoDevice*> devicesOfId;
	QList<InterfaceType> keys = interfaceToDevices.keys();
	for(const InterfaceType &interfaceType : qAsConst(keys)) {
		QVector<VicoDevice*> *devices = interfaceToDevices.value(interfaceType);
		for(VicoDevice *device : *devices) {
			if(DeviceRegistry::convertDeviceID(device->getDeviceId()) == deviceId) {
				devicesOfId.append(device);
			}
		}
	}
	return devicesOfId;
}

VicoDevice *DeviceCollection::getTCPConnectedDevice(const quint32 ip) const {
	for(VicoDevice *device : tcpDevices) {
		if(EthConnector *ethConn = dynamic_cast<EthConnector*>(device->getConnector())) {
			if (ethConn->getPeerAddress().toIPv4Address() == 0) {
				DeviceRegistry::getInstance().removeDeviceWithInterface(device, INTERFACE_ETHERNET_TCP);
			}

			if(ethConn->getPeerAddress().toIPv4Address() == ip) {
				return device;
			}
		}
	}
	return nullptr;

}

QHostAddress DeviceCollection::getDeviceIPFromDaemon(const QString serialNumber, VICOStatusType *statusCode) const {
	const QVector<VicoDevice*> devices = getDevices();
	for(VicoDevice *device : devices) {
		if (serialNumber != device->getDeviceId()) {
			continue;
		} else if (device->getDeviceType() == VICO_AV) {
			*statusCode = VICO_COMMAND_NOT_SUPPORTED_BY_DEVICE;
			return QHostAddress::Null;
		} else if(EthConnector *ethConn = dynamic_cast<EthConnector*>(device->getConnector())) {
			QHostAddress deviceIP = ethConn->getPeerAddress();
			return QHostAddress(deviceIP.toIPv4Address());
		} else {
			// DeviceID connected but not via Ethernet Interface
			continue;
		}
	}
	*statusCode = VICO_NO_DEVICE_WITH_ID_CONNECTED_ERROR;
	return QHostAddress::Null;

}

void DeviceCollection::removeConnection(IConnector *connector) {
	if(connector) {
		connector->closeConnection();
		if(EthConnector *ethConnector = dynamic_cast<EthConnector*>(connector)) {
			ethConnector->deleteLater();
		} else if(UsbConnector *usbConnector = dynamic_cast<UsbConnector*>(connector)) {
			usbConnector->deleteLater();
		}else if(UsbHidConnector *usbHidConnector = dynamic_cast<UsbHidConnector*>(connector)) {
			usbHidConnector->deleteLater();
		} else {
			delete connector;
		}
	}
}

HostAddresses::~HostAddresses() {
	QList<Host*> hosts = hostAddresses.values();
	while(!hosts.isEmpty()) {
		delete hosts.takeFirst();
	}
}

void HostAddresses::insertHostAddress(const quint32 &deviceIpAddress, const quint32 &hostIpAddress, const bool &weakCache) {
	Host *host = hostAddresses.value(deviceIpAddress, nullptr);
	// if the saved address is weak and the inserted one is weak, replace the old one with the new one
	if(host && host->weakCache && weakCache) {
		host->hostAddress = hostIpAddress;
	}
	// if the saved address is weak and the inserted one is strong, replace the old one with the new one
	else if(host && host->weakCache && !weakCache) {
		host->hostAddress = hostIpAddress;
		host->weakCache = false;
	}
	// if the saved address is weak and the inserted one is strong, replace the old one with the new one
	else if(host && !host->weakCache && !weakCache) {
		host->hostAddress = hostIpAddress;
		host->weakCache = false;
	}
	else if(!host) {
		host = new Host;
		host->hostAddress = hostIpAddress;
		host->weakCache = weakCache;
		hostAddresses.insert(deviceIpAddress, host);
	}
}

QHostAddress HostAddresses::getHostAddress(const quint32 &subnet, const int &netmask) const {
	QHostAddress subnetAddress(subnet);
	for(const quint32 &ip : hostAddresses.keys()) {
		if(QHostAddress(ip).isInSubnet(subnetAddress, netmask)) {
			return getHostAddress(ip);
		}
	}
	return QHostAddress();
}

QHostAddress HostAddresses::getHostAddress(const quint32 &deviceIpAddress) const {
	Host *host = hostAddresses.value(deviceIpAddress, nullptr);
	if(host) {
		return QHostAddress(host->hostAddress);
	} else {
		return QHostAddress();
	}
}

void HostAddresses::removeHostAddress(const quint32 &deviceIpAddress) {
	if(hostAddresses.contains(deviceIpAddress)) {
		Host *host = hostAddresses.take(deviceIpAddress);
		delete host;
	}
}
