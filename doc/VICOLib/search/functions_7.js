var searchData=
[
  ['isanaloghardwarepowerdownenabled_0',['isAnalogHardwarePowerdownEnabled',['../a00002.html#af687b04d423c70f46bd87abcaf06b5e2',1,'vicolib.h']]],
  ['isbaselinecorrectionenabled_1',['isBaselineCorrectionEnabled',['../a00002.html#a2836af4cab338f5a7bfe7a83015cf674',1,'vicolib.h']]],
  ['isdynamicresetenabled_2',['isDynamicResetEnabled',['../a00002.html#a056a871b567580a2995e6d2f6c911c79',1,'vicolib.h']]],
  ['isethernetpowerdownenabled_3',['isEthernetPowerdownEnabled',['../a00002.html#a13f2308a98455c343a13b874ccf870dc',1,'vicolib.h']]],
  ['ismediumfilterpulsedetectionenabled_4',['isMediumFilterPulseDetectionEnabled',['../a00002.html#ab6e4d27f5d1b63d45fcd18a375dfacad',1,'vicolib.h']]],
  ['isspipowerdownenabled_5',['isSPIPowerdownEnabled',['../a00002.html#a5fc6a95d4e620bea0f06e1d27ededa07',1,'vicolib.h']]],
  ['isusbpowerdownenabled_6',['isUSBPowerdownEnabled',['../a00002.html#a55bfe38f61c9eecfc827a7c8ded638c5',1,'vicolib.h']]]
];
