#!/bin/bash

BASE_DIR=$(dirname "$(readlink -f "$0")")

sed -i -e "s|PATH_TO_VICODAEMON|${BASE_DIR}/usr/bin|g" "${BASE_DIR}/usr/bin/run_service.sh"
sed -i -e "s|PATH_TO_VICODAEMON|${BASE_DIR}/usr/bin/run_service.sh|g" "${BASE_DIR}/usr/service/VICODaemon.service"

chmod +x "${BASE_DIR}/usr/bin/run_service.sh"

cp "${BASE_DIR}/usr/service/VICODaemon.service" /etc/systemd/system/.

cp "${BASE_DIR}/usr/service/VICODaemon.systemd-service.conf" /etc/dbus-1/system.d/.

# reload all the services in case the daemon was already installed
systemctl daemon-reload

# restart the daemon service
systemctl restart VICODaemon

# enable daemon autostart at startup
systemctl enable VICODaemon
