#ifndef ICONNECTOR_H
#define ICONNECTOR_H

#include <QtPlugin>
#include <QtNetwork>
#include "types.h"
#include "queryconstants.h"

/**
 * @brief The IConnector class is used as common interface for common connector implementations.
 */
class IConnector {
public:
	virtual ~IConnector() = default;
	virtual InterfaceType getConnectionType() const = 0;
	virtual void closeConnection() = 0;
	virtual void initializeConnection() = 0;

signals:
	virtual void msgReceived(const QByteArray& msg) = 0;
public slots:
	virtual void sendMsg(const QByteArray& msg, const quint16 size, const int &timeout = DEFAULT_TIMEOUT) = 0;

};

#define IConnector_Id "connectors"

Q_DECLARE_INTERFACE(IConnector, IConnector_Id)

#endif // ICONNECTOR_H
