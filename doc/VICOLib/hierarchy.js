var hierarchy =
[
    [ "DbgClpExtType", "a00082.html", null ],
    [ "DbgClpType", "a00078.html", null ],
    [ "DevInfo1BootloaderType", "a00070.html", null ],
    [ "DevInfo1VICOType", "a00066.html", null ],
    [ "FirmwareVersionType", "a00034.html", null ],
    [ "LiveInfo1VIAMPType", "a00054.html", null ],
    [ "LiveInfo1VICOType", "a00042.html", null ],
    [ "LiveInfo2VIAMPType", "a00058.html", null ],
    [ "LiveInfo2VICOType", "a00046.html", null ],
    [ "LiveInfoBoundariesVIAMPType", "a00062.html", null ],
    [ "LiveInfoBoundariesVICOType", "a00050.html", null ],
    [ "MCUStatusInfoType", "a00038.html", null ],
    [ "QObject", null, [
      [ "VicoQuery", "a00086.html", null ]
    ] ],
    [ "RunStatisticsType", "a00030.html", null ],
    [ "TempType", "a00074.html", null ],
    [ "VersionType", "a00026.html", null ]
];