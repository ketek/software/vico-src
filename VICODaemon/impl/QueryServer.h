#ifndef QUERYSERVER_H
#define QUERYSERVER_H

#include <QtNetwork>
#include "queryconstants.h"
#include "ClientQuery.h"
#include "VicoDevice.h"
#include "DeviceRegistry.h"
#include "UdpScan.h"
#include "TcpScan.h"
#include "usbscan.h"
#include "UsbHidScan.h"
#include "mutexhandler.h"

/**
 * @brief The TimeoutThread class forces timeouts in case the target device didn't respond via the active connection.
 */
class TimeoutThread : public QThread {
	Q_OBJECT
public:
	TimeoutThread(QObject * parent = nullptr);
	static qint64 MUTEX_LOCK_TIMEOUT; // in s

signals:
	void timeout(QString id);
	void forceStop();

public slots:
	void onMutexLocked(QString id);
	void onMutexUnlocked(QString id);
private slots:
	void onForceStop();

protected:
	void run() override;

private:

	QMap<QString, qint64> timeouts;
	QMutex lock;
	QMutex stopLock;
	bool stop = false;

};

/**
 * @brief The QueryServer class contains the request/response logic for all datagrams, including DPP and MCU standard- and specialcase datagrams.
 */
class QueryServer : public QLocalServer {

	Q_OBJECT
	Q_DISABLE_COPY(QueryServer)

public:
	explicit QueryServer(QObject *parent = nullptr);
	~QueryServer();
	void setup();
	void setUdpScan(UdpScan* udpScan);
	void setTcpScan(TcpScan* tcpScan);
	void setUsbScan(UsbScan* usbScan);
	void setUsbHidScan(UsbHidScan* usbHidScan);

protected:
	void incomingConnection(quintptr socketDescriptor) override;

signals:
	void logMessage(const QString &msg);

	void mutexLocked(QString id);
	void mutexUnlocked(QString id);

public slots:
	void stopServer();
	void getVicoReply(VicoDevice*, const QByteArray& msg);

private slots:
	void queryReceived(ClientQuery *query, const QJsonObject &json);
	void clientDisconnected(ClientQuery *client);
	void clientError(ClientQuery *client);
	void onTimeout(QString id);


private:
	enum RequestType {
		StopTCP,
		StopUDP,
		ScanTCP,
		ScanUDP,
		ScanUSB,
		ScanUSBHID,
		NumberOfDevices,
		GetPreferredInterface,
		SetPreferredInterface,
		GetLogLevel,
		SetLogLevel,
		GetHostIpAddress,
		SetHostIpAddress,
		RefreshDeviceConnections,
		DeviceByIndex,
		DaemonVersion,
        RemoveDeviceConnection,
		getDaemonConnection,
		Datagram,
		MCUDatagram,
		Other
	};

	QString getDeviceId(const QJsonObject &json) const;

	bool lockMutex(QString id, bool global = false, int tryTimeout = 2000);
	void unlockMutex(QString id, bool global = false);
	void handleStopTCPQuery(ClientQuery *query);
	void handleStopUDPQuery(ClientQuery *query);
	void handleScanTCPQuery(ClientQuery *query, const QJsonObject &json);
	void handleScanUDPQuery(ClientQuery *query, const QJsonObject &json);
	void handleScanUSBQuery(ClientQuery *query, const QJsonObject &json);
	void handleScanUSBHIDQuery(ClientQuery *query, const QJsonObject &json);
	void handleNumberOfDevices(ClientQuery *query);
	void handleGetPreferredInterface(ClientQuery *query, const QJsonObject &json);
	void handleSetPreferredInterface(ClientQuery *query, const QJsonObject &json);
	void handleGetLogLevel(ClientQuery *query);
	void handleSetLogLevel(ClientQuery *query, const QJsonObject &json);
	void handleGetHostIpAddress(ClientQuery *query, const QJsonObject &json);
	void handleSetHostIpAddress(ClientQuery *query, const QJsonObject &json);
	void handleRefreshDeviceConnections(ClientQuery *query);
	void handleDeviceByIndex(ClientQuery *query, const QJsonObject &json);
	void handleDaemonVersion(ClientQuery *query);
	void handleDatagramQuery(ClientQuery *query, const QJsonObject &json);
	void handleMCUDatagramQuery(ClientQuery *query, const QJsonObject &json);
	void logMsg(const QByteArray& msg);
    void handleRemoveDevice(ClientQuery *query, const QJsonObject &json);
	void handlegetDaemonConnection(ClientQuery *query, const QJsonObject &json);
	quint16 crc16(const char *data, quint8 offset, quint8 length) const;
	uint getMCUDatagramResponseSize(const QJsonObject &json, bool usePassThrough) const ;
	RequestType getRequestType(const QJsonObject &json) const;
	QVariant getRequestAPILog(const QJsonObject &json) const;
	MutexHandler mutexHandler;
	QVector<ClientQuery *> m_clients;
	QMap<VicoDevice *, ClientQuery *> queryToDeviceMap;
	QMap<ClientQuery *, int> queryToRequestMap;
	UdpScan* udpscan;
	TcpScan* tcpscan;
	QMap<ClientQuery*, int> requestedSize;
	QByteArray* responseData;
	TimeoutThread timeoutThread;
	UsbScan* usbscan;
	UsbHidScan* usbhidscan;
	QEventLoop *loop;
};

#endif // QUERYSERVER_H
