#!/bin/bash

# ARM compilation VICODaemon and VICOLib

# Prerequisite
# Update apt repositories
apt update
# Add architecture
## for arm64
dpkg --add-architecture arm64
## for armel
dpkg --add-architecture armel
## for armhf
dpkg --add-architecture armhf

# Add common libraries for cross-compilation
apt update && apt install -y pkg-config libncurses5-dev build-essential bison flex libssl-dev bc python-is-python3 python3-pip curl git cmake ninja-build
# Install cross-compiler
## for arm64
apt install -y libc6-arm64-cross libc6-dev-arm64-cross binutils-aarch64-linux-gnu gcc-aarch64-linux-gnu g++-aarch64-linux-gnu
## for armel
apt install -y libc6-armel-cross libc6-dev-armel-cross binutils-arm-linux-gnueabi gcc-arm-linux-gnueabi g++-arm-linux-gnueabi
## for armhf
apt install -y libc6-armhf-cross libc6-dev-armhf-cross binutils-arm-linux-gnueabihf gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf

# VICO-src
# Get the source files
## for arm64
git clone https://gitlab.com/ketek/software/VICO-src.git VICO-src_arm64
## for armel
git clone https://gitlab.com/ketek/software/VICO-src.git VICO-src_armel
## for armhf
git clone https://gitlab.com/ketek/software/VICO-src.git VICO-src_armhf

# Qt compilation
## for arm64, armel and armhf
### Choose the most suitable mirror in https://download.qt.io/archive/qt/6.5/6.5.2/single/qt-everywhere-src-6.5.2.tar.xz.mirrorlist
curl -O https://ftp.fau.de/qtproject/archive/qt/6.5/6.5.2/single/qt-everywhere-src-6.5.2.tar.xz
### Extract the file
tar -xf qt-everywhere-src-6.5.2.tar.xz
### Remove archive
rm qt-everywhere-src-6.5.2.tar.xz
### Go into the extracted directory
cd qt-everywhere-src-6.5.2
## for armhf only
### Copy the directory `qtbase/mkspecs/linux-arm-gnueabi-g++` into the new armhf configuration directory `qtbase/mkspecs/linux-arm-gnueabihf-g++`
cp -r qtbase/mkspecs/linux-arm-gnueabi-g++ qtbase/mkspecs/linux-arm-gnueabihf-g++
### Modify the contents of the new `qtbase/mkspecs/linux-arm-gnueabihf-g++/qmake.conf` file to use the armhf cross-compiler
sed -i -e "s|arm-linux-gnueabi-|arm-linux-gnueabihf-|" qtbase/mkspecs/linux-arm-gnueabihf-g++/qmake.conf
### Change directory
cd ..

# Qt compilation
## for x86_64
### Copy the extracted directory
cp -r qt-everywhere-src-6.5.2 qt-everywhere-src-6.5.2_x86_64
### Go into the copied directory
cd qt-everywhere-src-6.5.2_x86_64
### Configuration
./configure \
    -prefix /opt/x86_64/qt6 \
    -skip qt3d -skip qt5compat -skip qtactiveqt -skip qtcharts -skip qtcoap -skip qtconnectivity \
    -skip qtdatavis3d -skip qtdeclarative -skip qtdoc -skip qtgrpc -skip qthttpserver -skip qtimageformats \
    -skip qtlanguageserver -skip qtlocation -skip qtlottie -skip qtmqtt -skip qtmultimedia \
    -skip qtnetworkauth -skip qtopcua -skip qtpositioning -skip qtquick3d -skip qtquick3dphysics \
    -skip qtquickeffectmaker -skip qtquicktimeline -skip qtremoteobjects -skip qtscxml -skip qtsensors \
    -skip qtserialbus -skip qtserialport -skip qtshadertools -skip qtspeech -skip qtsvg -skip qtvirtualkeyboard \
    -skip qtwayland -skip qtwebchannel -skip qtwebengine -skip qtwebsockets -skip qtwebview -no-gui \
    -no-opengl -confirm-license
### After configuration is done, make and install
cmake --build . --parallel && cmake --install .
### Change directory
cd ..
### Remove Qt source
rm -r qt-everywhere-src-6.5.2_x86_64
## for arm64
### Copy the extracted directory
cp -r qt-everywhere-src-6.5.2 qt-everywhere-src-6.5.2_arm64
### Go into the copied directory
cd qt-everywhere-src-6.5.2_arm64
### Configuration
./configure -prefix /opt/aarch64-linux-gnu/qt6 -release -nomake tests -nomake examples \
    -qt-host-path /opt/x86_64/qt6 \
    -skip qt3d -skip qt5compat -skip qtactiveqt -skip qtcharts -skip qtcoap -skip qtconnectivity \
    -skip qtdatavis3d -skip qtdeclarative -skip qtdoc -skip qtgrpc -skip qthttpserver -skip qtimageformats \
    -skip qtlanguageserver -skip qtlocation -skip qtlottie -skip qtmqtt -skip qtmultimedia \
    -skip qtnetworkauth -skip qtopcua -skip qtpositioning -skip qtquick3d -skip qtquick3dphysics \
    -skip qtquickeffectmaker -skip qtquicktimeline -skip qtremoteobjects -skip qtscxml -skip qtsensors \
    -skip qtserialbus -skip qtserialport -skip qtshadertools -skip qtspeech -skip qtsvg -skip qtvirtualkeyboard \
    -skip qtwayland -skip qtwebchannel -skip qtwebengine -skip qtwebsockets -skip qtwebview -no-gui \
    -no-opengl -confirm-license \
    -- -DCMAKE_TOOLCHAIN_FILE=../VICO-src_arm64/deploy/toolchains/toolchain_arm64.cmake
### After configuration is done, make and install
cmake --build . --parallel && cmake --install .
### Change directory
cd ..
### Remove Qt source
rm -r qt-everywhere-src-6.5.2_arm64
## for armel
### Copy the extracted directory
cp -r qt-everywhere-src-6.5.2 qt-everywhere-src-6.5.2_armel
### Go into the copied directory
cd qt-everywhere-src-6.5.2_armel
### Configuration
./configure -prefix /opt/arm-linux-gnueabi/qt6 -release -nomake tests -nomake examples \
    -qt-host-path /opt/x86_64/qt6 \
    -skip qt3d -skip qt5compat -skip qtactiveqt -skip qtcharts -skip qtcoap -skip qtconnectivity \
    -skip qtdatavis3d -skip qtdeclarative -skip qtdoc -skip qtgrpc -skip qthttpserver -skip qtimageformats \
    -skip qtlanguageserver -skip qtlocation -skip qtlottie -skip qtmqtt -skip qtmultimedia \
    -skip qtnetworkauth -skip qtopcua -skip qtpositioning -skip qtquick3d -skip qtquick3dphysics \
    -skip qtquickeffectmaker -skip qtquicktimeline -skip qtremoteobjects -skip qtscxml -skip qtsensors \
    -skip qtserialbus -skip qtserialport -skip qtshadertools -skip qtspeech -skip qtsvg -skip qtvirtualkeyboard \
    -skip qtwayland -skip qtwebchannel -skip qtwebengine -skip qtwebsockets -skip qtwebview -no-gui \
    -no-opengl -confirm-license \
    -- -DCMAKE_TOOLCHAIN_FILE=../VICO-src_arm64/deploy/toolchains/toolchain_armel.cmake
### After configuration is done, make and install
cmake --build . --parallel && cmake --install .
### Change directory
cd ..
### Remove Qt source
rm -r qt-everywhere-src-6.5.2_armel
## for armhf
### Copy the extracted directory
cp -r qt-everywhere-src-6.5.2 qt-everywhere-src-6.5.2_armhf
### Go into the copied directory
cd qt-everywhere-src-6.5.2_armhf
### Configuration
./configure -prefix /opt/arm-linux-gnueabihf/qt6 -release -nomake tests -nomake examples \
    -qt-host-path /opt/x86_64/qt6 \
    -skip qt3d -skip qt5compat -skip qtactiveqt -skip qtcharts -skip qtcoap -skip qtconnectivity \
    -skip qtdatavis3d -skip qtdeclarative -skip qtdoc -skip qtgrpc -skip qthttpserver -skip qtimageformats \
    -skip qtlanguageserver -skip qtlocation -skip qtlottie -skip qtmqtt -skip qtmultimedia \
    -skip qtnetworkauth -skip qtopcua -skip qtpositioning -skip qtquick3d -skip qtquick3dphysics \
    -skip qtquickeffectmaker -skip qtquicktimeline -skip qtremoteobjects -skip qtscxml -skip qtsensors \
    -skip qtserialbus -skip qtserialport -skip qtshadertools -skip qtspeech -skip qtsvg -skip qtvirtualkeyboard \
    -skip qtwayland -skip qtwebchannel -skip qtwebengine -skip qtwebsockets -skip qtwebview -no-gui \
    -no-opengl -confirm-license \
    -- -DCMAKE_TOOLCHAIN_FILE=../VICO-src_arm64/deploy/toolchains/toolchain_armhf.cmake
### After configuration is done, make and install
cmake --build . --parallel && cmake --install .
### Change directory
cd ..
### Remove Qt source
rm -r qt-everywhere-src-6.5.2_armhf
# Remove Qt source
rm -r qt-everywhere-src-6.5.2

# QtService
# Install libsystemd-dev in order to include systemd service backend in QtService
## for arm64
apt install -y libsystemd-dev:arm64
## for armel
apt install -y libsystemd-dev:armel
## for armhf
apt install -y libsystemd-dev:armhf
# Ensure pkg-config can find systemd .pc files (needed in Docker)
## for arm64
ln -sf /usr/lib/aarch64-linux-gnu/pkgconfig/libsystemd.pc /usr/lib/aarch64-linux-gnu/pkgconfig/systemd.pc
export PKG_CONFIG_PATH=/usr/lib/aarch64-linux-gnu/pkgconfig:${PKG_CONFIG_PATH}
## for armel
ln -sf /usr/lib/arm-linux-gnueabi/pkgconfig/libsystemd.pc /usr/lib/arm-linux-gnueabi/pkgconfig/systemd.pc
export PKG_CONFIG_PATH=/usr/lib/arm-linux-gnueabi/pkgconfig:${PKG_CONFIG_PATH}
## for armhf
ln -sf /usr/lib/arm-linux-gnueabihf/pkgconfig/libsystemd.pc /usr/lib/arm-linux-gnueabihf/pkgconfig/systemd.pc
export PKG_CONFIG_PATH=/usr/lib/arm-linux-gnueabihf/pkgconfig:${PKG_CONFIG_PATH}
# Add x86_64 paths (if needed)
export PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig:${PKG_CONFIG_PATH}
# Clone the QtService repository
## for arm64
git clone --depth 1 --branch master https://gitlab.com/ketek/software/qtservice.git QtService_arm64
## for armel
git clone --depth 1 --branch master https://gitlab.com/ketek/software/qtservice.git QtService_armel
## for armhf
git clone --depth 1 --branch master https://gitlab.com/ketek/software/qtservice.git QtService_armhf
# Following the documentation located in the repository: run cmake and install QtService
## for arm64
cd QtService_arm64 && /opt/aarch64-linux-gnu/qt6/bin/qt-cmake -S . -B . && cmake --build . --parallel && cmake --install . && cd ..
## for armel
cd QtService_armel && /opt/arm-linux-gnueabi/qt6/bin/qt-cmake -S . -B . && cmake --build . --parallel && cmake --install . && cd ..
## for armhf
cd QtService_armhf && /opt/arm-linux-gnueabihf/qt6/bin/qt-cmake -S . -B . && cmake --build . --parallel && cmake --install . && cd ..
# Remove QtService folder, as it is no longer needed
## for arm64
rm -r QtService_arm64
## for armel
rm -r QtService_armel
## for armhf
rm -r QtService_armhf

# VICO-src
# Install dependencies according to the documentation at https://github.com/libusb/hidapi/blob/master/BUILD.md#linux
## for arm64
apt install -y pkg-config libudev-dev:arm64 libusb-1.0-0-dev:arm64
## for armel
apt install -y pkg-config libudev-dev:armel libusb-1.0-0-dev:armel
## for armhf
apt install -y pkg-config libudev-dev:armhf libusb-1.0-0-dev:armhf
# Build hidapi using the specific arm compiler using cmake
## for arm64
### Create temporary output for the hidapi build based on latest hidapi-0.14.0 tag
mkdir hidapi_arm64 && cd hidapi_arm64 && git clone --depth 1 --branch hidapi-0.14.0 https://github.com/libusb/hidapi.git
# Build and install using cmake
PKG_CONFIG_PATH=/usr/lib/aarch64-linux-gnu/pkgconfig cmake ./hidapi -DCMAKE_C_COMPILER=aarch64-linux-gnu-gcc -DCMAKE_SYSTEM_NAME=Linux \
    -DCMAKE_FIND_ROOT_PATH=/usr/aarch64-linux-gnu -DCMAKE_INSTALL_PREFIX=./install
cmake --build . --target install
cp -r ./install/lib/* /usr/lib/aarch64-linux-gnu/.
cp -r ./install/include/* /usr/lib/aarch64-linux-gnu/include/
### After that, the temporary folder can be removed.
cd ..
rm -r hidapi_arm64
## for armel
### Create temporary output for the hidapi build based on latest hidapi-0.14.0 tag
mkdir hidapi_armel && cd hidapi_armel && git clone --depth 1 --branch hidapi-0.14.0 https://github.com/libusb/hidapi.git
# Build and install using cmake
PKG_CONFIG_PATH=/usr/lib/arm-linux-gnueabi/pkgconfig cmake ./hidapi -DCMAKE_C_COMPILER=arm-linux-gnueabi-gcc -DCMAKE_SYSTEM_NAME=Linux \
    -DCMAKE_FIND_ROOT_PATH=/usr/arm-linux-gnueabi -DCMAKE_INSTALL_PREFIX=./install
cmake --build . --target install
cp -r ./install/lib/* /usr/lib/arm-linux-gnueabi/.
cp -r ./install/include/* /usr/lib/arm-linux-gnueabi/include/
### After that, the temporary folder can be removed.
cd ..
rm -r hidapi_armel
## for armhf
### Create temporary output for the hidapi build based on latest hidapi-0.14.0 tag
mkdir hidapi_armhf && cd hidapi_armhf && git clone --depth 1 --branch hidapi-0.14.0 https://github.com/libusb/hidapi.git
# Build and install using cmake
PKG_CONFIG_PATH=/usr/lib/arm-linux-gnueabihf/pkgconfig cmake ./hidapi -DCMAKE_C_COMPILER=arm-linux-gnueabihf-gcc -DCMAKE_SYSTEM_NAME=Linux \
    -DCMAKE_FIND_ROOT_PATH=/usr/arm-linux-gnueabihf -DCMAKE_INSTALL_PREFIX=./install
cmake --build . --target install
cp -r ./install/lib/* /usr/lib/arm-linux-gnueabihf/.
cp -r ./install/include/* /usr/lib/arm-linux-gnueabihf/include/
### After that, the temporary folder can be removed.
cd ..
rm -r hidapi_armhf

# Compile VICODaemon
## for arm64
### Change directory to VICODaemon
cd VICO-src_arm64/VICODaemon
### Compile using mkspecs
/opt/aarch64-linux-gnu/qt6/bin/qmake -spec /opt/aarch64-linux-gnu/qt6/mkspecs/linux-aarch64-gnu-g++ && make
### Change directory
cd ../../
## for armel
### Change directory to VICODaemon
cd VICO-src_armel/VICODaemon
### Compile
/opt/arm-linux-gnueabi/qt6/bin/qmake -spec /opt/arm-linux-gnueabi/qt6/mkspecs/linux-arm-gnueabi-g++ && make
### Change directory
cd ../../
## for armhf
### Change directory to VICODaemon
cd VICO-src_armhf/VICODaemon
### Compile
/opt/arm-linux-gnueabihf/qt6/bin/qmake -spec /opt/arm-linux-gnueabihf/qt6/mkspecs/linux-arm-gnueabihf-g++ && make
### Change directory
cd ../../

# Deployment
# Copy the VICODaemon_arm deploy folder
## for arm64
cp -r VICO-src_arm64/deploy/VICODaemon_arm VICODaemon_arm64
## for armel
cp -r VICO-src_armel/deploy/VICODaemon_arm VICODaemon_armel
## for armhf
cp -r VICO-src_armhf/deploy/VICODaemon_arm VICODaemon_armhf
# Copy the built VICODaemon into the usr/bin directory
## for arm64
cp VICO-src_arm64/VICODaemon/VICODaemon VICODaemon_arm64/usr/bin
## for armel
cp VICO-src_armel/VICODaemon/VICODaemon VICODaemon_armel/usr/bin
## for armhf
cp VICO-src_armhf/VICODaemon/VICODaemon VICODaemon_armhf/usr/bin
# Copy Qt libraries into the usr/lib directory
## for arm64
cp /opt/aarch64-linux-gnu/qt6/lib/libQt6Service.so* VICODaemon_arm64/usr/lib/.
cp /opt/aarch64-linux-gnu/qt6/lib/libQt6Network.so* VICODaemon_arm64/usr/lib/.
cp /opt/aarch64-linux-gnu/qt6/lib/libQt6Core.so* VICODaemon_arm64/usr/lib/.
cp /opt/aarch64-linux-gnu/qt6/lib/libQt6DBus.so* VICODaemon_arm64/usr/lib/.
## for armel
cp /opt/arm-linux-gnueabi/qt6/lib/libQt6Service.so* VICODaemon_armel/usr/lib/.
cp /opt/arm-linux-gnueabi/qt6/lib/libQt6Network.so* VICODaemon_armel/usr/lib/.
cp /opt/arm-linux-gnueabi/qt6/lib/libQt6Core.so* VICODaemon_armel/usr/lib/.
cp /opt/arm-linux-gnueabi/qt6/lib/libQt6DBus.so* VICODaemon_armel/usr/lib/.
## for armhf
cp /opt/arm-linux-gnueabihf/qt6/lib/libQt6Service.so* VICODaemon_armhf/usr/lib/.
cp /opt/arm-linux-gnueabihf/qt6/lib/libQt6Network.so* VICODaemon_armhf/usr/lib/.
cp /opt/arm-linux-gnueabihf/qt6/lib/libQt6Core.so* VICODaemon_armhf/usr/lib/.
cp /opt/arm-linux-gnueabihf/qt6/lib/libQt6DBus.so* VICODaemon_armhf/usr/lib/.
# Copy the needed Qt plugins into the usr/plugins directory
## for arm64
cp -r /opt/aarch64-linux-gnu/qt6/plugins/servicebackends VICODaemon_arm64/usr/plugins/.
## for armel
cp -r /opt/arm-linux-gnueabi/qt6/plugins/servicebackends VICODaemon_armel/usr/plugins/.
## for armhf
cp -r /opt/arm-linux-gnueabihf/qt6/plugins/servicebackends VICODaemon_armhf/usr/plugins/.
# Copy external libraries into the usr/lib directory
## for arm64
cp VICO-src_arm64/external-libs/LibFT4222/arm64/* VICODaemon_arm64/usr/lib/.
cp /usr/lib/aarch64-linux-gnu/libhidapi-hidraw.so* VICODaemon_arm64/usr/lib/
cp /usr/lib/aarch64-linux-gnu/libudev.so* VICODaemon_arm64/usr/lib/.
## for armel
cp VICO-src_armel/external-libs/LibFT4222/armel/* VICODaemon_armel/usr/lib/.
cp /usr/lib/arm-linux-gnueabi/libhidapi-hidraw.so* VICODaemon_armel/usr/lib/
cp /usr/lib/arm-linux-gnueabi/libudev.so* VICODaemon_armel/usr/lib/.
## for armhf
cp VICO-src_armhf/external-libs/LibFT4222/armhf/* VICODaemon_armhf/usr/lib/.
cp /usr/lib/arm-linux-gnueabihf/libhidapi-hidraw.so* VICODaemon_armhf/usr/lib/
cp /usr/lib/arm-linux-gnueabihf/libudev.so* VICODaemon_armhf/usr/lib/.
# Copy translations
## for arm64
cp /opt/aarch64-linux-gnu/qt6/translations/qt_*.qm VICODaemon_arm64/usr/translations/.
## for armel
cp /opt/arm-linux-gnueabi/qt6/translations/qt_*.qm VICODaemon_armel/usr/translations/.
## for armhf
cp /opt/arm-linux-gnueabihf/qt6/translations/qt_*.qm VICODaemon_armhf/usr/translations/.
# Copy license files
## for arm64
cp VICO-src_arm64/VICODaemon/LICENSE* VICODaemon_arm64/.
## for armel
cp VICO-src_armel/VICODaemon/LICENSE* VICODaemon_armel/.
## for armhf
cp VICO-src_armhf/VICODaemon/LICENSE* VICODaemon_armhf/.

# VICOLib
# Compile VICOLib
## for arm64
### Change directory to VICOLib
cd VICO-src_arm64/VICOLib
### Compile
/opt/aarch64-linux-gnu/qt6/bin/qmake -spec /opt/aarch64-linux-gnu/qt6/mkspecs/linux-aarch64-gnu-g++ && make
### Change directory
cd ../../
## for armel
### Change directory to VICOLib
cd VICO-src_armel/VICOLib
### Compile
/opt/arm-linux-gnueabi/qt6/bin/qmake -spec /opt/arm-linux-gnueabi/qt6/mkspecs/linux-arm-gnueabi-g++ && make
### Change directory
cd ../../
## for armhf
### Change directory to VICOLib
cd VICO-src_armhf/VICOLib
### Compile
/opt/arm-linux-gnueabihf/qt6/bin/qmake -spec /opt/arm-linux-gnueabihf/qt6/mkspecs/linux-arm-gnueabihf-g++ && make
### Change directory
cd ../../

# Deployment
# Copy the VICOLib_arm deploy folder
## for arm64
cp -r VICO-src_arm64/deploy/VICOLib_arm VICOLib_arm64
## for armel
cp -r VICO-src_armel/deploy/VICOLib_arm VICOLib_armel
## for armhf
cp -r VICO-src_armhf/deploy/VICOLib_arm VICOLib_armhf
# Copy the built VICOLib into the directory and the example folder
## for arm64
cp VICO-src_arm64/VICOLib/libVICOLib.so* VICOLib_arm64/.
cp VICO-src_arm64/VICOLib/libVICOLib.so* VICOLib_arm64/example/.
## for armel
cp VICO-src_armel/VICOLib/libVICOLib.so* VICOLib_armel/.
cp VICO-src_armel/VICOLib/libVICOLib.so* VICOLib_armel/example/.
## for armhf
cp VICO-src_armhf/VICOLib/libVICOLib.so* VICOLib_armhf/.
cp VICO-src_armhf/VICOLib/libVICOLib.so* VICOLib_armhf/example/.
# Copy header files and Qt binaries to the example directory
## for arm64
cp VICO-src_arm64/VICOLib/impl/*.h VICOLib_arm64/example/.
cp VICODaemon_arm64/usr/lib/libQt6Core.so* VICOLib_arm64/example/.
cp VICODaemon_arm64/usr/lib/libQt6Network.so* VICOLib_arm64/example/.
## for armel
cp VICO-src_armel/VICOLib/impl/*.h VICOLib_armel/example/.
cp VICODaemon_armel/usr/lib/libQt6Core.so* VICOLib_armel/example/.
cp VICODaemon_armel/usr/lib/libQt6Network.so* VICOLib_armel/example/.
## for armhf
cp VICO-src_armhf/VICOLib/impl/*.h VICOLib_armhf/example/.
cp VICODaemon_armhf/usr/lib/libQt6Core.so* VICOLib_armhf/example/.
cp VICODaemon_armhf/usr/lib/libQt6Network.so* VICOLib_armhf/example/.
# Compile the example C++ file
## for arm64
### Change directory to VICOLib example
cd VICOLib_arm64/example
### Compile
aarch64-linux-gnu-g++ -o VICOLib_arm64_example main.cpp -L. -lVICOLib -Wl,-rpath,.
### Change directory
cd ../../
## for armel
### Change directory to VICOLib example
cd VICOLib_armel/example
### Compile
arm-linux-gnueabi-g++ -o VICOLib_armel_example main.cpp -L. -lVICOLib -Wl,-rpath,.
### Change directory
cd ../../
## for armhf
### Change directory to VICOLib example
cd VICOLib_armhf/example
### Compile
arm-linux-gnueabihf-g++ -o VICOLib_armhf_example main.cpp -L. -lVICOLib -Wl,-rpath,.
### Change directory
cd ../../
# Copy license files
## for arm64
cp VICO-src_arm64/VICOLib/LICENSE* VICOLib_arm64/.
## for armel
cp VICO-src_armel/VICOLib/LICENSE* VICOLib_armel/.
## for armhf
cp VICO-src_armhf/VICOLib/LICENSE* VICOLib_armhf/.

# Remove created folders
## for arm64
rm -r VICO-src_arm64
## for armel
rm -r VICO-src_armel
## for armhf
rm -r VICO-src_armhf

# Create 7z archive
7z a VICOSoftware_arm -oVICOSoftware_arm.7z