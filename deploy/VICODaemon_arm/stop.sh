#!/bin/bash

# Stop the VICODaemon service using systemd
systemctl stop VICODaemon

# Disable the VICODaemon service to prevent autostart at startup
systemctl disable VICODaemon

# Remove the VICODaemon service file
rm /etc/systemd/system/VICODaemon.service

# Remove the VICODaemon dbus configuration file
rm /etc/dbus-1/system.d/VICODaemon.systemd-service.conf

# Reload all services after removing the VICODaemon service
systemctl daemon-reload
