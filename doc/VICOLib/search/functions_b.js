var searchData=
[
  ['readfirmwaresection_0',['readFirmwareSection',['../a00002.html#a63311816b440e4dc2536a07d7f7d5559',1,'vicolib.h']]],
  ['refreshdeviceconnections_1',['refreshDeviceConnections',['../a00002.html#a0af2644147a04d182f1e90146a6d57d2',1,'vicolib.h']]],
  ['removedeviceconnection_2',['removeDeviceConnection',['../a00002.html#a6c5b277dacbf3cf4aafdf6f3d0153c0a',1,'vicolib.h']]],
  ['reporterror_3',['reportError',['../a00086.html#a183cf5f7bae87d5d3624c9ab97ad2249',1,'VicoQuery']]],
  ['resetfpga_4',['resetFPGA',['../a00002.html#a1904d1c381411ffb389731fb4f6db659',1,'vicolib.h']]],
  ['resetmcu_5',['resetMCU',['../a00002.html#a5a3e2727f45a46a9aa04d006c5b8cff2',1,'vicolib.h']]],
  ['resumerun_6',['resumeRun',['../a00002.html#aa67c29e5f523d94a9bbd0405244a13a8',1,'vicolib.h']]]
];
