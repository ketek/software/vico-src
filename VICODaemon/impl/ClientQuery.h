#ifndef CLIENTQUERY_H
#define CLIENTQUERY_H

#include <QtNetwork>

/**
 * @brief The ClientQuery class handles the connection from VICODaemon to the VICOLib. This means either receiving queries from VICOlib, or answering queries to VICOLib.
 */
class ClientQuery : public QObject
{
	Q_OBJECT
	Q_DISABLE_COPY(ClientQuery)

public:
	explicit ClientQuery(QObject *parent = nullptr);
	virtual bool setSocketDescriptor(qintptr socketDescriptor);
	void answerQuery(const QJsonObject &data);
signals:
	void queryReceived(const QJsonObject &reply);
	void disconnectedFromClient();
	void error();
	void logMessage(const QString &msg);

public slots:
	void disconnectFromClient();

private slots:
	void onReadyRead();
	void onErrorOccurred(QLocalSocket::LocalSocketError socketError);

private:
	QLocalSocket *socket;
	quint32 blocksize;
	void jsonReceived(const QJsonObject &doc);
};
Q_DECLARE_METATYPE(QLocalSocket::LocalSocketError)
#endif // CLIENTQUERY_H
