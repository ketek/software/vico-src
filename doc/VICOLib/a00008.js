var a00008 =
[
    [ "VersionType", "a00026.html", "a00026" ],
    [ "RunStatisticsType", "a00030.html", "a00030" ],
    [ "FirmwareVersionType", "a00034.html", "a00034" ],
    [ "MCUStatusInfoType", "a00038.html", "a00038" ],
    [ "LiveInfo1VICOType", "a00042.html", "a00042" ],
    [ "LiveInfo2VICOType", "a00046.html", "a00046" ],
    [ "LiveInfoBoundariesVICOType", "a00050.html", "a00050" ],
    [ "LiveInfo1VIAMPType", "a00054.html", "a00054" ],
    [ "LiveInfo2VIAMPType", "a00058.html", "a00058" ],
    [ "LiveInfoBoundariesVIAMPType", "a00062.html", "a00062" ],
    [ "DevInfo1VICOType", "a00066.html", "a00066" ],
    [ "DevInfo1BootloaderType", "a00070.html", "a00070" ],
    [ "TempType", "a00074.html", "a00074" ],
    [ "DbgClpType", "a00078.html", "a00078" ],
    [ "DbgClpExtType", "a00082.html", "a00082" ],
    [ "BooleanType", "a00008.html#a0302c79f6d6b93d902af278d1191e054", null ],
    [ "CharType", "a00008.html#a759ad3e3ce7817487b5ed18f68561154", null ],
    [ "DoubleType", "a00008.html#a5f642903648a5cbec7a6d8326bc2eed6", null ],
    [ "FloatType", "a00008.html#abad655ce3987edbc759b46415d1d0120", null ],
    [ "UInt16Type", "a00008.html#a7387aac0178fcc6d6cb70a2625a63bc0", null ],
    [ "UInt32Type", "a00008.html#a3364c225e4641202e777c012fcd14f92", null ],
    [ "UInt8Type", "a00008.html#a3af313c104055b0c5e56145e6ef2d14f", null ],
    [ "BaselineTrimType", "a00008.html#acabfe55595d206d18f8d3986807d05a8", [
      [ "LONGEST_POSSIBLE_MEDIUM_FILTER", "a00008.html#acabfe55595d206d18f8d3986807d05a8af399452dab1754ca65f4c6c74ea61f32", null ],
      [ "LONG_MEDIUM_FILTER", "a00008.html#acabfe55595d206d18f8d3986807d05a8ab339825b021abc48e5a4e4bb4dae064b", null ],
      [ "INTERMEDIATE_MEDIUM_FILTER", "a00008.html#acabfe55595d206d18f8d3986807d05a8a1e4acfb88142f60dda4c1bd2904e2380", null ],
      [ "SHORT_MEDIUM_FILTER", "a00008.html#acabfe55595d206d18f8d3986807d05a8aa708c97f39f5e0cf1210e6cbe4d467bf", null ],
      [ "SHORTEST_POSSIBLE_MEDIUM_FILTER", "a00008.html#acabfe55595d206d18f8d3986807d05a8aa12e9286890eaa629a91bebe264e7be5", null ]
    ] ],
    [ "BootloaderReasonType", "a00008.html#a2f2de055f449efdbc64a26b849184922", [
      [ "RescuePinActive", "a00008.html#a2f2de055f449efdbc64a26b849184922a9161f28eca9a867ea78f113940cef7aa", null ],
      [ "FlashRequest", "a00008.html#a2f2de055f449efdbc64a26b849184922a9933ed7be93d2e2d6c7cf02637d7c881", null ],
      [ "AppCrcInvalid", "a00008.html#a2f2de055f449efdbc64a26b849184922ae793101b692d52ee29c1f68dfa4383d7", null ]
    ] ],
    [ "BootloaderSessionType", "a00008.html#ab1608f794858a5c05ad54b20627f1c4f", [
      [ "Default", "a00008.html#ab1608f794858a5c05ad54b20627f1c4fa79935518a3889663d8688b6b01fff051", null ],
      [ "Flash", "a00008.html#ab1608f794858a5c05ad54b20627f1c4fab5fcae55946da2e578e45bef020fa9e2", null ]
    ] ],
    [ "ClockingSpeedType", "a00008.html#a1151fa44f5733275ec643f5d8760726d", [
      [ "MHZ_80", "a00008.html#a1151fa44f5733275ec643f5d8760726da2143fbdefa6cfd5b059fbf8e4cdef706", null ]
    ] ],
    [ "CommandType", "a00008.html#a21e038f5b8958e203d28bc4f18472352", [
      [ "READ", "a00008.html#a21e038f5b8958e203d28bc4f18472352acb9be765f361bb7efb9073730aac92c6", null ],
      [ "WRITE", "a00008.html#a21e038f5b8958e203d28bc4f18472352a61aa7ff70b76bff0fda378cf61d6afbc", null ]
    ] ],
    [ "DPPStatusType", "a00008.html#a5e0b8308439c337e74cbedce93d9c53a", [
      [ "DPP_SUCCESS", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aab6e84e71c47ba7e5e2054e889bc8f95a", null ],
      [ "DPP_OUT_OF_RANGE_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa951aa6136437acbc141a691144008354", null ],
      [ "DPP_PARAMETER_READ_ONLY_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa2a861f2d2bdb7366872afa06253be321", null ],
      [ "DPP_PARAMETER_NOT_EXISTING_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa139b7b881540f191b461a1ad593d6786", null ],
      [ "DPP_INVALID_COMMAND_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aade7efe89d0d25e19496bd5b3de3e769f", null ],
      [ "DPP_REQUEST_CURRENTLY_NOT_POSSIBLE_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa2a01a0e60cb023d0a4cc8e5d7017000b", null ],
      [ "DPP_TIMEOUT_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa173092d9234cba940754c6a5c8c00903", null ],
      [ "DPP_SYNTAX_ERROR_INVALID_REQUEST_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aacc50474a88a672a586dd35ab25f5b781", null ],
      [ "DPP_NO_DEVICE_WITH_ID_CONNECTED_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa262d484c73f1dba7ee8660e7670a4b89", null ],
      [ "DPP_VICODAEMON_IS_STOPPED", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa22d92c7b43738f4387f9b9f6e1c0747b", null ],
      [ "DPP_VICOLIB_TIMEOUT_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa74c8cda3316534341a5f5f8844ec33c3", null ],
      [ "DPP_RESPONSE_OUT_OF_BOUNDS_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aaaab7c7161fd85f262a4f8d3d44b83343", null ],
      [ "DPP_INVALID_ARGUMENT_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa39a60336a8e4721af65cfa4bf34bf25f", null ],
      [ "DPP_INVALID_RESPONSE_DATA_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa75dd74ef0af216abaf688910c63cc6b4", null ],
      [ "DPP_VICOLIB_OUT_OF_RANGE_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa5ac5fae9bbabcf76719ede8462c6ede8", null ],
      [ "DPP_COMMAND_NOT_SUPPORTED_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aacd1720966224e491f9c16d46fee8fa6c", null ],
      [ "DPP_UNDEFINED_ERROR", "a00008.html#a5e0b8308439c337e74cbedce93d9c53aa5a39503633c6254a011d1e4dab1f65db", null ]
    ] ],
    [ "EthernetProtocolType", "a00008.html#a9cbea0ecdcdedae782e812875b1a30f9", [
      [ "TCP", "a00008.html#a9cbea0ecdcdedae782e812875b1a30f9aa040cd7feeb588104634cdadf35abf1c", null ],
      [ "UDP", "a00008.html#a9cbea0ecdcdedae782e812875b1a30f9adb542475cf9d0636e4225e216cee9ae6", null ]
    ] ],
    [ "EthernetSpeedType", "a00008.html#a45468ea319c6f890e3ca60734afd71c9", [
      [ "AUTO_NEGOTIATION", "a00008.html#a45468ea319c6f890e3ca60734afd71c9a53308169760b89a44d594510cd269e74", null ],
      [ "HALF_DUPLEX_10_MBITS", "a00008.html#a45468ea319c6f890e3ca60734afd71c9ad34614e1280d9b2359d49264bc8137dc", null ],
      [ "FULL_DUPLEX_10_MBITS", "a00008.html#a45468ea319c6f890e3ca60734afd71c9af8ea3a3b8db17d357a689fa59b9a0a18", null ],
      [ "HALF_DUPLEX_100_MBITS", "a00008.html#a45468ea319c6f890e3ca60734afd71c9a4d6872c8cf4f237c91562cb6b26cc303", null ],
      [ "FULL_DUPLEX_100_MBITS", "a00008.html#a45468ea319c6f890e3ca60734afd71c9ac297bc8c9392e75fc9ffa215c712c6f3", null ]
    ] ],
    [ "EventScopeType", "a00008.html#acc81e3aa04d287235f9296f75bbb11ea", [
      [ "ADC_DATA", "a00008.html#acc81e3aa04d287235f9296f75bbb11eaa1dd47b8ebedd6c45d145f32ac883be3e", null ],
      [ "FASTFILTER", "a00008.html#acc81e3aa04d287235f9296f75bbb11eaa3a42c70e59bd3f2494cb176bfc5f7877", null ],
      [ "MEDIUMFILTER", "a00008.html#acc81e3aa04d287235f9296f75bbb11eaa171e883f1bb5d91f55298a5ee7c89c9d", null ],
      [ "SLOWFILTER", "a00008.html#acc81e3aa04d287235f9296f75bbb11eaa964a26574fd53488aa6b8ce1cfbd4e49", null ],
      [ "BASELINE_AVERAGE", "a00008.html#acc81e3aa04d287235f9296f75bbb11eaa811bdbb56ba817c88946ea3e5f6eb808", null ],
      [ "BASELINE_SAMPLE", "a00008.html#acc81e3aa04d287235f9296f75bbb11eaa1e915d3d82ce6247aff9c59058634864", null ]
    ] ],
    [ "InterfaceType", "a00008.html#af1ef2c97e255bd7f21d3b7614b283d9d", [
      [ "INTERFACE_UNKNOWN", "a00008.html#af1ef2c97e255bd7f21d3b7614b283d9da831a53242f259bcbc636e087290fe629", null ],
      [ "INTERFACE_ETHERNET_TCP", "a00008.html#af1ef2c97e255bd7f21d3b7614b283d9da07463f829c7cd64c8ba160df6c73b04b", null ],
      [ "INTERFACE_ETHERNET_UDP", "a00008.html#af1ef2c97e255bd7f21d3b7614b283d9da4b41fecf4eac9982d48f24cb96ccb504", null ],
      [ "INTERFACE_USB", "a00008.html#af1ef2c97e255bd7f21d3b7614b283d9daf5ad0a3868d515c1041169371f0d31c9", null ],
      [ "INTERFACE_USB_HID", "a00008.html#af1ef2c97e255bd7f21d3b7614b283d9da4cda852f5798fc2cc4356d1d5b7f3140", null ]
    ] ],
    [ "LiveInfoVIAMPErrorType", "a00008.html#a28d62e044fc472c1bd5e34bfe57cf8a3", [
      [ "NO_VIAMP_ERROR", "a00008.html#a28d62e044fc472c1bd5e34bfe57cf8a3ae575ade88c9525e56dda8d44e9c72779", null ],
      [ "VPERR_01", "a00008.html#a28d62e044fc472c1bd5e34bfe57cf8a3ac3142a73d21fa7ad5adfd8d603cc7e31", null ],
      [ "VPERR_02", "a00008.html#a28d62e044fc472c1bd5e34bfe57cf8a3ac24d4ea4820cb79f3a997a7b34bf417d", null ],
      [ "VPERR_03", "a00008.html#a28d62e044fc472c1bd5e34bfe57cf8a3af0748bb04310ac2baa8d7c4c35cfdf94", null ],
      [ "VPERR_04", "a00008.html#a28d62e044fc472c1bd5e34bfe57cf8a3ae9ca2047d0f0a8009152c2ec76f2fca0", null ],
      [ "VPERR_05", "a00008.html#a28d62e044fc472c1bd5e34bfe57cf8a3af81749d6fe68e6123e555b11c28caa19", null ],
      [ "VPERR_06", "a00008.html#a28d62e044fc472c1bd5e34bfe57cf8a3a050ed416619c0afde46dadb52c36f670", null ],
      [ "VPERR_07", "a00008.html#a28d62e044fc472c1bd5e34bfe57cf8a3ad314475676464580e3ebbc6b964cfd35", null ],
      [ "UNKNOWN_VIAMP_ERROR", "a00008.html#a28d62e044fc472c1bd5e34bfe57cf8a3a1e946eda96a45b522e8f3c3ef8f789ec", null ]
    ] ],
    [ "LiveInfoVIAMPStateType", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3", [
      [ "INACTIVE", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3a3ff8ba88da6f8947ab7c22b7825c6bb6", null ],
      [ "EN_VOLTAGE", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3ae55f509a7bc4a5576158db58750236c3", null ],
      [ "CHECK_CONN_TEMP", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3a8f3e9560c5bdaaaa6719daaf4a3cafdf", null ],
      [ "CHECK_CONN_I2C", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3aab794426c9aa79cf1723ff4affb068e7", null ],
      [ "DISCONNECTED", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3acdaad1112073e3e2ea032424c38c34e1", null ],
      [ "VALIDATE_EEPCONTENT", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3a3b08918937be887f2591a5e43c69edeb", null ],
      [ "NO_OPERATION", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3ac2aefff5174213dbd2f848c448debc5b", null ],
      [ "ENABLE_HV", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3a5777dd11458327e9454973044a668e5a", null ],
      [ "VIAMP_ERROR", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3a831db123bce7f0cc4329f05b34d5a6f7", null ],
      [ "PREPARE_OPERATION", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3ae6d945bc0706156a31e0ce66a9d8508d", null ],
      [ "OP_READY", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3a34a32bd71c19cd42f198870bff2d0d94", null ],
      [ "OP_ECO1", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3a32973d722cafa4ed3a4e684e7b918054", null ],
      [ "OP_FULL", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3a53dd689199bd3570069518358f861bcc", null ],
      [ "DISABLE_HV", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3aea6fa3409019ce286cdfe25de8d44dc4", null ],
      [ "OP_ECO2", "a00008.html#a593679ebe2c5361d1e91ebc58b3e25e3a9c6e6a14068136e5322b3b50753c4566", null ]
    ] ],
    [ "LiveInfoVICOErrorType", "a00008.html#a54d168a379505a24ddf538b966b0fff0", [
      [ "NO_VICO_ERROR", "a00008.html#a54d168a379505a24ddf538b966b0fff0a37897352b392e08198864ef5523c19ac", null ],
      [ "VOERR_01", "a00008.html#a54d168a379505a24ddf538b966b0fff0a27e4e7d309d123c3b5259fe164a3d9c3", null ],
      [ "VOERR_02", "a00008.html#a54d168a379505a24ddf538b966b0fff0af2902448895127d73ed8b85ffc12140f", null ],
      [ "VOERR_03", "a00008.html#a54d168a379505a24ddf538b966b0fff0abd55106a3dba4650045030596920bd7c", null ],
      [ "VOERR_11", "a00008.html#a54d168a379505a24ddf538b966b0fff0a8446e413ddbc7a85c5a4af8ffa05eddc", null ],
      [ "VOERR_12", "a00008.html#a54d168a379505a24ddf538b966b0fff0a93173b1b5931bb1a3d8e76ed75df1315", null ],
      [ "VOERR_13", "a00008.html#a54d168a379505a24ddf538b966b0fff0a803971abd29f62dd142a2a76ff700871", null ],
      [ "VOERR_14", "a00008.html#a54d168a379505a24ddf538b966b0fff0a736a7bc1c8c89c6b1b2acd4ed4414cfe", null ],
      [ "UNKNOWN_VICO_ERROR", "a00008.html#a54d168a379505a24ddf538b966b0fff0a629b28a5947e58bf7f63bc6bfe67f77b", null ]
    ] ],
    [ "LiveInfoVICOStateType", "a00008.html#ab021cb6462a87152ac9ab7aa8be31e11", [
      [ "INIT", "a00008.html#ab021cb6462a87152ac9ab7aa8be31e11a0cb1b2c6a7db1f1084886c98909a3f36", null ],
      [ "OPERATION", "a00008.html#ab021cb6462a87152ac9ab7aa8be31e11ae6c7d41b255eff353251fbec6fe839e4", null ],
      [ "VICO_ERROR", "a00008.html#ab021cb6462a87152ac9ab7aa8be31e11a899a31c35abfab99451da995a14f2d5c", null ],
      [ "ECO3", "a00008.html#ab021cb6462a87152ac9ab7aa8be31e11a9dd6ce7ffe568e72feef0e9486c0062c", null ],
      [ "EEA", "a00008.html#ab021cb6462a87152ac9ab7aa8be31e11ac1ff6b7ce2cad1c4c45241d8ba045765", null ]
    ] ],
    [ "LogLevelType", "a00008.html#afe7f03cf4783010401c6e749fc8c61b8", [
      [ "LOG_NONE", "a00008.html#afe7f03cf4783010401c6e749fc8c61b8a85639df34979de4e5ff6f7b05e4de8f1", null ],
      [ "LOG_ERROR", "a00008.html#afe7f03cf4783010401c6e749fc8c61b8a230506cce5c68c3bac5a821c42ed3473", null ],
      [ "LOG_WARNING", "a00008.html#afe7f03cf4783010401c6e749fc8c61b8a8f6fe15bfe15104da6d1b360194a5400", null ],
      [ "LOG_INFO", "a00008.html#afe7f03cf4783010401c6e749fc8c61b8a6e98ff471e3ce6c4ef2d75c37ee51837", null ],
      [ "LOG_DEBUG", "a00008.html#afe7f03cf4783010401c6e749fc8c61b8ab9f002c6ffbfd511da8090213227454e", null ]
    ] ],
    [ "MCUStatusType", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8", [
      [ "MCU_SUCCESS", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8ade04131d83b67ead5c1fb5d203cd7b4f", null ],
      [ "MCU_COMMAND_NOT_SUPPORTED", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a9725cf2b1f684b17261c307f46e8959c", null ],
      [ "MCU_WRONG_CRC_CHECKSUM", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a376d2658f547c934267572b01af85d05", null ],
      [ "MCU_COMMAND_LENGTH_MISMATCH", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8ae4246d63682ae8a50be81c5716f33f15", null ],
      [ "MCU_VERSION_NOT_SUPPORTED", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8aa79378dc1181102a703cd9db71ebcb20", null ],
      [ "MCU_ERROR", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8ad9887800eeafe629b296a87712695943", null ],
      [ "MCU_LEN_OUT_OF_VALID_RANGE", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a26bbcdcce36841c29647a5db237835b8", null ],
      [ "MCU_ADDRESS_OUT_OF_VALID_RANGE", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8aa400087ba857248f12c848874446fcfb", null ],
      [ "MCU_SYSTEM_IS_NOT_IN_EEA_MODE", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a937227f338b33a567a3c426a163a8ec4", null ],
      [ "MCU_TWI_SLAVE_NACK", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a85f315b84e9c6da8f2472fc18a790dce", null ],
      [ "MCU_GETADC_MUTEX_NOT_AVAILABLE", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a1e004b31004c2d378818f283342e73f2", null ],
      [ "MCU_BL_FLASH_SESSION_ACTIVE", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a0d15e2e2899d46695fa1dff60e0d35ce", null ],
      [ "MCU_BL_NO_FLASH_SESSION_ACTIVE", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a697302858f85d69bf2f6c98e0830fbb0", null ],
      [ "MCU_BL_ADDRESS_OUT_OF_VALID_RANGE", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a86357882263fbe7187d8a655fe4ceb27", null ],
      [ "MCU_BL_LEN_OUT_OF_VALID_RANGE", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8ad4ee0c797fc98d910868739c8d6ef564", null ],
      [ "MCU_BL_DATA_NOT_IN_SEQUENCE_WITHIN_SESSION", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a5ec055c7d6316692cbc4d3fd8ad6115d", null ],
      [ "MCU_BL_APPLICATION_CHECKSUM_INVALID", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8aca93556ade3c34d53ea03d5811428159", null ],
      [ "MCU_BL_ALREADY_IN_APPLICATION", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a41756de37db712339a4fe2d98303942e", null ],
      [ "MCU_NO_DEVICE_WITH_ID_CONNECTED_ERROR", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a932f95eca7aafa2dc49a1b0cca19a40f", null ],
      [ "MCU_VICODAEMON_IS_STOPPED", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8af56443fc0e405bed48cb9d5a91f24a67", null ],
      [ "MCU_VICOLIB_TIMEOUT_ERROR", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a615f215de7de6dce59a2d42b8749fa70", null ],
      [ "MCU_RESPONSE_OUT_OF_BOUNDS_ERROR", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8ada48c0fc6188163adda44641bddea234", null ],
      [ "MCU_INVALID_ARGUMENT_ERROR", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a44b9c0120f137fe58c9a876ab8af5ce8", null ],
      [ "MCU_INVALID_RESPONSE_DATA_ERROR", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a9deaf5a025a6f79d49ad1d8235b196a1", null ],
      [ "DPP_UART_TIMEOUT_ERROR", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8adfa0f8f25e09e3959b56c680e26d85fd", null ],
      [ "DPP_NO_ACCESS_TO_INTERNAL_MEMORY_ERROR", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a0826b178bb02500d1171c5612d773566", null ],
      [ "DPP_INCOMPLETE_UART_COMMAND_ERROR", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a5eace52cba2edb8a845e5e8fd92f0520", null ],
      [ "MCU_UNDEFINED_ERROR", "a00008.html#ade5e6c98769a15a4611a6ff810ea66c8a601bc171eb45977c53470157486fa9e3", null ]
    ] ],
    [ "ModeType", "a00008.html#affedf11c575f5d8463807348d4f7f71b", [
      [ "NO_REQUEST", "a00008.html#affedf11c575f5d8463807348d4f7f71ba9bec1b66248eeb708d67b21311e2e6f3", null ],
      [ "FULL_MODE", "a00008.html#affedf11c575f5d8463807348d4f7f71bad0ef341514d196f3470a1d6196160070", null ],
      [ "ECO1_MODE", "a00008.html#affedf11c575f5d8463807348d4f7f71ba8757eff2ee568e392d8f2731c33dd9ae", null ],
      [ "ECO2_MODE", "a00008.html#affedf11c575f5d8463807348d4f7f71baad156901d52420de8b9b4667178a83fb", null ],
      [ "ECO3_MODE", "a00008.html#affedf11c575f5d8463807348d4f7f71ba89078382291dd5fc00c4a6d34a5abdaa", null ],
      [ "EEA_MODE", "a00008.html#affedf11c575f5d8463807348d4f7f71ba6ab377ede32a0167b65b755defa84b03", null ]
    ] ],
    [ "PortType", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21", [
      [ "PORT_A", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21aeb6782d9dfedf3c6a78ffdb1624fa454", null ],
      [ "PORT_B", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21a16ada472d473fbd0207b99e9e4d68f4a", null ],
      [ "PORT_C", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21a627cc690c37f97527dd2f07aa22092d9", null ],
      [ "PORT_D", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21af7242fe75227a46a190645663f91ce69", null ],
      [ "PORT_E", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21abad63f022d1fa37a66f87dc31a78f6a9", null ],
      [ "PORT_F", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21aa3760b302740c7d09c93ec7a634f837c", null ],
      [ "PORT_G", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21a48afb424254d52e7d97a7c1428f5aafa", null ],
      [ "PORT_H", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21a31d1ae08c10668d936d1c2c6426c1c47", null ],
      [ "PORT_J", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21a3eea70262d530701087c2a3a09c475fe", null ],
      [ "PORT_K", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21a23bae6dc45cdca7c90be62437df0f454", null ],
      [ "PORT_L", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21a9e83da163100e922da1c8065b5cf43c2", null ],
      [ "PORT_M", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21ab944189326f5bef270cabd531a023766", null ],
      [ "PORT_N", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21aef6d6fac4ce97e1efa95afc1c3e9177b", null ],
      [ "PORT_P", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21a1a9a09148fb7bcfb9c01d102ae49c3b4", null ],
      [ "PORT_Q", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21a949e8cdb80df02562cb0f2e02f87eaea", null ],
      [ "PORT_R", "a00008.html#a7674c329addda8fbe6176bbbf25c8b21ab94b63ec7d4ff26c379cb895421bb2e8", null ]
    ] ],
    [ "ResetDetectionType", "a00008.html#ada5f054329fa921aebe928b7b5d65a40", [
      [ "CSA", "a00008.html#ada5f054329fa921aebe928b7b5d65a40a006959baeaf3a40aefe8dfcb2ca7c840", null ],
      [ "CLD", "a00008.html#ada5f054329fa921aebe928b7b5d65a40aa221782b67a51cb90d16e00dcdbaebfd", null ],
      [ "CPC", "a00008.html#ada5f054329fa921aebe928b7b5d65a40aeb1e91b3bbee6621cacded4f224d71fb", null ],
      [ "CFT", "a00008.html#ada5f054329fa921aebe928b7b5d65a40ad7c77838ce82c02672d0803ddc4fc2f7", null ]
    ] ],
    [ "SoftwarePackage", "a00008.html#a54fec37e523f1dc73d5fbf62c25c011e", [
      [ "Bootloader", "a00008.html#a54fec37e523f1dc73d5fbf62c25c011ea9faadf271a57f65df057f378d85af82d", null ],
      [ "Application", "a00008.html#a54fec37e523f1dc73d5fbf62c25c011eabc36c6d0296a73abbbc3c3aeb2a13d0e", null ]
    ] ],
    [ "StopConditionType", "a00008.html#a935f47de8bf9fcbbbe86bc28a7679c3b", [
      [ "NONE", "a00008.html#a935f47de8bf9fcbbbe86bc28a7679c3bac157bdf0b85a40d2619cbc8bc1ae5fe2", null ],
      [ "STOP_AT_FIXED_LIVETIME", "a00008.html#a935f47de8bf9fcbbbe86bc28a7679c3ba84566509329f23bced87d71469f0a066", null ],
      [ "STOP_AT_FIXED_REALTIME", "a00008.html#a935f47de8bf9fcbbbe86bc28a7679c3ba8cc45ac79ad9846e50cc27290d7d4070", null ],
      [ "STOP_AT_FIXED_INPUT_COUNTS", "a00008.html#a935f47de8bf9fcbbbe86bc28a7679c3ba70604eb8ad2c22552c8076d06873ff59", null ],
      [ "STOP_AT_FIXED_OUTPUT_COUNTS", "a00008.html#a935f47de8bf9fcbbbe86bc28a7679c3ba975c5683d5a5cabba9a1960f76ea711e", null ]
    ] ],
    [ "TriggerDurationType", "a00008.html#a1b7b6e6e1ce8015dfa99fe6db9709586", [
      [ "TRIGGER_DURATION_1_SECOND", "a00008.html#a1b7b6e6e1ce8015dfa99fe6db9709586a717471fddd24c021565cb32f7166bcbd", null ],
      [ "TRIGGER_DURATION_2_SECONDS", "a00008.html#a1b7b6e6e1ce8015dfa99fe6db9709586ac82e0329fc6a3f01b3e7a2887849e08f", null ],
      [ "TRIGGER_DURATION_4_SECONDS", "a00008.html#a1b7b6e6e1ce8015dfa99fe6db9709586a2c8c7196272dd8c55f1d848d0d332e97", null ],
      [ "TRIGGER_DURATION_8_SECONDS", "a00008.html#a1b7b6e6e1ce8015dfa99fe6db9709586af35b4a4219798a580b3f4a3fad3dda55", null ],
      [ "TRIGGER_DURATION_16_SECONDS", "a00008.html#a1b7b6e6e1ce8015dfa99fe6db9709586a157063fee7d12909055ad0c8a0713040", null ]
    ] ],
    [ "TriggerSourceType", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0", [
      [ "INSTANT_TRIGGER", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0ab2d8c0b40f61975a9c7c952505305c88", null ],
      [ "SPECIFIC_ADC_VALUE", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0af6a7f9b912a0dfba3e76065c52dbd4d4", null ],
      [ "ADC_OUT_OF_RANGE", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0ae8b943da2800cb313c487261a9eec7b1", null ],
      [ "FASTFILTER_TRIGGER", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0a3a8c8055675b5701551bd7e31c7907ba", null ],
      [ "FASTFILTER_RESET_DETECTED", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0ae703850f454eb3ab09dfbba87dcd6110", null ],
      [ "FASTFILTER_PILEUP", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0a56bc621ee57ebdcd709cff2927314ddf", null ],
      [ "MEDIUMFILTER_TRIGGER", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0a630b9f727d108525c5deee3c67d4162a", null ],
      [ "MEDIUMFILTER_RESET_DETECTED", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0aba3d5ba2f84dcb9721e15973cfb9a769", null ],
      [ "MEDIUMFILTER_PILEUP", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0a4c18774b1ea5556fc21f69ca07900ae7", null ],
      [ "NEW_OUTPUT_COUNT_ANY_ENERGY", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0aea80d77b7c23d893148d6cc4f9060af3", null ],
      [ "NEW_OUTPUT_COUNT_SPECIFIC_ENERGY", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0a410c15f2e847552bba32be169977506a", null ],
      [ "NEW_BASELINE_SAMPLE", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0a13287ddd356571780d96e879d34d8f8f", null ],
      [ "ACTIVE_RESET_INITIATE", "a00008.html#a4ed68363159bce20f8d1b5dbc550cdf0a15d221550d42eb78d2936e86ef922d44", null ]
    ] ],
    [ "TriggerTimeoutType", "a00008.html#a04584f139b7f26796501d0f7651b301d", [
      [ "TIMEOUT_1_SECOND", "a00008.html#a04584f139b7f26796501d0f7651b301da29c9d03823ff42e4288978b7694f5106", null ],
      [ "TIMEOUT_2_SECOND", "a00008.html#a04584f139b7f26796501d0f7651b301da786c9fc957625d6e9171f6b5d7164772", null ],
      [ "TIMEOUT_4_SECOND", "a00008.html#a04584f139b7f26796501d0f7651b301da1f2e5cef70b06e8274201c1c3f3edeca", null ],
      [ "TIMEOUT_8_SECOND", "a00008.html#a04584f139b7f26796501d0f7651b301dac742104927a2358d58c3b570d8af746c", null ],
      [ "TIMEOUT_16_SECOND", "a00008.html#a04584f139b7f26796501d0f7651b301dac64d4d299a790114c382d0bc08663923", null ]
    ] ],
    [ "TWIModeType", "a00008.html#a3ca530c7fe407345ab2fefb672fd511d", [
      [ "TWI_WRITE", "a00008.html#a3ca530c7fe407345ab2fefb672fd511daabe9d2ee52d1644f355020fae1a71dc7", null ],
      [ "TWI_READ", "a00008.html#a3ca530c7fe407345ab2fefb672fd511dadbff79e8154b1f3059addaf4b5047ba8", null ],
      [ "TWI_WRITE_READ", "a00008.html#a3ca530c7fe407345ab2fefb672fd511da58429e6000bb604cdf88af6539526119", null ]
    ] ],
    [ "VICOStatusType", "a00008.html#a42c6cba0402a9b6d29b4b9a3d1af2de1", [
      [ "VICO_SUCCESS", "a00008.html#a42c6cba0402a9b6d29b4b9a3d1af2de1a0e30e99c5b9ab326651ce581ae32ebeb", null ],
      [ "VICO_COMMAND_NOT_SUPPORTED", "a00008.html#a42c6cba0402a9b6d29b4b9a3d1af2de1a09d2decd78cc36bf3ba6ed2d39a4beca", null ],
      [ "VICO_COMMAND_NOT_SUPPORTED_BY_DEVICE", "a00008.html#a42c6cba0402a9b6d29b4b9a3d1af2de1a6543a45c8799453213df4804b19acbe6", null ],
      [ "VICO_NO_DEVICE_WITH_ID_CONNECTED_ERROR", "a00008.html#a42c6cba0402a9b6d29b4b9a3d1af2de1ae9ea70597a5372f5f1f2c6f5adf2dc16", null ],
      [ "VICO_VICODAEMON_IS_STOPPED", "a00008.html#a42c6cba0402a9b6d29b4b9a3d1af2de1a2a45c3bd7c1a02d19567b978b2a9c467", null ],
      [ "VICO_VICOLIB_TIMEOUT_ERROR", "a00008.html#a42c6cba0402a9b6d29b4b9a3d1af2de1afe92f3ab9a4f388718dec9a5b658d358", null ],
      [ "VICO_RESPONSE_OUT_OF_BOUNDS_ERROR", "a00008.html#a42c6cba0402a9b6d29b4b9a3d1af2de1aab08eaa16e4621d84bb85313eba1fb56", null ],
      [ "VICO_INVALID_ARGUMENT_ERROR", "a00008.html#a42c6cba0402a9b6d29b4b9a3d1af2de1a1a4a1d66794456def1cdc63d42b65c43", null ],
      [ "VICO_INVALID_RESPONSE_DATA_ERROR", "a00008.html#a42c6cba0402a9b6d29b4b9a3d1af2de1ac3c1dd032b8c60a427d111384ff9227b", null ],
      [ "VICO_UNDEFINED_ERROR", "a00008.html#a42c6cba0402a9b6d29b4b9a3d1af2de1a67290e37247ab21f00837d4aa1ef51d6", null ]
    ] ]
];