#ifndef VICODAEMON_H
#define VICODAEMON_H

#include <QtService/service.h>
#include <QDebug>
#include <QSharedMemory>
#include "QueryServer.h"
#include "DeviceRegistry.h"
#include "UdpScan.h"
#include "TcpScan.h"
#include "UsbHidScan.h"

#ifdef Q_OS_WIN
#define TRACE_MAX_STACK_FRAMES 1024
#define TRACE_MAX_FUNCTION_NAME_LENGTH 1024
#include <dbghelp.h>
typedef WINBOOL (*p_SymInitialize)(HANDLE hProcess, PCSTR UserSearchPath, WINBOOL fInvadeProcess);
typedef WINBOOL (*p_SymFromAddr) (HANDLE hProcess, DWORD64 Address, PDWORD64 Displacement, PSYMBOL_INFO Symbol);
typedef WINBOOL (*p_SymGetLineFromAddr64) (HANDLE hProcess, DWORD64 qwAddr, PDWORD pdwDisplacement, PIMAGEHLP_LINE64 Line64);
#endif

/**
 * @brief The VicoDaemon class contains the VICODaemon QtService implementation. Allows to start and stop the VICODaemon service.
 */
class VicoDaemon : public QtService::Service
{
	Q_OBJECT

public:

	explicit VicoDaemon(int &argc, char **argv);

	static bool setLogLevel(const quint8 &type);
	static quint8 getLogLevel();

protected:
	CommandResult onStart() override;

	CommandResult onStop(int &exitCode) override;

private:
	static const int SHARED_MEMORY_BYTE_NUMBER;
	static const QString SHARED_MEMORY_KEY;

	QSharedMemory *daemonSharedMemory = nullptr;
	QueryServer server;
	UdpScan udpscan;
	TcpScan tcpscan;
	UsbScan usbscan;
	UsbHidScan usbhidscan;

	bool isDaemonRunning();

	static void daemonMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &message);
#ifdef Q_OS_WIN
	static QLibrary* loadDebugLibrary();
	static void printStackTrace(QLibrary *debugLibrary);
#endif
	static void signalHandler(int signum);

private slots:
	void logMessage(const QString &msg);
};

#undef qService
#define qService static_cast<VicoDaemon*>(QtService::Service::instance())

class VICORunnable : public QRunnable {
private:
	std::function<void ()> functionToRun;

	VICORunnable(std::function<void ()> functionToRun) : QRunnable() {
		this->functionToRun = functionToRun;
		setAutoDelete(true);
	}

	void run() override {
		functionToRun();
	}

public:
	static QRunnable* create(std::function<void ()> functionToRun) {
		return new VICORunnable(functionToRun);
	}
};

#endif // VICODAEMON_H
