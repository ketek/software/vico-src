#ifndef USBHIDSCAN_H
#define USBHIDSCAN_H

#include <QObject>
#include "DeviceRegistry.h"
#include "hidapi.h"
#include "UsbHidConnector.h"

/**
 * @brief The UsbHidScan class performs a scan for USB HID-connected devices.
 */
class UsbHidScan : public QObject
{
	Q_OBJECT
public:
	explicit UsbHidScan(QObject *parent = nullptr);
	const QList<QString> getUsbHidDevices();
	const QList<QString> start();

private:
	QList<QString> usb_strings;
	void listFtUsbHidDevices();
	void createUsbHidDevice();

signals:
	void scanFinished();
};
#endif // USBHIDSCAN_H
