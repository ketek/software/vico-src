#ifndef USBCONNECTOR_H
#define USBCONNECTOR_H

#include <QObject>
#include <QDebug>
#include "ftd2xx.h"
#include "LibFT4222.h"
#include <time.h>
#include <unistd.h>
#include "IConnector.h"
#include "types.h"

/**
 * @brief The UsbConnector class creates or closes a connection to a device via FT4222 USB.
 */
class UsbConnector : public QObject, public IConnector
{
	Q_OBJECT
	Q_INTERFACES(IConnector)
public:
	explicit UsbConnector(FT_HANDLE fthandle, FT_HANDLE fthandleGPIO, QString dataSerNo, QString gpioSerNo, QObject *parent = nullptr);
	~UsbConnector();

	InterfaceType getConnectionType() const override;
	void initializeConnection() override;
	void closeConnection() override;

signals:
	void msgReceived(const QByteArray& msg) override;

public slots:
	void sendMsg(const QByteArray& msg, const quint16 size, const int &timeout = DEFAULT_TIMEOUT) override;

private:
	FT_HANDLE fthandle;
	FT_HANDLE fthandleGPIO;
	InterfaceType connectionType;
	FT4222_STATUS DPP3_init(FT_HANDLE ftHandle) const;
	QThreadPool *threadPool;
	QString dataSerNo;
	QString gpioSerNo;

};

#endif // USBCONNECTOR_H
