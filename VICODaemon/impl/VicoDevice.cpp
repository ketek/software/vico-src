#include "VicoDevice.h"

VicoDevice::VicoDevice(QObject *parent) : QObject(parent) {
}

VicoDevice::~VicoDevice() {
}

void VicoDevice::setConnector(IConnector *connector) {
	this->connector = connector;
	if(EthConnector *ethConnector = dynamic_cast<EthConnector*>(this->connector)) {
		connect(ethConnector, &EthConnector::msgReceived, this, &VicoDevice::msgRec);
		connect(ethConnector, &EthConnector::disconnected, this, &VicoDevice::socketDisconnected);
	}
	else if (UsbHidConnector *usbHidConnector = dynamic_cast<UsbHidConnector*>(this->connector)) {
		connect(usbHidConnector, &UsbHidConnector::msgReceived, this, &VicoDevice::msgRec);
	}
	else if(UsbConnector* usbConnector = dynamic_cast<UsbConnector*>(this->connector)) {
		connect(usbConnector, &UsbConnector::msgReceived, this, &VicoDevice::msgRec);
	}
}

void VicoDevice::msgRec(const QByteArray& msg) {
	emit msgReceived(this, msg);
}

void VicoDevice::socketDisconnected() {
	emit disconnected();
}

void VicoDevice::sendMsg(const QByteArray& msg, const quint16 size, const int &timeout) {
	emit sendingMsg();
	this->connector->sendMsg(msg, size, timeout);
}

void VicoDevice::setDeviceId(QString deviceId)
{
	this->deviceId = deviceId;
}

QString VicoDevice::getDeviceId()
{
	return this->deviceId;
}

IConnector* VicoDevice::getConnector() {
	return this->connector;
}

DeviceType VicoDevice::getDeviceType() {
	return this->deviceType;
}

void VicoDevice::setDeviceType(DeviceType devType) {
	this->deviceType = devType;
}
