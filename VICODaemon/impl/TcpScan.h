#ifndef TCPSCAN_H
#define TCPSCAN_H

#include <QObject>
#include <QTcpSocket>
#include <QNetworkDatagram>
#include <QDebug>
#include <QProcess>
#include <QHostAddress>
#include <QCoreApplication>
#include "DeviceRegistry.h"

/**
 * @brief The TcpScan class performs a scanning over a given IP range to find available TCP-configured devices.
 */
class TcpScan : public QObject
{
	Q_OBJECT
public:
	explicit TcpScan(QObject *parent = nullptr);
	~TcpScan() = default;
	void start(QString firstip, QString lastip, const bool &useBinding);
	bool stop_f = false;

	void scan(quint32 ip, const bool &useBinding);

public slots:
	void stop();

private:
	quint16 dest_port = 3141;
	quint16 DEFAULT_RETRY_DELAY = 3u;
	quint16 TIMEOUT_TCP_SCAN_TRY_TO_CONNECT = 80;
	quint16 TIMEOUT_WAIT_FOR_DEVICE_RESPONSE = 2000;
	quint16 TIMEOUT_SOCKET_CONNECT_MS;
	int TIMEOUT_SCANNING_PER_IP_S;
	int AMOUNT_RETRIES;

	QVector<VicoDevice*> reconnectPreviousDevices(QMap<quint32, VicoDevice*> &previousDevices);
	QMap<quint32, VicoDevice*> getPreviousDevices(quint32 firstIP, quint32 lastIP);
	void removeUnconnectedDevices(QVector<quint32> &ipRange, QMap<quint32, VicoDevice*> &previousDevices, QVector<VicoDevice*> &reconnectedDevices);

signals:
	void scanFinished();
};

#endif // TCPSCAN_H
