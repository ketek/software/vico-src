#!/bin/bash

export LD_LIBRARY_PATH=PATH_TO_VICODAEMON/../lib:PATH_TO_VICODAEMON/../plugins/servicebackends:PATH_TO_VICODAEMON/../plugins/bearer
export QT_PLUGIN_PATH=PATH_TO_VICODAEMON/../plugins:$QT_PLUGIN_PATH
PATH_TO_VICODAEMON/VICODaemon $1 $2 $3
