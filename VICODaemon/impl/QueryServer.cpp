#include "QueryServer.h"
#include "QDebug"
#include "vicodaemon.h"

// Default lock timeout initialization
qint64 TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;

QueryServer::QueryServer(QObject *parent) : QLocalServer(parent) {

}

QueryServer::~QueryServer() {
}

void QueryServer::setup() {
	this->responseData = new QByteArray();
	connect(this, &QueryServer::mutexLocked, &timeoutThread, &TimeoutThread::onMutexLocked, Qt::ConnectionType::DirectConnection);
	connect(this, &QueryServer::mutexUnlocked, &timeoutThread, &TimeoutThread::onMutexUnlocked, Qt::ConnectionType::DirectConnection);
	connect(this, &QueryServer::destroyed, &timeoutThread, &TimeoutThread::forceStop, Qt::ConnectionType::DirectConnection);
	connect(&timeoutThread, &TimeoutThread::timeout, this, &QueryServer::onTimeout, Qt::ConnectionType::BlockingQueuedConnection);
	timeoutThread.start();
}

void QueryServer::setUdpScan(UdpScan *udpscan) {
	this->udpscan = udpscan;

}

void QueryServer::setTcpScan(TcpScan *tcpscan)
{
	this->tcpscan = tcpscan;
}

void QueryServer::setUsbScan(UsbScan *usbscan)
{
	this->usbscan = usbscan;
}

void QueryServer::setUsbHidScan(UsbHidScan *usbhidscan)
{
	this->usbhidscan = usbhidscan;
}

void QueryServer::incomingConnection(quintptr socketDescriptor) {
	ClientQuery *query = new ClientQuery(this);
	if(!query->setSocketDescriptor(socketDescriptor)) {
		query->deleteLater();
		return;
	}
	connect(query, &ClientQuery::disconnectedFromClient, this, std::bind(&QueryServer::clientDisconnected, this, query));
	connect(query, &ClientQuery::error, this, std::bind(&QueryServer::clientError, this, query));
	connect(query, &ClientQuery::queryReceived, this, std::bind(&QueryServer::queryReceived, this, query, std::placeholders::_1));
	connect(query, &ClientQuery::logMessage, this, &QueryServer::logMessage);

	m_clients.append(query);
}

QString QueryServer::getDeviceId(const QJsonObject &json) const {
	if(json.contains(API_DATAGRAM_DEVICE)) {
		return json[API_DATAGRAM_DEVICE].toVariant().toString();
	} else {
		qDebug() << "[INTERNAL]\tThe device id is not found within the query!";
		return QStringLiteral("DeviceIdNotFound");
	}
}

bool QueryServer::lockMutex(QString id, bool global, int tryTimeout) {
	if(mutexHandler.lockMutex(id, tryTimeout, global)) {
		if(!global) emit mutexLocked(id);
		return true;
	} else {
		return false;
	}
}

void QueryServer::unlockMutex(QString id, bool global) {
	if(mutexHandler.unlockMutex(id, global)) {
		if(!global) emit mutexUnlocked(id);
	}
}

void QueryServer::handleStopTCPQuery(ClientQuery *query) {
	QJsonObject replyData;
	tcpscan->stop();
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleStopUDPQuery(ClientQuery *query) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;
	udpscan->stop();
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleScanTCPQuery(ClientQuery *query, const QJsonObject &json) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;
	QString fromIp = json.value(API_DATAGRAM_FROM_IP).toString();
	QString toIp = json.value(API_DATAGRAM_TO_IP).toString();
	bool useBinding = json.value(API_DATAGRAM_ETH_SCAN_USE_BINDING).toBool(false);
	tcpscan->start(fromIp, toIp, useBinding);
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleScanUDPQuery(ClientQuery *query, const QJsonObject &json) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;

	QString ipAddress = json.value(API_DATAGRAM_UDPSCAN_IP).toString();
	quint16 subnetMask = json.value(API_DATAGRAM_UDPSCAN_SUBNETMASK).toInt();
	bool useBinding = json.value(API_DATAGRAM_ETH_SCAN_USE_BINDING).toBool(false);
	udpscan->start(ipAddress, subnetMask, useBinding);
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleScanUSBQuery(ClientQuery *query, const QJsonObject &) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;
	usbscan->start();
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleScanUSBHIDQuery(ClientQuery *query, const QJsonObject &) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;
	usbhidscan->start();
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleNumberOfDevices(ClientQuery *query) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;
	replyData[API_DATAGRAM_DATA] = DeviceRegistry::getInstance().getDevices().size();
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleRemoveDevice(ClientQuery *query, const QJsonObject &json) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	VicoDevice* device = nullptr;
	DeviceRegistry& devReg =  DeviceRegistry::getInstance();
	QVector<VicoDevice*> foundDevices = devReg.getDevices();
	quint8 nDevices = foundDevices.size();
	QString serialNumber = json[API_DATAGRAM_DEVICE].toString().toUpper();
	bool deviceFound = false;
	QJsonObject replyData;

    // Iterate through all devices (serialNumber+Interface combination)
    for(quint8 i=0;i<nDevices;i++){
        device = foundDevices.at(i);
        QString deviceID = device->getDeviceId();
        // Remove devices with given serialNumber
        if (deviceID == serialNumber) {
            devReg.removeDevice(device);
            deviceFound = true;
			}
		}

	if (!deviceFound) {
		replyData[API_STATUSTYPE] = VICO_NO_DEVICE_WITH_ID_CONNECTED_ERROR;
	} else {
		replyData[API_STATUSTYPE] = VICO_SUCCESS;
	}

	query->answerQuery(replyData);
}

void QueryServer::handlegetDaemonConnection(ClientQuery *query, const QJsonObject &json) {

	QString serialNumber = json[API_DATAGRAM_DEVICE].toString().toUpper();
	VICOStatusType statusCode = VICO_SUCCESS;
	QHostAddress ipAddress = DeviceRegistry::getInstance().getDeviceIPFromDaemon(serialNumber, &statusCode);
	QJsonObject replyData;
	replyData[API_STATUSTYPE] = statusCode;
	
	if (statusCode != VICO_SUCCESS || ipAddress == QHostAddress::AnyIPv4) {
		replyData[API_DATAGRAM_DATA] = QStringLiteral("");
	} else if (!ipAddress.isNull()) {
		replyData[API_DATAGRAM_DATA] = ipAddress.toString();
	} else {
		replyData[API_STATUSTYPE] = VICO_UNDEFINED_ERROR;
		replyData[API_DATAGRAM_DATA] = QStringLiteral("");
	}
	query->answerQuery(replyData);
}

void QueryServer::handleGetPreferredInterface(ClientQuery *query, const QJsonObject &json) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;
	QJsonValue datagramDeviceId = json[API_DATAGRAM_DEVICE];
	quint64 deviceId = datagramDeviceId.toString().toULongLong(nullptr, 16);
	replyData[API_DATAGRAM_DATA] = DeviceRegistry::getInstance().getPreferredInterface(deviceId);
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleSetPreferredInterface(ClientQuery *query, const QJsonObject &json) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;
	QJsonValue datagramDeviceId = json[API_DATAGRAM_DEVICE];
	quint64 deviceId = datagramDeviceId.toString().toULongLong(nullptr, 16);
	quint8 preferredInterface = json[API_DATAGRAM_INTERFACE].toVariant().toUInt();
	if (preferredInterface == INTERFACE_UNKNOWN || preferredInterface == INTERFACE_ETHERNET_TCP || preferredInterface == INTERFACE_ETHERNET_UDP || preferredInterface == INTERFACE_USB || preferredInterface == INTERFACE_USB_HID) {
		DeviceRegistry::getInstance().setPreferredInterface(deviceId, (InterfaceType)preferredInterface);
		switch (preferredInterface)
		{
			case INTERFACE_UNKNOWN: qDebug() << "[LIB]\t[REQ]\tPreferred connection interface set to unknown."; break;
			case INTERFACE_ETHERNET_TCP: qDebug() << "[LIB]\t[REQ]\tPreferred connection interface set to TCP."; break;
			case INTERFACE_ETHERNET_UDP: qDebug() << "[LIB]\t[REQ]\tPreferred connection interface set to UDP."; break;
			case INTERFACE_USB: qDebug() << "[LIB]\t[REQ]\tPreferred connection interface set to USB."; break;
			case INTERFACE_USB_HID: qDebug() << "[LIB]\t[REQ]\tPreferred connection interface set to USB HID."; break;
			default: qCritical() << "[LIB]\t[REQ]\tRequested preferred interface not available."; break;
		}
		replyData[API_STATUSTYPE] = VICO_SUCCESS;
	}
	else {
		replyData[API_STATUSTYPE] = VICO_INVALID_ARGUMENT_ERROR;
	}
	query->answerQuery(replyData);

}

void QueryServer::handleGetLogLevel(ClientQuery *query) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;
	replyData[API_DATAGRAM_DATA] = VicoDaemon::getLogLevel();
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleSetLogLevel(ClientQuery *query, const QJsonObject &json) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;
	quint8 logLevel = json[API_DATAGRAM_LOG_LEVEL].toVariant().toUInt();
	if(VicoDaemon::setLogLevel(logLevel)) {
		replyData[API_STATUSTYPE] = VICO_SUCCESS;
	} else {
		replyData[API_STATUSTYPE] = VICO_INVALID_ARGUMENT_ERROR;
	}
	query->answerQuery(replyData);
}

void QueryServer::handleGetHostIpAddress(ClientQuery *query, const QJsonObject &json) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = 2;
	QJsonObject replyData;
	QVariant deviceIpAddressVariant = json[API_INTERNAL_DEVICE_IP_ADDRESS_KEY].toVariant();
	quint32 deviceIpAddress = deviceIpAddressVariant.toUInt();
	QHostAddress hostAddress = DeviceRegistry::getInstance().getHostAddress(deviceIpAddress);
	replyData[API_INTERNAL_HOST_IP_ADDRESS_KEY] = QJsonValue::fromVariant(hostAddress.toIPv4Address());
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleSetHostIpAddress(ClientQuery *query, const QJsonObject &json) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = 2;
	QJsonObject replyData;
	quint32 deviceIpAddressUInt32 = json[API_INTERNAL_DEVICE_IP_ADDRESS_KEY].toVariant().toUInt();
	quint32 hostIpAddressUInt32 = json[API_INTERNAL_HOST_IP_ADDRESS_KEY].toVariant().toUInt();
	DeviceRegistry::getInstance().insertHostAddress(deviceIpAddressUInt32, hostIpAddressUInt32);
	qDebug() << "[LIB]\t[REQ]\tHost IP Address set to" << QHostAddress(hostIpAddressUInt32).toString();
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleRefreshDeviceConnections(ClientQuery *query) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;
	DeviceRegistry::getInstance().refreshDeviceConnections();
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleDaemonVersion(ClientQuery *query) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonObject replyData;
	replyData[API_DATAGRAM_DATA] = QStringLiteral(VERSION);
	replyData[API_STATUSTYPE] = VICO_SUCCESS;
	query->answerQuery(replyData);
}

void QueryServer::handleDeviceByIndex(ClientQuery *query, const QJsonObject &json) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	int deviceIndex = json.value(API_INTERNAL_DEVICEINDEX).toInt();
	QJsonObject replyData;
	VicoDevice* device = nullptr;
	if (DeviceRegistry::getInstance().getDevices().count() > deviceIndex && deviceIndex >= 0) {
		device = DeviceRegistry::getInstance().getDevices().at(deviceIndex);
	}
	if (device) {
		QJsonArray deviceInfo;
		deviceInfo.append(device->getDeviceId());
		deviceInfo.append(device->getConnector()->getConnectionType());
		replyData[API_DATAGRAM_DATA] = deviceInfo;
		replyData[API_STATUSTYPE] = VICO_SUCCESS;
	}
	else {
		replyData[API_STATUSTYPE] = VICO_NO_DEVICE_WITH_ID_CONNECTED_ERROR;
	}
	query->answerQuery(replyData);
}

void QueryServer::handleDatagramQuery(ClientQuery *query, const QJsonObject &json) {
	QJsonValue datagramdeviceId = json[API_DATAGRAM_DEVICE];
	QJsonValue datagramId = json.value(API_DATAGRAM_ID);
	QJsonValue datagramSize = json.value(API_DATAGRAM_SIZE);
	QJsonValue datagramCommand = json.value(API_DATAGRAM_CMD);
	QJsonValue datagramData = json.value(API_DATAGRAM_DATA);
	// Send ID as QByteArray
	quint64 id = datagramId.toVariant().toDouble();
	queryToRequestMap[query] = id;
	uint size = datagramSize.toInt();
	requestedSize.insert(query, size);
	quint8 cmd = datagramCommand.toInt();
	quint16 data = datagramData.toInt();
	QByteArray q_id;
	q_id.resize(4);
	q_id[0] = id;
	q_id[1] = cmd;
	q_id[2] = data>>8;
	q_id[3] = data;

	// Special case write firmware
	if (id == 92) {
		q_id.resize(1028);
		QJsonObject jsonData = json.value(API_DATAGRAM_FIRMWARE).toObject();
		QVariantMap pairMap = jsonData.toVariantMap();
		if(pairMap.size() != 1024) {
			QJsonObject replyData;
			replyData[API_STATUSTYPE] = DPP_INVALID_ARGUMENT_ERROR;
			qCritical() << "[LIB]\t[REQ]\tWrong payload for write firmware found with return code:" << DPP_INVALID_ARGUMENT_ERROR;
			query->answerQuery(replyData);
			unlockMutex(getDeviceId(json));
		} else {
			for(int i=0;i<pairMap.size();i++) {
				QString dataKey = API_DATAGRAM_FIRMWARE+QString::number(i);
				q_id[4+i] = pairMap.value(dataKey, 0u).toUInt();
			}
		}
	}
	// Special case delete firmware takes up to 120s
	else if (id == 91) {
		TimeoutThread::MUTEX_LOCK_TIMEOUT = DELETE_FIRMWARE_TIMEOUT;
	}
	// Event Scope Trigger Timeout ID83 can be up to 16s. Hence When Event Scope Get ID84 is requested, increase the TIMEOUT
	else if (id == 84) {
		TimeoutThread::MUTEX_LOCK_TIMEOUT = EVENT_SCOPE_TIMEOUT;
	}
	else {
		TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	}
	quint64 deviceId = datagramdeviceId.toString().toULongLong(nullptr, 16);
	VicoDevice* targetDevice = DeviceRegistry::getInstance().getDeviceById(deviceId);

	// Connect only if the target device exists
	if (targetDevice != nullptr) {
		queryToDeviceMap[targetDevice] = query;
		connect(targetDevice, &VicoDevice::msgReceived, this, &QueryServer::getVicoReply, Qt::DirectConnection);
		targetDevice->sendMsg(q_id, requestedSize.value(query, -1), TimeoutThread::MUTEX_LOCK_TIMEOUT);

	}
	else {
		QJsonObject replyData;
		replyData[API_STATUSTYPE] = DPP_NO_DEVICE_WITH_ID_CONNECTED_ERROR;
		query->answerQuery(replyData);
		unlockMutex(getDeviceId(json));
	}
}

void QueryServer::handleMCUDatagramQuery(ClientQuery *query, const QJsonObject &json) {
	TimeoutThread::MUTEX_LOCK_TIMEOUT = DEFAULT_TIMEOUT;
	QJsonValue datagramMCUEnabled = json.value(API_MCU_DATAGRAM_ENABLE);
	QJsonValue datagramdeviceId = json[API_DATAGRAM_DEVICE];
	QJsonValue datagramVer = json.value(API_MCU_PROTOCOL_VERSION);
	QJsonValue datagramCommand = json.value(API_DATAGRAM_CMD);
	QJsonValue datagramSize = json.value(API_DATAGRAM_SIZE);
	QJsonValue datagramCrc = json.value(API_MCU_CHECKSUM);

	QVariantMap datagramData = json.value(API_DATAGRAM_DATA).toObject().toVariantMap();
	quint8 ver = datagramVer.toVariant().toUInt();
	quint8 cix = datagramCommand.toVariant().toUInt();
	quint8 cl = datagramSize.toVariant().toUInt();
	quint16 crc = datagramCrc.toVariant().toUInt();
	QByteArray q_id;
	q_id.resize(5+datagramData.size());
	q_id[0] = ver;
	q_id[1] = cix;
	q_id[2] = cl;
	q_id[3] = (crc & 0xFF00) >> 8;
	q_id[4] = crc & 0x00FF;

	for(int i=0;i<datagramData.size();i++) {
		QString dataKey = API_DATAGRAM_DATA+QString::number(i);
		q_id[5+i] = datagramData.value(dataKey, 0u).toUInt();
	}

	quint64 deviceId = datagramdeviceId.toString().toULongLong(nullptr, 16);
	VicoDevice* targetDevice = DeviceRegistry::getInstance().getDeviceById(deviceId);
	// If the targeted device is a DV, the MCU communication will go over UART, which needs the DPP MCU Passthrough suffix
	if (targetDevice->getDeviceType() == VICO_DV) {
		QByteArray mcuPassThrough;
		q_id.insert(0, 0x01);
		q_id.insert(0, MCU_PASS_THROUGH_ID);
		queryToRequestMap[query] = MCU_PASS_THROUGH_ID;
		requestedSize.insert(query, getMCUDatagramResponseSize(json, true));
	}
	// If the targeted device is an AV, communication is directly done with the MCU
	else {
		queryToRequestMap[query] = cix;
		requestedSize.insert(query, getMCUDatagramResponseSize(json, false));
	}

	if (targetDevice != nullptr) {
		queryToDeviceMap[targetDevice] = query;
		connect(targetDevice, &VicoDevice::msgReceived, this, &QueryServer::getVicoReply, Qt::DirectConnection);
		targetDevice->sendMsg(q_id, requestedSize.value(query, -1), TimeoutThread::MUTEX_LOCK_TIMEOUT);

	} else {
		QJsonObject replyData;
		replyData[API_STATUSTYPE] = MCU_NO_DEVICE_WITH_ID_CONNECTED_ERROR;
		query->answerQuery(replyData);
		unlockMutex(getDeviceId(json));
	}
}

uint QueryServer::getMCUDatagramResponseSize(const QJsonObject & json, bool usePassThrough) const {
	quint8 passThroughBytes = 2;
	quint8 standardBytes = 6;
	quint8 variableBytes = 0;
	QJsonValue datagramLength = json.value(API_MCU_LENGTH);
	variableBytes = datagramLength.toVariant().toUInt();
	uint result = standardBytes  + variableBytes;
	if (usePassThrough) {
		result += passThroughBytes;
	}
	return result;
}

QueryServer::RequestType QueryServer::getRequestType(const QJsonObject &json) const {
	QJsonValue mcuDatagramEnabled = json.value(API_MCU_DATAGRAM_ENABLE);
	QJsonValue datagramId = json.value(API_DATAGRAM_ID);
	QJsonValue datagramRequest = json.value(API_DATAGRAM_REQUEST);
	if(mcuDatagramEnabled.toBool(false)) {
		return RequestType::MCUDatagram;
	} else if (datagramId.isNull() || datagramId.isUndefined()) {
		QString request = datagramRequest.toString();
		if (request == QString(API_INTERNAL_STOPTCP)) {
			return RequestType::StopTCP;
		} else if(request == QString(API_INTERNAL_STOPUDP)) {
			return RequestType::StopUDP;
		} else if (request == QString(API_INTERNAL_SCANTCP)) {
			return RequestType::ScanTCP;
		} else if (request == QString(API_INTERNAL_SCANUDP)) {
			return RequestType::ScanUDP;
		} else if (request == QString(API_INTERNAL_SCANUSB)) {
			return RequestType::ScanUSB;
		} else if (request == QString(API_INTERNAL_SCANUSBHID)) {
			return RequestType::ScanUSBHID;
		} else if (request == QString(API_INTERNAL_GETNUMBERDEVICES)) {
			return RequestType::NumberOfDevices;
		} else if (request == QString(API_INTERNAL_GET_PREFERRED_INTERFACE)) {
			return RequestType::GetPreferredInterface;
		} else if (request == QString(API_INTERNAL_SET_PREFERRED_INTERFACE)) {
			return RequestType::SetPreferredInterface;
		} else if (request == QString(API_INTERNAL_GETHOSTIPADDRESS)) {
			return RequestType::GetHostIpAddress;
		} else if (request == QString(API_INTERNAL_SETHOSTIPADDRESS)) {
			return RequestType::SetHostIpAddress;
		} else if (request == QString(API_INTERNAL_GET_LOG_LEVEL)) {
			return RequestType::GetLogLevel;
		} else if (request == QString(API_INTERNAL_SET_LOG_LEVEL)) {
			return RequestType::SetLogLevel;
		} else if (request == QString(API_INTERNAL_REFRESHDEVICECONNECTIONS)) {
			return RequestType::RefreshDeviceConnections;
		} else if (request == QString(API_INTERNAL_GETDEVICEINFOBYINDEX)) {
			return RequestType::DeviceByIndex;
		} else if (request == QString(API_INTERNAL_DAEMONVERSION)) {
			return RequestType::DaemonVersion;
		} else if (request == QString(API_INTERNAL_REMOVE_DEVICE_CONNECTION)) {
			return RequestType::RemoveDeviceConnection;
		} else if (request == QString(API_INTERNAL_DAEMON_CONNECTION)) {
			return RequestType::getDaemonConnection;
		} else {
			return RequestType::Other;
		}
	} else {
		return RequestType::Datagram;
	}
}

QVariant QueryServer::getRequestAPILog(const QJsonObject &json) const {
	QJsonValue mcuDatagramEnabled = json.value(API_MCU_DATAGRAM_ENABLE);
	QueryServer::RequestType requestType = getRequestType(json);

	if(mcuDatagramEnabled.toBool(false)) {
		int cmd = json.value(API_DATAGRAM_CMD).toInt(-1);
		if(0x00u == cmd) {
			return QStringLiteral("swPkgGetActive");
		} else if(0x01u == cmd) {
			return QStringLiteral("flashRead");
		} else if(0x02u == cmd) {
			return QStringLiteral("flashWriteSessionStart");
		} else if(0x03u == cmd) {
			return QStringLiteral("flashWriteSessionExit");
		} else if(0x04u == cmd) {
			return QStringLiteral("flashWriteSessionReset");
		} else if(0x05u == cmd) {
			return QStringLiteral("flashWriteSessionData");
		} else if(0x0Au == cmd) {
			return QStringLiteral("blGetSession");
		} else if(0x0Bu == cmd) {
			return QStringLiteral("blGetReason");
		} else if(0x0Eu == cmd) {
			return QStringLiteral("swPkgStartApplication");
		} else if(0x0Fu == cmd) {
			return QStringLiteral("swPkgStartBootloader");
		} else if(0x10u == cmd) {
			return QStringLiteral("liveInfo1VICO");
		} else if(0x11u == cmd) {
			return QStringLiteral("liveInfo2VICO");
		} else if(0x12u == cmd) {
			return QStringLiteral("liveInfoBoundariesVICO");
		} else if(0x15u == cmd) {
			return QStringLiteral("liveInfo1VIAMP");
		} else if(0x16u == cmd) {
			return QStringLiteral("liveInfo2VIAMP");
		} else if(0x17u == cmd) {
			return QStringLiteral("liveInfoBoundariesVIAMP");
		} else if(0x19u == cmd) {
			return QStringLiteral("devInfo1Bootloader");
		} else if(0x1Au == cmd) {
			return QStringLiteral("devInfo1VICO");
		} else if(0x1Bu == cmd) {
			return QStringLiteral("devInfo2VICO");
		} else if(0x1Du == cmd) {
			return QStringLiteral("devInfo1VIAMP");
		} else if(0x20u == cmd) {
			return QStringLiteral("setMode");
		} else if(0x25u == cmd) {
			return QStringLiteral("resetMCU");
		} else if(0x26u == cmd) {
			return QStringLiteral("resetFPGA");
		} else if(0x30u == cmd) {
			return QStringLiteral("getTemp");
		} else if(0x31u == cmd) {
			return QStringLiteral("setTemp");
		} else if(0x36u == cmd) {
			return QStringLiteral("setRdy");
		} else if(0x37u == cmd) {
			return QStringLiteral("setARdy");
		} else if(0x38u == cmd) {
			return QStringLiteral("getRdy");
		} else if(0x39u == cmd) {
			return QStringLiteral("getARdy");
		} else if(0x040u == cmd) {
			return QStringLiteral("getEEP");
		} else if(0x41u == cmd) {
			return QStringLiteral("setEEP");
		} else if(0x50u == cmd) {
			return QStringLiteral("twi");
		} else if(0x60u == cmd) {
			return QStringLiteral("getIO");
		} else if(0x61u == cmd) {
			return QStringLiteral("setIO");
		} else if(0x62u == cmd) {
			return QStringLiteral("getADC");
		} else if(0x63u == cmd) {
			return QStringLiteral("setDAC");
		} else if(0x64u == cmd) {
			return QStringLiteral("setIMax");
		} else if(0x65u == cmd) {
			return QStringLiteral("setUMax");
		} else if(0x6Eu == cmd) {
			return QStringLiteral("dbgClp");
		} else if(0x6Fu == cmd) {
			return QStringLiteral("dbgClpExt");
		}
		return QVariant();

	} else if (RequestType::StopTCP == requestType) {
		return QStringLiteral("cancelTcpScan");
	} else if (RequestType::StopUDP == requestType) {
		return QStringLiteral("cancelUdpScan");
	} else if (RequestType::ScanTCP == requestType) {
		return QStringLiteral("scanTcpDevices");
	} else if (RequestType::ScanUDP == requestType) {
		return QStringLiteral("scanUdpDevices");
	} else if (RequestType::ScanUSB == requestType) {
		return QStringLiteral("scanUsbDevices");
	} else if (RequestType::ScanUSBHID == requestType) {
		return QStringLiteral("scanUsbHIDDevices");
	} else if (RequestType::NumberOfDevices == requestType) {
		return QStringLiteral("getNumberOfDevices");
	} else if (RequestType::GetPreferredInterface == requestType) {
		return QStringLiteral("getPreferredInterface");
	} else if (RequestType::SetPreferredInterface == requestType) {
		return QStringLiteral("setPreferredInterface");
	} else if (RequestType::GetHostIpAddress == requestType) {
		return QStringLiteral("getHostIpAddress");
	} else if (RequestType::SetHostIpAddress == requestType) {
		return QStringLiteral("setHostIpAddress");
	} else if (RequestType::GetLogLevel == requestType) {
		return QStringLiteral("getLogLevel");
	} else if (RequestType::SetLogLevel == requestType) {
		return QStringLiteral("setLogLevel");
	} else if (RequestType::RefreshDeviceConnections == requestType) {
		return QStringLiteral("refreshDeviceConnections");
	} else if (RequestType::DeviceByIndex == requestType) {
		return QStringLiteral("getDeviceInfoByIndex");
	} else if (RequestType::DaemonVersion == requestType) {
		return QStringLiteral("getDaemonVersion");
	} else if (RequestType::RemoveDeviceConnection == requestType) {
		return QStringLiteral("RemoveDeviceConnection");
	} else if (RequestType::Datagram == requestType) {
		int datagramId = json.value(API_DATAGRAM_ID).toInt(-1);
		int cmd = json.value(API_DATAGRAM_CMD).toInt(-1);
		int data = json.value(API_DATAGRAM_DATA).toInt(-1);
		if(0 == datagramId) {
			return QStringLiteral("startRun");
		} else if(1 == datagramId) {
			return QStringLiteral("stopRun");
		} else if(1 == datagramId) {
			return QStringLiteral("stopRun");
		} else if(2 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getStopCondition");
		} else if(2 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setStopCondition");
		} else if(5 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getRunStatus");
		} else if(6 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getRunRealtime");
		} else if(8 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getRunLivetime");
		} else if(10 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getRunOutputCounts");
		} else if(12 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getRunInputCounts");
		} else if(14 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getRunOutputCountRate");
		} else if(16 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getRunInputCountRate");
		} else if(18 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getRunStatistics");
		} else if(19 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getMCADataRaw");
		} else if(19 == datagramId && API_DATAGRAM_CMD_READ == cmd) { // TODO no difference to getMCADataRaw
			return QStringLiteral("getMCADataUInt32");
		} else if(19 == datagramId && API_DATAGRAM_CMD_READ == cmd) { // TODO no difference to getMCADataRaw
			return QStringLiteral("getMCAData1BytePerBin");
		} else if(19 == datagramId && API_DATAGRAM_CMD_READ == cmd) { // TODO no difference to getMCADataRaw
			return QStringLiteral("getMCAData2BytesPerBin");
		} else if(19 == datagramId && API_DATAGRAM_CMD_READ == cmd) { // TODO no difference to getMCADataRaw
			return QStringLiteral("getMCAData3BytesPerBin");
		} else if(20 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setMCANumberOfBins");
		} else if(20 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getMCANumberOfBins");
		} else if(21 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setMCABytesPerBin");
		} else if(21 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getMCABytesPerBin");
		} else if(32 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setFastFilterPeakingTime");
		} else if(32 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getFastFilterPeakingTime");
		} else if(33 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getFastFilterGapTime");
		} else if(34 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getMediumFilterPeakingTime");
		} else if(35 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getMediumFilterGapTime");
		} else if(36 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setSlowFilterPeakingTime");
		} else if(36 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getSlowFilterPeakingTime");
		} else if(37 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setSlowFilterGapTime");
		} else if(37 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getSlowFilterGapTime");
		} else if(38 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setFastFilterTriggerThreshold");
		} else if(38 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getFastFilterTriggerThreshold");
		} else if(39 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setMediumFilterTriggerThreshold");
		} else if(39 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getMediumFilterTriggerThreshold");
		} else if(40 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 1 == data) {
			return QStringLiteral("enableMediumFilterPulseDetection");
		} else if(40 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 0 == data) {
			return QStringLiteral("disableMediumFilterPulseDetection");
		} else if(40 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("isMediumFilterPulseDetectionEnabled");
		} else if(41 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setFastFilterMaxWidth");
		} else if(41 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getFastFilterMaxWidth");
		} else if(42 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setMediumFilterMaxWidth");
		} else if(42 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getMediumFilterMaxWidth");
		} else if(43 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setResetInhibitTime");
		} else if(43 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getResetInhibitTime");
		} else if(44 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setBaselineAverageLength");
		} else if(44 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getBaselineAverageLength");
		} else if(45 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setBaselineTrim");
		} else if(45 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getBaselineTrim");
		} else if(46 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 1 == data) {
			return QStringLiteral("enableBaselineCorrection");
		} else if(46 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 0 == data) {
			return QStringLiteral("disableBaselineCorrection");
		} else if(46 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("isBaselineCorrectionEnabled");
		} else if(47 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setDigitalEnergyGain");
		} else if(47 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getDigitalEnergyGain");
		} else if(48 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setDigitalEnergyOffset");
		} else if(48 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getDigitalEnergyOffset");
		} else if(49 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 1 == data) {
			return QStringLiteral("enableDynamicReset");
		} else if(49 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 0 == data) {
			return QStringLiteral("disableDynamicReset");
		} else if(49 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("isDynamicResetEnabled");
		} else if(50 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setDynamicResetThreshold");
		} else if(50 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getDynamicResetThreshold");
		} else if(51 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setDynamicResetDuration");
		} else if(51 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getDynamicResetDuration");
		} else if(56 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setResetDetection");
		} else if(56 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getResetDetection");
		} else if(64 == datagramId && API_DATAGRAM_CMD_READ == cmd && 0 == data) {
			return QStringLiteral("loadDefaultParameterSet");
		} else if(64 == datagramId && API_DATAGRAM_CMD_READ == cmd && 1 == data) {
			return QStringLiteral("loadParameterSet");
		} else if(65 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 0 == data) {
			return QStringLiteral("saveDefaultParameterSet");
		} else if(65 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 1 == data) {
			return QStringLiteral("saveParameterSet");
		} else if(66 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getFirmwareVersion");
		} else if(72 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getMCUStatusInfo");
		} else if(73 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getBoardTemperature");
		} else if(74 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 1 == data) {
			return QStringLiteral("enableAnalogHardwarePowerdown");
		} else if(74 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 0 == data) {
			return QStringLiteral("disableAnalogHardwarePowerdown");
		} else if(74 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("isAnalogHardwarePowerdownEnabled");
		} else if(75 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setClockingSpeed");
		} else if(75 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getClockingSpeed");
		} else if(79 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getAllParameters");
		} else if(80 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setEventTriggerSource");
		} else if(80 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getEventTriggerSource");
		} else if(81 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setEventTriggerValue");
		} else if(81 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getEventTriggerValue");
		} else if(82 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setEventScopeSamplingInterval");
		} else if(82 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getEventScopeSamplingInterval");
		} else if(83 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setEventScopeTriggerTimeout");
		} else if(83 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getEventScopeTriggerTimeout");
		} else if(84 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getEventScope");
		} else if(85 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("calculateEventRate");
		} else if(86 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getEventRate");
		} else if(91 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("deleteFirmware");
		} else if(92 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("writeFirmwareSection");
		} else if(93 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("readFirmwareSection");
		} else if(94 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setServiceCode");
		} else if(94 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getServiceCode");
		} else if(96 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 1 == data) {
			return QStringLiteral("enableEthernetPowerdown");
		} else if(96 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 0 == data) {
			return QStringLiteral("disableEthernetPowerdown");
		} else if(96 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("isEthernetPowerdownEnabled");
		} else if(97 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setEthernetProtocol");
		} else if(97 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getEthernetProtocol");
		} else if(98 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setEthernetSpeed");
		} else if(98 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getEthernetSpeed");
		} else if(100 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setIPAddress");
		} else if(100 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getIPAddress");
		} else if(102 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setSubnetMask");
		} else if(102 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getSubnetMask");
		} else if(104 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setGatewayIPAddress");
		} else if(104 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getGatewayIPAddress");
		} else if(106 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getEthernetPort");
		} else if(107 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("setMACAddress");
		} else if(107 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getMACAddress");
		} else if(110 == datagramId && API_DATAGRAM_CMD_WRITE == cmd) {
			return QStringLiteral("applyEthernetConfiguration");
		} else if(115 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 1 == data) {
			return QStringLiteral("enableUSBPowerdown");
		} else if(115 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 0 == data) {
			return QStringLiteral("disableUSBPowerdown");
		} else if(115 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("isUSBPowerdownEnabled");
		} else if(118 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 1 == data) {
			return QStringLiteral("enableSPIPowerdown");
		} else if(118 == datagramId && API_DATAGRAM_CMD_WRITE == cmd && 0 == data) {
			return QStringLiteral("disableSPIPowerdown");
		} else if(118 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("isSPIPowerdownEnabled");
		} else if(90 == datagramId && API_DATAGRAM_CMD_READ == cmd) {
			return QStringLiteral("getKeyRevision");
		}
		return QVariant();
	} else {
		return "unknown";
	}
}

void QueryServer::queryReceived(ClientQuery *query, const QJsonObject &json) {
	auto failureToLock = [&] {
		qCritical() << "[INTERNAL]\tTry to lock failed. Will stop connection.";
		QJsonObject replyData;
		replyData[API_STATUSTYPE] = VICO_VICOLIB_TIMEOUT_ERROR;
		query->answerQuery(replyData);
	};

	/*
	  Scanning should be blocked when:
	  - any other mutex is locked
	  (reason: scanning will erase (part) of the device registry -> currently executed request might lead to erroneous results)
	  Any other request should be blocked when:
	  - scanning is being performed
	  (reason: scanning will erase (part) of the device registry -> the request might be needing a device not present in the registry)
	 */
	RequestType requestType = getRequestType(json);
	if(requestType == RequestType::StopTCP) {
		// no mutex needed
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" << getRequestAPILog(json).toString();
		handleStopTCPQuery(query);
	} else if(requestType == RequestType::StopUDP) {
		// no mutex needed
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		handleStopUDPQuery(query);
	} else if(requestType == RequestType::DaemonVersion) {
		// no mutex needed
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		handleDaemonVersion(query);
	} else if(requestType == RequestType::ScanTCP) {
		// mutex needed
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		if(lockMutex(QStringLiteral("Scanning"), true)) {
			handleScanTCPQuery(query, json);
			unlockMutex(QStringLiteral("Scanning"), true);
		} else {
			failureToLock();
		}
	} else if(requestType == RequestType::ScanUDP) {
		// mutex needed
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		if(lockMutex(QStringLiteral("Scanning"), true)) {
			handleScanUDPQuery(query, json);
			unlockMutex(QStringLiteral("Scanning"), true);
		} else {
			failureToLock();
		}
	} else if(requestType == RequestType::ScanUSB) {
		// mutex needed
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		if(lockMutex(QStringLiteral("Scanning"), true)) {
			handleScanUSBQuery(query, json);
			unlockMutex(QStringLiteral("Scanning"), true);
		} else {
			failureToLock();
		}
	}else if(requestType == RequestType::ScanUSBHID) {
		// mutex needed
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		if(lockMutex(QStringLiteral("Scanning"), true)) {
			handleScanUSBHIDQuery(query, json);
			unlockMutex(QStringLiteral("Scanning"), true);
		} else {
			failureToLock();
		}
	} else if(requestType == RequestType::NumberOfDevices) {
		// mutex needed
		// normal timeout
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		if(lockMutex(QStringLiteral("Scanning"), true)) {
			handleNumberOfDevices(query);
			unlockMutex(QStringLiteral("Scanning"), true);
		} else {
			failureToLock();
		}
	} else if(requestType == RequestType::GetPreferredInterface) {
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		QString datagramdeviceId = getDeviceId(json);
		if(lockMutex(QStringLiteral("PreferredInterface"), true)) {
			handleGetPreferredInterface(query, json);
			unlockMutex(QStringLiteral("PreferredInterface"), true);
		} else {
			failureToLock();
		}
	} else if(requestType == RequestType::SetPreferredInterface) {
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		QString datagramdeviceId = getDeviceId(json);
		if(lockMutex(QStringLiteral("PreferredInterface"), true)) {
			handleSetPreferredInterface(query, json);
			unlockMutex(QStringLiteral("PreferredInterface"), true);
		} else {
			failureToLock();
		}
	} else if(requestType == RequestType::GetHostIpAddress) {
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		QString datagramdeviceId = getDeviceId(json);
		if(lockMutex(QStringLiteral("HostIpAddress"), true)) {
			handleGetHostIpAddress(query, json);
			unlockMutex(QStringLiteral("HostIpAddress"), true);
		} else {
			failureToLock();
		}
	} else if(requestType == RequestType::SetHostIpAddress) {
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		QString datagramdeviceId = getDeviceId(json);
		if(lockMutex(QStringLiteral("HostIpAddress"), true)) {
			handleSetHostIpAddress(query, json);
			unlockMutex(QStringLiteral("HostIpAddress"), true);
		} else {
			failureToLock();
		}
	}
	else if(requestType == RequestType::GetLogLevel) {
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		QString datagramdeviceId = getDeviceId(json);
		if(lockMutex(QStringLiteral("LogLevel"), true)) {
			handleGetLogLevel(query);
			unlockMutex(QStringLiteral("LogLevel"), true);
		} else {
			failureToLock();
		}
	}
	else if(requestType == RequestType::SetLogLevel) {
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		QString datagramdeviceId = getDeviceId(json);
		if(lockMutex(QStringLiteral("LogLevel"), true)) {
			handleSetLogLevel(query, json);
			unlockMutex(QStringLiteral("LogLevel"), true);
		} else {
			failureToLock();
		}
	}
	else if(requestType == RequestType::RefreshDeviceConnections) {
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		QString datagramdeviceId = getDeviceId(json);
		if(lockMutex(QStringLiteral("RefreshDeviceConnections"), true)) {
			handleRefreshDeviceConnections(query);
			unlockMutex(QStringLiteral("RefreshDeviceConnections"), true);
		} else {
			failureToLock();
		}
	}
	else if(requestType == RequestType::DeviceByIndex) {
		// mutex needed
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" <<getRequestAPILog(json).toString();
		if(lockMutex(QStringLiteral("Scanning"), true)) {
			handleDeviceByIndex(query, json);
			unlockMutex(QStringLiteral("Scanning"), true);
		} else {
			failureToLock();
		}
	} else if(requestType == RequestType::RemoveDeviceConnection) {
		// mutex needed
		// normal timeout
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" << getRequestAPILog(json).toString();
		if(lockMutex(QStringLiteral("RemoveDeviceConnection"), true)) {
			handleRemoveDevice(query, json);
			unlockMutex(QStringLiteral("RemoveDeviceConnection"), true);
		} else {
			failureToLock();
		}
	} else if(requestType == RequestType::getDaemonConnection) {
		// mutex needed
		// normal timeout
		qInfo().nospace().noquote() << "[LIB]\t[REQ]\t" << getRequestAPILog(json).toString();
		if(lockMutex(QStringLiteral("getDaemonConnection"), true)) {
			handlegetDaemonConnection(query, json);
			unlockMutex(QStringLiteral("getDaemonConnection"), true);
		} else {
			failureToLock();
		}
	} else if(requestType == RequestType::Datagram) {
		// mutex needed
		// normal timeout
		QString datagramdeviceId = getDeviceId(json);
		quint64 deviceId = datagramdeviceId.toULongLong(nullptr, 16);
		VicoDevice* targetDevice = DeviceRegistry::getInstance().getDeviceById(deviceId);
		QVariant apiLog = getRequestAPILog(json);
		if(apiLog.isValid())
			qInfo().nospace().noquote() << "[DEV]\t[REQ]\t" << apiLog.toString();
		if(!targetDevice) {
			QJsonObject replyData;
			qCritical() << "[LIB]\t[RES]\tNo device with requested serial number found, return code" << DPP_NO_DEVICE_WITH_ID_CONNECTED_ERROR;
			replyData[API_STATUSTYPE] = DPP_NO_DEVICE_WITH_ID_CONNECTED_ERROR;
			query->answerQuery(replyData);
		} else if(targetDevice->getDeviceType() == VICO_AV) {
			QJsonObject replyData;
			qCritical() << "[LIB]\t[RES]\tDevice cannot process the requested datagramm, return code" << DPP_COMMAND_NOT_SUPPORTED_ERROR;
			replyData[API_STATUSTYPE] = DPP_COMMAND_NOT_SUPPORTED_ERROR;
			query->answerQuery(replyData);
		}
		else if(lockMutex(datagramdeviceId)) {
			handleDatagramQuery(query, json);
		} else {
			failureToLock();
		}
	} else if(requestType == RequestType::MCUDatagram) {
		// mutex needed
		// normal timeout
		QString datagramdeviceId = getDeviceId(json);
		quint64 deviceId = datagramdeviceId.toULongLong(nullptr, 16);
		VicoDevice* targetDevice = DeviceRegistry::getInstance().getDeviceById(deviceId);
		QVariant apiLog = getRequestAPILog(json);
		if(apiLog.isValid())
			qInfo().nospace().noquote() << "[DEV]\t[REQ]\t" << apiLog.toString();
		if(!targetDevice) {
			QJsonObject replyData;
			qCritical() << "[LIB]\t[RES]\tNo device with requested serial number found, return code" << DPP_NO_DEVICE_WITH_ID_CONNECTED_ERROR;
			replyData[API_STATUSTYPE] = MCU_NO_DEVICE_WITH_ID_CONNECTED_ERROR;
			query->answerQuery(replyData);
		} else if(lockMutex(datagramdeviceId)) {
			handleMCUDatagramQuery(query, json);
		} else {
			failureToLock();
		}
	} else {
		QJsonObject replyData;
		replyData[API_STATUSTYPE] = VICO_COMMAND_NOT_SUPPORTED;
		query->answerQuery(replyData);
	}
}

quint16 QueryServer::crc16(const char *data, quint8 offset, quint8 length) const {
	if(!data || offset >= length) return 0;
	quint16 crc = 0xFFFFu;
	for(int i=0;i<length;i++) {
		quint8 target = (quint8)data[offset + i];
		crc ^= target << 8;
		for(int j=0;j<8;j++) {
			if((crc & 0x8000u) > 0) {
				crc = (crc << 1) ^ 0x1021;
			} else {
				crc = crc << 1;
			}
		}
	}
	return crc & 0xFFFF;
}

void QueryServer::getVicoReply(VicoDevice *device, const QByteArray& msg) {
	QString deviceId;
	if(device) deviceId = device->getDeviceId();
	ClientQuery* query = queryToDeviceMap.value(device, nullptr);
	QJsonObject replyData;
	responseData->append(msg);
	// Process DPP standard datagrams
	if (query && device->getDeviceType() == VICO_DV && msg.size()==4) {
		logMsg(*responseData);
		quint8 returnCode = msg[1];
		replyData[API_STATUSTYPE] = returnCode;
		// recovering the byte array uint16 value by preparing an uint16 for the first byte
		quint16 dataValue = msg[2];
		// Left-shift the first byte by 8
		dataValue = msg[2] << 8;
		// Initialize the second byte into a uint8
		quint8 thirdByte = msg[3];
		// recover the value
		dataValue = dataValue | thirdByte;
		replyData[API_DATAGRAM_DATA] = dataValue;
		query->answerQuery(replyData);
		// After finishing the request/response, reset the requested datagram to its default
		queryToRequestMap.remove(query);
		responseData->clear();
		requestedSize.remove(query);
		// Special case Ethernet reconfigure: Removed device after ethernet reconfigure
		if (msg[0] == 110) {
			if(device && dynamic_cast<EthConnector*>(device->getConnector())) {
				DeviceRegistry::getInstance().removeDevice(device);
			}
		}
		disconnect(device, &VicoDevice::msgReceived, this, &QueryServer::getVicoReply);
		unlockMutex(deviceId);
	}
	else if(query) {
		// Process MCU datagrams from AV (direct communication)
		if(device->getDeviceType() == VICO_AV) {
			quint8 ver = msg[0];
			replyData[API_MCU_PROTOCOL_VERSION] = ver;
			quint8 rc = msg[5];
			replyData[API_STATUSTYPE] = rc;
			quint8 cl = msg[2];
			// the size of the response in MCU datagram is given in this byte
			requestedSize.insert(query, cl);
			quint16 crcByte3 = (0x00FF & msg[3]) << 8;
			quint16 crcByte4 = 0x00FF & msg[4];
			quint16 responseCrc = crcByte3 | crcByte4; // big-endian
			replyData[API_MCU_CHECKSUM] = responseCrc;

			// Reset the CRC bytes to 0 to recalculate the CRC
			QByteArray msg_calculate = msg;
			((char*) msg_calculate.data())[3] = 0;
			((char*) msg_calculate.data())[4] = 0;
			quint16 calculatedCRC = crc16(msg_calculate.data(), 0, cl);
			// Check if the calculated and responded CRC match
			if (calculatedCRC != responseCrc) {
				qCritical() << "[LIB]\t[RES]\tFound invalid checksum with return code:" << MCU_WRONG_CRC_CHECKSUM;
				replyData[API_STATUSTYPE] = MCU_WRONG_CRC_CHECKSUM;
			}

			QVariantMap dataMap;
			int i = 0;
			for(const uchar unsignedByte : *responseData) {
				if (i > 5) {
					dataMap.insert(API_DATAGRAM_DATA + QString::number(i-6), unsignedByte);
				}
				i++;
			}
			QJsonArray data;
			data.append(QJsonObject::fromVariantMap(dataMap));
			replyData[API_DATAGRAM_DATA] = data;
		}
		// Process MCU datagrams from DV when the query is based on an mcu passthrough
		else if(device->getDeviceType() == VICO_DV && queryToRequestMap[query] == MCU_PASS_THROUGH_ID) {
			// Translate MCU Passthrough errors to VICOLib error codes
			quint8 status = msg[1];
			quint8 rc = msg[7];
			if (status == 1) {
				qCritical() << "[LIB]\t[RES]\tUART timeout error occured with return code:" << DPP_UART_TIMEOUT_ERROR;
				replyData[API_STATUSTYPE] = DPP_UART_TIMEOUT_ERROR;
			}
			else if (status == 2) {
				qCritical() << "[LIB]\t[RES]\tNo access to internal memory with return code:" << DPP_NO_ACCESS_TO_INTERNAL_MEMORY_ERROR;
				replyData[API_STATUSTYPE] = DPP_NO_ACCESS_TO_INTERNAL_MEMORY_ERROR;
			}
			else if (status == 3) {
				qCritical() << "[LIB]\t[RES]\tIncomplete UART command with return code:" << DPP_INCOMPLETE_UART_COMMAND_ERROR;
				replyData[API_STATUSTYPE] = DPP_INCOMPLETE_UART_COMMAND_ERROR;
			}
			else {
				replyData[API_STATUSTYPE] = rc;
			}

			quint8 ver = msg[2];
			quint8 cl = msg[4];
			// the size of the response in MCU datagram is given in this byte
			// add the extra 2 bytes in the case of DV (MCU Passthrough)
			requestedSize.insert(query, cl + 2);
			replyData[API_MCU_PROTOCOL_VERSION] = ver;
			quint16 crcByte5 = (0x00FF & msg[5]) << 8;
			quint16 crcByte6 = 0x00FF & msg[6];
			quint16 responseCrc = crcByte5 | crcByte6; // big-endian
			replyData[API_MCU_CHECKSUM] = responseCrc;
			
			// Reset the CRC bytes to 0 to recalculate the CRC
			QByteArray msg_calculate = msg;
			((char*) msg_calculate.data())[5] = 0;
			((char*) msg_calculate.data())[6] = 0;
			// Ignore the 2 DPP passthrough bytes
			quint16 calculatedCRC = crc16(msg_calculate.data(), 2, cl);
			// Check if the calculated and responded CRC match
			if (calculatedCRC != responseCrc) {
				qCritical() << "[LIB]\t[RES]\tFound invalid checksum with return code:" << MCU_WRONG_CRC_CHECKSUM;
				replyData[API_STATUSTYPE] = MCU_WRONG_CRC_CHECKSUM;
			}
			QVariantMap dataMap;
			int i = 0;
			for(const uchar unsignedByte : *responseData) {
				if (i > 7) {
					dataMap.insert(API_DATAGRAM_DATA + QString::number(i-8), unsignedByte);
				}
				i++;
			}
			QJsonArray data;
			data.append(QJsonObject::fromVariantMap(dataMap));
			replyData[API_DATAGRAM_DATA] = data;
		}
		// Process non-std DPP datagrams MCARead / EventScope
		else if (device->getDeviceType() == VICO_DV && (queryToRequestMap[query] == 19 || queryToRequestMap[query] == 84)) {
			int index = 0;
			QJsonArray data;
			uchar value = 0;
			uint count = 0;
			bool isTriggerTimeout = true;
			for(const uchar unsignedByte : *responseData) {
				// For EventScope 84 if the byte array only contains 0, it means that trigger timeout occured
				if (queryToRequestMap[query] == 84 && uint(unsignedByte) != 0) {
					isTriggerTimeout = false;
				}

				if(index == 0 && index != responseData->size() - 1) {
					value = unsignedByte;
					count = 1;
				} else if(index == 0 && index == responseData->size() - 1) {
					QVariantMap map;
					map.insert(QStringLiteral("value"), uint(unsignedByte));
					map.insert(QStringLiteral("count"), 1);
					data.append(QJsonObject::fromVariantMap(map));
				} else if(index == responseData->size() - 1) {
					if(value == unsignedByte) {
						QVariantMap map;
						map.insert(QStringLiteral("value"), uint(value));
						map.insert(QStringLiteral("count"), count+1);
						data.append(QJsonObject::fromVariantMap(map));
					} else {
						QVariantMap map;
						map.insert(QStringLiteral("value"), uint(value));
						map.insert(QStringLiteral("count"), count);
						data.append(QJsonObject::fromVariantMap(map));
						map.clear();
						map.insert(QStringLiteral("value"), uint(unsignedByte));
						map.insert(QStringLiteral("count"), 1);
						data.append(QJsonObject::fromVariantMap(map));
					}
				} else if(value == unsignedByte) {
					count++;
				} else {
					QVariantMap map;
					map.insert(QStringLiteral("value"), uint(value));
					map.insert(QStringLiteral("count"), count);
					data.append(QJsonObject::fromVariantMap(map));
					value = unsignedByte;
					count = 1;
				}
				index++;
			}
			// For EventScope if the byte array only contains 0, it means that trigger timeout occured
			if (queryToRequestMap[query] == 84 && isTriggerTimeout) {
				qCritical() << "[LIB]\t[RES]\tEventScope byte array empty, return code:" << DPP_TIMEOUT_ERROR;
				replyData[API_STATUSTYPE] = DPP_TIMEOUT_ERROR;
			}
			else {
				replyData[API_STATUSTYPE] = DPP_SUCCESS;
			}

			replyData[API_MCA] = data;
		}
		// Process non-std DPP datagrams run statistics / read all parameters
		else if (device->getDeviceType() == VICO_DV && (queryToRequestMap[query] == 18 || queryToRequestMap[query] == 79)) {
			QVariantMap dataMap;
			int i = 0;
			for(const uchar unsignedByte : *responseData) {
				dataMap.insert(API_DATAGRAM_DATA + QString::number(i), unsignedByte);
				i++;
			}
			QJsonArray data;
			data.append(QJsonObject::fromVariantMap(dataMap));
			replyData[API_DATAGRAM_DATA] = data;
		}
		// Process non-std DPP datagram read firmware
		else if (device->getDeviceType() == VICO_DV && (queryToRequestMap[query] == 93)) {
			quint8 returnCode = msg[1];
			replyData[API_STATUSTYPE] = returnCode;
			// recovering the segment number value by preparing an uint16 for the first byte
			quint16 dataValue = msg[2];
			dataValue = msg[2] << 8;
			quint8 thirdByte = msg[3];
			dataValue = dataValue | thirdByte;
			replyData[API_DATAGRAM_DATA] = dataValue;

			QVariantMap dataMap;
			int i = 0;
			if (responseData->size() < 1028) {
				qCritical() << "[LIB]\t[RES]\tInvalid data length returned with return code:" << DPP_INVALID_RESPONSE_DATA_ERROR;
				replyData[API_STATUSTYPE] = DPP_INVALID_RESPONSE_DATA_ERROR;
			}
			else {
				for(const uchar unsignedByte : *responseData) {
					// The four default dg bytes aren't the fw
					if (i>3) {
						dataMap.insert(API_DATAGRAM_FIRMWARE + QString::number(i-4), unsignedByte);
					}
					i++;
				}
			}
			replyData[API_DATAGRAM_FIRMWARE] = QJsonObject::fromVariantMap(dataMap);
		}
		// We have the full data for non-standard datagrams
		if (responseData->size() == requestedSize.value(query, -1)) {
			logMsg(*responseData);
			query->answerQuery(replyData);
			// After finishing the request/response, reset the requested datagram to its default
			queryToRequestMap.remove(query);
			requestedSize.remove(query);
			responseData->clear();
			disconnect(device, &VicoDevice::msgReceived, this, &QueryServer::getVicoReply);
			unlockMutex(deviceId);
		}
		else if (responseData->size() < requestedSize.value(query, -1)) {
			// Response not done due to incomplete response, typically for Ethernet connection
		}
		else {
			logMsg(*responseData);
			replyData[API_STATUSTYPE] = VICO_INVALID_RESPONSE_DATA_ERROR;
			qCritical() << "[LIB]\t[RES]\tInvalid response due to overly size responded: Requested size" << requestedSize.value(query, -1) << "and response data was " << responseData->size() << "with return code" << VICO_INVALID_RESPONSE_DATA_ERROR;
			query->answerQuery(replyData);
			// After finishing the request/response, reset the requested datagram to its default
			queryToRequestMap.remove(query);
			requestedSize.remove(query);
			responseData->clear();
			disconnect(device, &VicoDevice::msgReceived, this, &QueryServer::getVicoReply);
			unlockMutex(deviceId);

		}
	} else if(query && device) {
		qCritical() << "[LIB]\t[RES]\tQuery was not found with return code:" << VICO_INVALID_RESPONSE_DATA_ERROR;
		replyData[API_STATUSTYPE] = VICO_INVALID_RESPONSE_DATA_ERROR;
		query->answerQuery(replyData);
		responseData->clear();
		disconnect(device, &VicoDevice::msgReceived, this, &QueryServer::getVicoReply);
		unlockMutex(deviceId);
	} else if(query && !device) {
		qCritical() << "[LIB]\t[RES]\tQuery was not found with return code:" << VICO_INVALID_RESPONSE_DATA_ERROR;
		responseData->clear();
	} else if(!query && device) {
		qCritical() << "[LIB]\t[RES]\tQuery was not found with return code:" << VICO_INVALID_RESPONSE_DATA_ERROR;
		responseData->clear();
		disconnect(device, &VicoDevice::msgReceived, this, &QueryServer::getVicoReply);
		unlockMutex(deviceId);
	} else {
		qCritical() << "[LIB]\t[RES]\tQuery was not found with return code:" << VICO_INVALID_RESPONSE_DATA_ERROR;
	}
}

void QueryServer::logMsg(const QByteArray& msg) {
	QString msgOut (QStringLiteral("[DEV]\t[RES]\t["));
	for (int i = 0; i < msg.size(); ++i) {
		msgOut.append(QString::number((quint8)msg[i]));
		if (i != msg.size()-1) {
			msgOut.append(QStringLiteral(","));
		}
	}
	msgOut.append(QStringLiteral("]"));
	qDebug().noquote() << msgOut;
}


void QueryServer::clientError(ClientQuery *client) {
	Q_UNUSED(client)
	emit logMessage(QLatin1String("Error from a call."));
}

void QueryServer::onTimeout(QString id) {
	VicoDevice *device = DeviceRegistry::getInstance().getDeviceById(id.toULongLong(nullptr, 16));
	if (device != nullptr) {
		ClientQuery *query =  queryToDeviceMap[device];
		if (query != nullptr) {
			QJsonObject replyData;
			replyData[API_STATUSTYPE] = VICO_VICOLIB_TIMEOUT_ERROR;
			qCritical() << "[LIB]\t[RES]\tDisconnected due to timeout of id" << id << "with return code" << VICO_VICOLIB_TIMEOUT_ERROR;
			query->answerQuery(replyData);
			queryToDeviceMap.remove(device);
		}
	}
	unlockMutex(id);
}

void QueryServer::stopServer() {
	for(ClientQuery *query : m_clients) {
		query->disconnectFromClient();
		query->deleteLater();
	}
	close();
}

void QueryServer::clientDisconnected(ClientQuery *client) {
	m_clients.removeAll(client);
	client->deleteLater();
}

TimeoutThread::TimeoutThread(QObject *parent) : QThread(parent) {
	connect(this, &TimeoutThread::forceStop, this, &TimeoutThread::onForceStop, Qt::ConnectionType::DirectConnection);
}

void TimeoutThread::onMutexLocked(QString id) {
	QMutexLocker locker(&lock);
	timeouts.insert(id.toUpper(), QDateTime::currentSecsSinceEpoch());
}

void TimeoutThread::onMutexUnlocked(QString id) {
	QMutexLocker locker(&lock);
	timeouts.remove(id.toUpper());
}

void TimeoutThread::onForceStop() {
	QMutexLocker locker(&stopLock);
	stop = true;
}

void TimeoutThread::run() {
	forever {
		qint64 currentTime = QDateTime::currentSecsSinceEpoch();
		QList<QString> ids;
		{
			QMutexLocker locker(&lock);
			for(QString id : timeouts.keys()) {
				qint64 delta = currentTime - timeouts[id];
				if(delta > TimeoutThread::MUTEX_LOCK_TIMEOUT) {
					ids.append(id);
				}
			}
		}
		for (QString id : ids) {
			emit timeout(id);
		}

		{
			QMutexLocker locker(&stopLock);
			if(stop) return;
		}
		QThread::msleep(50);
	}
}
