#include "UsbHidScan.h"
#include "UsbHidConnector.h"

UsbHidScan::UsbHidScan(QObject *parent) : QObject(parent){
}


void UsbHidScan::createUsbHidDevice()
{
	hid_device* handle;
	bool deviceFound = false;
	VicoDevice* newDevice = DeviceRegistry::getInstance().createVICODevice();
	// We handle adding the USB device asynchronously
	QTimer timer;
	timer.setSingleShot(true);
	QEventLoop loop;
	connect(&DeviceRegistry::getInstance(), &DeviceRegistry::deviceFound, &loop, [&] {
		deviceFound = true;
		loop.quit();
	});
	connect( &timer, &QTimer::timeout, &loop, &QEventLoop::quit );
	UsbHidConnector *usbHidConnector = new UsbHidConnector(handle, newDevice);
	if (usbHidConnector->handle != nullptr) {
		DeviceRegistry::getInstance().addDevice(newDevice, usbHidConnector);
		// TODO Timer does not yet quit when deviceFound is emitted
		timer.start(1000);
		if (!deviceFound) {
			loop.exec();
			timer.stop();
		}
		if(deviceFound) { //replay received before timer
			qDebug() << "[HID]\tUSB HID connected.";
		}
		else {
			qDebug("[HID]\tUSB HID timeout.");
		}
	}
}


const QList<QString> UsbHidScan::start()
{
	// Each time a scanning is started, remove all previously scanned devices
	DeviceRegistry::getInstance().removeDevicesOfConnection(INTERFACE_USB_HID);
	createUsbHidDevice();
	// QueryServer gets the signal
	emit scanFinished();
	return usb_strings;
}

const QList<QString> UsbHidScan::getUsbHidDevices()
{
	return usb_strings;
}
