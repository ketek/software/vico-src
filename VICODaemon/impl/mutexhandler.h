#ifndef MUTEXTHANDLER_H
#define MUTEXTHANDLER_H

#include <QObject>
#include <QMutex>
#include <QSemaphore>
#include "DeviceRegistry.h"

/**
 * @brief The MutexHandler class takes care of a proper execution of incoming requests to the device. Only one request per time must be possible.
 */
class MutexHandler : public QObject
{
	Q_OBJECT
public:
	explicit MutexHandler(QObject *parent = nullptr);
	~MutexHandler();

	bool lockMutex(QString id, int tryTimeout = 500, bool global = false);
	bool unlockMutex(QString id, bool global = false);
	QMutex* getMutex(QString id);

private slots:
	void onDeviceNumberChanged();
signals:

private:
	QMutex globalMutex;
	QMap<QString, QMutex*> mutexes;
	QSemaphore resources;
	int resourceCount = 0;

	int getResourceCount();
	bool lockGlobalResource(int tryTimeout = 500);
	void unlockGlobalResource();
	bool lockResource(int count = 1, int tryTimeout = 500);
	void unlockResource(int count = 1);
};

#endif // MUTEXTHANDLER_H
