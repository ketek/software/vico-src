TEMPLATE = app

QT = core service network

TARGET = VICODaemon
VERSION = 2.4.0.0

CONFIG += c++14 console
CONFIG -= app_bundle
# CONFIG(release):DEFINES += QT_NO_DEBUG_OUTPUT

# for 32-bit builds define the following variable
# DEFINES += BUILD_32BIT

# uncommenting the line below would add the debug info to the exe file
#CONFIG += force_debug_info

win32 {
    contains(DEFINES, BUILD_32BIT) {
    message("Building for Windows (32-bit)")
    }
    !contains(DEFINES, BUILD_32BIT) {
    message("Building for Windows (64-bit)")
    }
}

linux-g++ {
    message("Building for Linux (g++)")
}

linux-aarch64-gnu-g++ {
    message("Building for ARM (aarch64-gnu-g++)")
}

linux-arm-gnueabi-g++ {
    message("Building for ARM (gnueabi-g++)")
}

linux-arm-gnueabihf-g++ {
    message("Building for ARM (gnueabihf-g++)")
}

# Dependency: ftd2xx for linux and windows systems
INCLUDEPATH += $$PWD/../external-libs/ftd2xx
linux {
    INCLUDEPATH += $$PWD/../external-libs/ftd2xx/linux_inc
}
DEPENDPATH += $$PWD/../external-libs/ftd2xx
win32 {
    contains(DEFINES, BUILD_32BIT) {
        LIBS += -L$$PWD/../external-libs/ftd2xx/i386
    }
    !contains(DEFINES, BUILD_32BIT) {
        LIBS += -L$$PWD/../external-libs/ftd2xx/amd64
    }
}
linux-g++ {
    contains(DEFINES, BUILD_32BIT) {
        # TODO
	#LIBS += -L$$PWD/../external-libs/ftd2xx/i386
    }
    !contains(DEFINES, BUILD_32BIT) {
        LIBS += -L$$PWD/../external-libs/ftd2xx/linux64
    }
}
linux-aarch64-gnu-g++ {
    LIBS += -L$$PWD/../external-libs/ftd2xx/arm64
}
linux-arm-gnueabi-g++ {
    LIBS += -L$$PWD/../external-libs/ftd2xx/armel
}
linux-arm-gnueabihf-g++ {
    LIBS += -L$$PWD/../external-libs/ftd2xx/armhf
}

LIBS += -lftd2xx

# Dependency: LibFT4222 for linux and windows systems
INCLUDEPATH += $$PWD/../external-libs/LibFT4222/inc/
DEPENDPATH += $$PWD/../external-libs/LibFT4222/inc/

win32 {
    contains(DEFINES, BUILD_32BIT) {
        LIBS += -L$$PWD/../external-libs/LibFT4222/dll/i386
	LIBS += -lLibFT4222
    }
    !contains(DEFINES, BUILD_32BIT) {
        LIBS += -L$$PWD/../external-libs/LibFT4222/dll/amd64
	LIBS += -lLibFT4222-64
    }
}
linux-g++ {
    contains(DEFINES, BUILD_32BIT) {
        # TODO
	#LIBS += -L$$PWD/../external-libs/LibFT4222/lib/i386
	#LIBS += -lLibFT4222
    }
    !contains(DEFINES, BUILD_32BIT) {
        LIBS += -L$$PWD/../external-libs/LibFT4222/linux64
	LIBS += -lft4222
    }
}
linux-aarch64-gnu-g++ {
    LIBS += -L$$PWD/../external-libs/LibFT4222/arm64
    LIBS += -lft4222
}
linux-arm-gnueabi-g++ {
    LIBS += -L$$PWD/../external-libs/LibFT4222/armel
    LIBS += -lft4222
}
linux-arm-gnueabihf-g++ {
    LIBS += -L$$PWD/../external-libs/LibFT4222/armhf
    LIBS += -lft4222
}

# Dependency: HIDAPI for linux and windows systems
INCLUDEPATH += $$PWD/../external-libs/hidapi/inc/
DEPENDPATH += $$PWD/../external-libs/hidapi/inc/
win32 {
    contains(DEFINES, BUILD_32BIT) {
        LIBS += -L$$PWD/../external-libs/hidapi/dll/x86
	LIBS += -lhidapi
    }
    !contains(DEFINES, BUILD_32BIT) {
        LIBS += -L$$PWD/../external-libs/hidapi/dll/x64
	LIBS += -lhidapi
    }
}

linux-g++ {
    contains(DEFINES, BUILD_32BIT) {
        #TODO
	#LIBS += -L$$PWD/../external-libs/hidapi/lib/x86
    }
    !contains(DEFINES, BUILD_32BIT) {
        LIBS += -L$$PWD/../external-libs/hidapi/linux64
	LIBS += -lhidapi-hidraw
    }
}
linux-aarch64-gnu-g++ {
    LIBS += -L/usr/lib/aarch64-linux-gnu
    LIBS += -lhidapi-hidraw
}
linux-arm-gnueabi-g++ {
    LIBS += -L/usr/lib/arm-linux-gnueabi
    LIBS += -lhidapi-hidraw
}
linux-arm-gnueabihf-g++ {
    LIBS += -L/usr/lib/arm-linux-gnueabihf
    LIBS += -lhidapi-hidraw
}

DEFINES += QT_DEPRECATED_WARNINGS QT_ASCII_CAST_WARNINGS QT_USE_QSTRINGBUILDER
DEFINES += "TARGET=\\\"$$TARGET\\\""
DEFINES += "VERSION=\\\"$$VERSION\\\""

HEADERS += \
        impl/ClientQuery.h \
	impl/DeviceRegistry.h \
	impl/EthConnector.h \
	impl/IConnector.h \
	impl/QueryServer.h \
	impl/TcpScan.h \
	impl/UdpScan.h \
	impl/UsbConnector.h \
	impl/UsbHidConnector.h \
	impl/UsbHidScan.h \
	impl/devicecollection.h \
	impl/mutexhandler.h \
	impl/queryconstants.h \
	impl/types.h \
	impl/usbscan.h \
	impl/VicoDevice.h \
	impl/vicodaemon.h

SOURCES += \
        impl/ClientQuery.cpp \
	impl/DeviceRegistry.cpp \
	impl/EthConnector.cpp \
	impl/QueryServer.cpp \
	impl/TcpScan.cpp \
	impl/UdpScan.cpp \
	impl/UsbConnector.cpp \
	impl/UsbHidConnector.cpp \
	impl/UsbHidScan.cpp \
	impl/devicecollection.cpp \
	impl/mutexhandler.cpp \
	impl/usbscan.cpp \
	impl/VicoDevice.cpp \
	impl/main.cpp \
	impl/vicodaemon.cpp

target.path = $$[QT_INSTALL_BINS]
INSTALLS += target

linux:!android {
        # install targets for systemd service files
	QMAKE_SUBSTITUTES += vicodaemon.service.in

        install_svcconf.files += $$shadowed(vicodaemon.service)
	install_svcconf.CONFIG += no_check_exist
	install_svcconf.path = $$[QT_INSTALL_LIBS]/systemd/system/
	INSTALLS += install_svcconf
}

win32 {
        # install targets for windows service files
	QMAKE_SUBSTITUTES += vicodaemon-install.bat.in

        install_svcconf.files += $$shadowed(vicodaemon-install.bat)
	install_svcconf.CONFIG += no_check_exist
	install_svcconf.path = $$[QT_INSTALL_BINS]
	INSTALLS += install_svcconf
}

macos {
        # install targets for launchd service files
	QMAKE_SUBSTITUTES += net.ketek.vicodaemon.plist.in

        install_svcconf.files += $$shadowed(net.ketek.vicodaemon.plist)
	install_svcconf.CONFIG += no_check_exist
	install_svcconf.path = /Library/LaunchDaemons
	INSTALLS += install_svcconf
}

DISTFILES += $$QMAKE_SUBSTITUTES

@
CONFIG += console
@
