var annotated_dup =
[
    [ "DbgClpExtType", "a00082.html", "a00082" ],
    [ "DbgClpType", "a00078.html", "a00078" ],
    [ "DevInfo1BootloaderType", "a00070.html", "a00070" ],
    [ "DevInfo1VICOType", "a00066.html", "a00066" ],
    [ "FirmwareVersionType", "a00034.html", "a00034" ],
    [ "LiveInfo1VIAMPType", "a00054.html", "a00054" ],
    [ "LiveInfo1VICOType", "a00042.html", "a00042" ],
    [ "LiveInfo2VIAMPType", "a00058.html", "a00058" ],
    [ "LiveInfo2VICOType", "a00046.html", "a00046" ],
    [ "LiveInfoBoundariesVIAMPType", "a00062.html", "a00062" ],
    [ "LiveInfoBoundariesVICOType", "a00050.html", "a00050" ],
    [ "MCUStatusInfoType", "a00038.html", "a00038" ],
    [ "RunStatisticsType", "a00030.html", "a00030" ],
    [ "TempType", "a00074.html", "a00074" ],
    [ "VersionType", "a00026.html", "a00026" ],
    [ "VicoQuery", "a00086.html", "a00086" ]
];