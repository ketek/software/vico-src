/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ARM compilation", "index.html", [
    [ "Introduction", "index.html", "index" ],
    [ "Compilation from source", "a00011.html", [
      [ "Version", "a00011.html#autotoc_md3", null ],
      [ "Introduction", "a00011.html#autotoc_md4", [
        [ "Environment", "a00011.html#autotoc_md5", null ],
        [ "Notes", "a00011.html#autotoc_md6", null ]
      ] ],
      [ "Prerequisite", "a00011.html#autotoc_md7", null ],
      [ "VICO-src", "a00011.html#autotoc_md8", [
        [ "VICO-src Setup", "a00011.html#autotoc_md9", null ]
      ] ],
      [ "Qt", "a00011.html#autotoc_md10", [
        [ "Qt Setup", "a00011.html#autotoc_md11", [
          [ "armhf only", "a00011.html#autotoc_md12", null ]
        ] ],
        [ "Compilation", "a00011.html#autotoc_md13", [
          [ "Qt compilation", "a00011.html#autotoc_md14", null ]
        ] ]
      ] ],
      [ "Qt Service", "a00011.html#autotoc_md15", null ],
      [ "VICODaemon", "a00011.html#autotoc_md16", [
        [ "HIDAPI compilation", "a00011.html#autotoc_md17", null ],
        [ "Compilation VICODaemon", "a00011.html#autotoc_md18", null ],
        [ "Deployment VICODaemon", "a00011.html#autotoc_md19", null ],
        [ "Running VICODaemon", "a00011.html#autotoc_md20", null ]
      ] ],
      [ "VICOLib", "a00011.html#autotoc_md21", [
        [ "Compilation VICOLib", "a00011.html#autotoc_md22", null ],
        [ "Deployment VICOLib", "a00011.html#autotoc_md23", null ],
        [ "Running VICOLib example", "a00011.html#autotoc_md24", null ]
      ] ],
      [ "Archiving (optional)", "a00011.html#autotoc_md25", null ]
    ] ],
    [ "Compilation using Qt binaries", "a00012.html", [
      [ "Version", "a00012.html#autotoc_md27", null ],
      [ "Introduction", "a00012.html#autotoc_md28", null ],
      [ "Alternative Installation: Compiling from Source", "a00012.html#autotoc_md29", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"a00011.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';