#ifndef QUERYCONSTANTS_H
#define QUERYCONSTANTS_H
#include <QString>

const int DEFAULT_TIMEOUT = 2;
const int EVENT_SCOPE_TIMEOUT = 18;
const int DELETE_FIRMWARE_TIMEOUT = 120;

// Targeted request of internal function
const QString API_DATAGRAM_REQUEST = QLatin1String("request");
// Internal getDevices function declaration
const QString API_INTERNAL_GETDEVICEINFOBYINDEX = QLatin1String("getDeviceInfoByIndex");
const QString API_INTERNAL_DEVICEINDEX = QLatin1String("deviceIndex");
const QString API_INTERNAL_GETNUMBERDEVICES = QLatin1String("getNumberDevices");

// TCP SCAN
const QString API_INTERNAL_STOPTCP = QLatin1String("stopTcp");
const QString API_INTERNAL_SCANTCP = QLatin1String("scanTcp");
// Targeted request of internal function
const QString API_DATAGRAM_FROM_IP = QLatin1String("fromIp");
// Targeted request of internal function
const QString API_DATAGRAM_TO_IP = QLatin1String("toIp");
// UDP SCAN
const QString API_INTERNAL_SCANUDP = QLatin1String("scanUdp");
// Targeted request of internal function
const QString API_DATAGRAM_UDPSCAN_IP = QLatin1String("udpScanIp");
// Targeted request of internal function
const QString API_DATAGRAM_UDPSCAN_SUBNETMASK = QLatin1String("udpScanSubnetMask");
const QString API_INTERNAL_STOPUDP = QLatin1String("stopUdp");
// common eth scan
const QString API_DATAGRAM_ETH_SCAN_USE_BINDING = QLatin1String("useBinding");
// USB SCAN
const QString API_INTERNAL_SCANUSB = QLatin1String("scanUsb");
const QString API_INTERNAL_SCANUSBHID = QLatin1String("scanUsbHid");
const QString API_INTERNAL_DAEMONVERSION = QLatin1String("daemonversion");
const QString API_INTERNAL_REMOVE_DEVICE_CONNECTION = QLatin1String("removeDeviceConnection");
// Targeted device of DPP Datagram
const QString API_DATAGRAM_DEVICE = QLatin1String("device");
// Target preferred interface
const QString API_DATAGRAM_INTERFACE = QLatin1String("interface");
// Target log level
const QString API_DATAGRAM_LOG_LEVEL = QLatin1String("logLevel");
// Target host ip address
const QString API_INTERNAL_SETHOSTIPADDRESS = QLatin1String("setHostIpAddress");
const QString API_INTERNAL_GETHOSTIPADDRESS = QLatin1String("getHostIpAddress");
const QString API_INTERNAL_HOST_IP_ADDRESS_KEY = QLatin1String("hostIPAddress");
const QString API_INTERNAL_DEVICE_IP_ADDRESS_KEY = QLatin1String("deviceIpAddress");
const QString API_INTERNAL_DAEMON_CONNECTION = QLatin1String("daemonConnection");
// Parameter ID of DPP Datagram: 1 Byte
const QString API_DATAGRAM_ID = QLatin1String("id");
// Command of DPP Datagram: 0x00 for read, 0x01 for write
const QString API_DATAGRAM_CMD = QLatin1String("cmd");
constexpr int API_DATAGRAM_CMD_READ = 0x00;
constexpr int API_DATAGRAM_CMD_WRITE = 0x01;
// Data of DPP Datagram
const QString API_DATAGRAM_DATA = QLatin1String("data");
// Data of firmware write
const QString API_DATAGRAM_FIRMWARE = QLatin1String("fw");
const QString API_STATUSTYPE = QLatin1String("statusType");
// Size of DPP Datagram, by default 4, else non-standard size
const QString API_DATAGRAM_SIZE = QLatin1String("size");

// MCU
const QString API_MCU_DATAGRAM_ENABLE = QLatin1String("MCU_DATAGRAM");
const QString API_MCU_PROTOCOL_VERSION = QLatin1String("MCU_PROTOCOL_VERSION");
const QString API_MCU_CHECKSUM = QLatin1String("MCU_CHECKSUM");
const QString API_MCU_START_ADDRESS = QLatin1String("MCU_START_ADDRESS");
const QString API_MCU_LENGTH = QLatin1String("MCU_LENGTH");
const quint8 MCU_PASS_THROUGH_ID = 71u;
// MCA
const QString API_MCA = QLatin1String("mca");
const QString API_INTERNAL_GET_PREFERRED_INTERFACE = QLatin1String("getPreferredInterface");
const QString API_INTERNAL_SET_PREFERRED_INTERFACE = QLatin1String("setPreferredInterface");
const QString API_INTERNAL_GET_LOG_LEVEL = QLatin1String("getLogLevel");
const QString API_INTERNAL_SET_LOG_LEVEL = QLatin1String("setLogLevel");
const QString API_INTERNAL_REFRESHDEVICECONNECTIONS = QLatin1String("refreshDeviceConnections");

#endif // QUERYCONSTANTS_H
