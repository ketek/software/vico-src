#ifndef UDPSCAN_H
#define UDPSCAN_H

#include <QObject>
#include <QDebug>
#include <QUdpSocket>
#include <QNetworkDatagram>
#include <QString>
#include <QUdpSocket>
#include <QTimer>
#include "DeviceRegistry.h"

/**
 * @brief The UdpScan class performs a scanning over a given IP range to find available UDP-configured devices.
 */
class UdpScan : public QObject
{
	Q_OBJECT

public:
	explicit UdpScan(QObject *parent = nullptr);
	bool stop_f = false;
	void stop();

public slots:
	void start(QString netadd, quint16 netmask, const bool &useBinding);
	void readyToRead();

private slots:
	void onHandleFoundIPs();

private:
	QUdpSocket *socket;
	QTimer *timer;
	quint16 dest_port = 3141;
	QList<QHostAddress> foundIPs;

signals:
	void scanFinished();
	void handleFoundIPs();
};

#endif // UDPSCAN_H
