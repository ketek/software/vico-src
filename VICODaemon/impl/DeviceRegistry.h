#ifndef DEVICEREGISTRY_H
#define DEVICEREGISTRY_H

#include <QtCore>
#include <QTcpSocket>
#include <QNetworkDatagram>
#include "ftd2xx.h"
#include "LibFT4222.h"
#include "devicecollection.h"
#include "VicoDevice.h"
#include "EthConnector.h"
#include "UsbHidConnector.h"
#include "UsbConnector.h"

/**
 * @brief The DeviceRegistry class maintains the active device connections. It provides all utility functions to add/refresh/remove/get device connections.
 */
class DeviceRegistry : public QObject
{
	Q_OBJECT
public:
	DeviceRegistry(const DeviceRegistry&) = delete;
	DeviceRegistry& operator=(const DeviceRegistry &) = delete;
	DeviceRegistry(DeviceRegistry &&) = delete;
	DeviceRegistry & operator=(DeviceRegistry &&) = delete;
	DeviceRegistry() = delete;

	static DeviceRegistry &getInstance();

	const QVector<VicoDevice*> getDevices();
	VicoDevice* getDeviceById(quint64 id);
	const QVector<VicoDevice*> getDevicesByConnection(InterfaceType type);
	void removeDevicesOfConnection(InterfaceType type);
	void addDevice(VicoDevice* newDevice, IConnector *connector);
	void removeDevice(quint64 deviceId);
	void removeDevice(VicoDevice* device);
	void removeDeviceWithInterface(VicoDevice* newDevice, InterfaceType type);
	VicoDevice *createVICODevice();
	void start();
	const QVector<quint64> getAllSerialNumbers();

	InterfaceType getPreferredInterface(quint64 deviceId);
	void setPreferredInterface(quint64 deviceId, InterfaceType preferredInterface);
	void removePreferredInterface(quint64 deviceId);

	void refreshDeviceConnections();

	VicoDevice* getTCPConnectedDevice(quint32 ip);
	QHostAddress getDeviceIPFromDaemon(const QString serialNumber, VICOStatusType *statusCode);

	static quint64 convertDeviceID(const QString deviceId);
	static QString convertDeviceID(const quint64 deviceId);

	bool pingDevice(VicoDevice *device);

	void insertHostAddress(const quint32 &deviceIpAddress, const quint32 &hostIpAddress, const bool &weakCache = false);
	QHostAddress getHostAddress(const quint32 &subnet, const int &netmask) const;
	QHostAddress getHostAddress(const quint32 &deviceIpAddress) const;
	void removeHostAddress(const quint32 &deviceIpAddress);

signals:
	void deviceFound(VicoDevice* device);
	void deviceConnectionStillActive(VicoDevice* device);
	void deviceRemoved();

public slots:
	void getVicoReply(VicoDevice* device, const QByteArray& msg);

private:
	DeviceRegistry(QObject *parent = nullptr);
	~DeviceRegistry() = default;
	quint8 PARAMETER_ID_MAC_0_1 = 107;
	quint8 PARAMETER_ID_MAC_2_3 = 108;
	quint8 PARAMETER_ID_MAC_4_5 = 109;
	quint8 DEVINFO2_VICO_CIX = 0x1B;

	const quint32 PING_TIMEOUT = 2000u;
	// Same timeout chosen as within TCPScan and is an guessed value
	const quint16 TIMEOUT_WAIT_FOR_DEVICE_RESPONSE = 1000u;
	DeviceCollection deviceCollection;
	HostAddresses hostAddresses;

	QMap<quint64, InterfaceType> preferredInterfaces;

	QDataStream in;

	void readReply();

};

#endif // DEVICEREGISTRY_H
