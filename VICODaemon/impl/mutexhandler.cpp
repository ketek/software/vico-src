#include "mutexhandler.h"

MutexHandler::MutexHandler(QObject *parent) : QObject(parent) {
	connect(&DeviceRegistry::getInstance(), &DeviceRegistry::deviceFound, this, &MutexHandler::onDeviceNumberChanged, Qt::ConnectionType::DirectConnection);
	connect(&DeviceRegistry::getInstance(), &DeviceRegistry::deviceRemoved, this, &MutexHandler::onDeviceNumberChanged, Qt::ConnectionType::DirectConnection);
}

MutexHandler::~MutexHandler() {
	for(QMutex *mutex : mutexes.values()) {
		mutex->try_lock();
		mutex->unlock();
	}
	qDeleteAll(mutexes.values());
}

bool MutexHandler::lockMutex(QString id, int tryTimeout, bool global) {
	if(global) {
		return lockGlobalResource(tryTimeout);
	} else {
		if(getMutex(id)->tryLock(tryTimeout)) {
			if(lockResource()) {
				return true;
			} else {
				getMutex(id)->unlock();
				return false;
			}
		} else {
			return false;
		}
	}
}

bool MutexHandler::unlockMutex(QString id, bool global) {
	if(global) {
		unlockGlobalResource();
		return true;
	} else if(mutexes.contains(id.toUpper())) {
		getMutex(id)->try_lock();
		getMutex(id)->unlock();
		unlockResource();
		return true;
	} else {
		return false;
	}
}

QMutex *MutexHandler::getMutex(QString id) {
	if(!mutexes.contains(id.toUpper())) {
		mutexes.insert(id.toUpper(), new QMutex);
	}
	return mutexes[id.toUpper()];
}

bool MutexHandler::lockResource(int count, int tryTimeout) {
	QMutexLocker locker(&globalMutex);
	return resources.tryAcquire(count, tryTimeout);
}

void MutexHandler::unlockResource(int count) {
	QMutexLocker locker(&globalMutex);
	resources.release(count);
}

bool MutexHandler::lockGlobalResource(int tryTimeout) {
	const int count = getResourceCount();
	QMutexLocker locker(&globalMutex);
	return resources.tryAcquire(count, tryTimeout);
}

void MutexHandler::unlockGlobalResource() {
	const int count = getResourceCount();
	QMutexLocker locker(&globalMutex);
	resources.release(count);
}

void MutexHandler::onDeviceNumberChanged() {
	const int count = getResourceCount();
	QMutexLocker locker(&globalMutex);
	const int numberOfDevices = DeviceRegistry::getInstance().getDevices().size();
	resourceCount = numberOfDevices;
	if(numberOfDevices == count) {
		// do nothing since it will be unlocked later
	} else if(numberOfDevices > count) {
		// currently, count resources are locked.
		// need to lock the new one
		resources.release(numberOfDevices);
		resources.acquire(numberOfDevices);
	} else {
		// do nothing
		// calling unlockGlobalResource then will not release the currently locked resource
	}
}

int MutexHandler::getResourceCount() {
	QMutexLocker locker(&globalMutex);
	return resourceCount;
}
