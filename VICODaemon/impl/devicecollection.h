#ifndef DEVICECOLLECTION_H
#define DEVICECOLLECTION_H

#include "VicoDevice.h"

/**
 * @brief The DeviceCollection class implements all options for possible device connections, which are TCP, UDP or USB for VICO-DVs and USB via HIDAPI for VICO-AVs.
 */
class DeviceCollection
{
public:
	DeviceCollection();
	~DeviceCollection();

	void addDevice(VicoDevice *device);

	void removeDevice(VicoDevice *device);
	void removeDevices(const quint64 deviceId);
	void removeDevices(const QVector<InterfaceType> interfaceTypes = QVector<InterfaceType>());
	void removeDeviceWithInterface(VicoDevice *device, const InterfaceType interfaceTypes);

	bool contains(const quint64 deviceId) const;
	bool contains(VicoDevice* device) const;

	const QVector<quint64> getSerialNumbers() const;

	const QVector<VicoDevice*> getDevices(const QVector<InterfaceType> interfaceTypes = QVector<InterfaceType>()) const;
	VicoDevice* getDevice(const quint64 deviceId, const InterfaceType interfaceTypes) const;
	const QVector<VicoDevice*> getDevices(const quint64 deviceId) const;
	VicoDevice* getTCPConnectedDevice(const quint32 ip) const;
	QHostAddress getDeviceIPFromDaemon(const QString serialNumber, VICOStatusType *statusCode) const;

private:
	QVector<VicoDevice*> tcpDevices;
	QVector<VicoDevice*> udpDevices;
	QVector<VicoDevice*> usbDevices;
	QVector<VicoDevice*> usbHidDevices;
	QVector<VicoDevice*> unknownDevices;
	const QMap<InterfaceType, QVector<VicoDevice*>*> interfaceToDevices {
		{InterfaceType::INTERFACE_ETHERNET_TCP, &tcpDevices},
		{InterfaceType::INTERFACE_ETHERNET_UDP, &udpDevices},
		{InterfaceType::INTERFACE_USB, &usbDevices},
		{InterfaceType::INTERFACE_USB_HID, &usbHidDevices},
		{InterfaceType::INTERFACE_UNKNOWN, &unknownDevices}
	};


	void removeConnection(IConnector *connector);
};

class HostAddresses {
public:
	HostAddresses() = default;
	~HostAddresses();

	/**
	 * Inserts host address into the cache.
	 * If weakCache is set to true, then the host address can be influenced by the host addresses found by ethernet scans
	 * Otherwise, the host address can only be set using the setHostIpAddress API
	 * @brief insertHostAddress
	 * @param deviceIpAddress
	 * @param hostIpAddress
	 * @param weakCache
	 */
	void insertHostAddress(const quint32 &deviceIpAddress, const quint32 &hostIpAddress, const bool &weakCache = false);

	/**
	 * Get the cached host address that is linked to the subnet.
	 * @brief getHostAddress
	 * @param subnet
	 * @param netmask
	 * @return
	 */
	QHostAddress getHostAddress(const quint32 &subnet, const int &netmask) const;

	/**
	 * Get the cached host address that is linked to the device IP Address.
	 * @brief getHostAddress
	 * @param deviceIpAddress
	 * @return
	 */
	QHostAddress getHostAddress(const quint32 &deviceIpAddress) const;

	void removeHostAddress(const quint32 &deviceIpAddress);

private:
	struct Host {
		quint32 hostAddress = 0u;
		bool weakCache = false;
	};

	QMap<quint32, Host*> hostAddresses;

};

#endif // DEVICECOLLECTION_H
