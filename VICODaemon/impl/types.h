#ifndef TYPES_H
#define TYPES_H

/**
 * The connection type retrieved by #scanTcpDevices(), #scanUdpDevices(), and #scanUsbDevices().
 */
typedef enum
{
	/** 0: If the found device connection couldn't be assigned. */
	INTERFACE_UNKNOWN =			0U,
	/** 1: If the found device connection is established via TCP. */
	INTERFACE_ETHERNET_TCP =	1U,
	/** 2: If the found device connection is established via UDP. */
	INTERFACE_ETHERNET_UDP =	2U,
	/** 3: If the found device connection is established via USB (digital version device). */
	INTERFACE_USB =				3U,
	/** 4: If the found device connection is established via USB (analog version device). */
	INTERFACE_USB_HID =			4U
} InterfaceType;

typedef enum
{
	VICO_DV  = 0U,
	VICO_AV  = 1U
}
DeviceType;

/**
 * VICOLib-related return codes. Range 0x00 - 0xFF is reserved for VICOLib-specific return codes.
 */
typedef enum
{
	/** 0x00: VICOLib return code: Request successfully processed. */
	VICO_SUCCESS =								0x00,
	/** 0x01: VICOLib return code: Command not supported by VICODaemon. */
	VICO_COMMAND_NOT_SUPPORTED =				0x01,
	/** 0x02: VICOLib return code: Command not supported by selected device. */
	VICO_COMMAND_NOT_SUPPORTED_BY_DEVICE  =		0x02,
	/** 0x50: VICOLib return code: No device connection with targeted serial number active. */
	VICO_NO_DEVICE_WITH_ID_CONNECTED_ERROR =	0x50,
	/** 0x51: VICOLib return code: VICODaemon is currently not running. See \ref API (VICODaemon). */
	VICO_VICODAEMON_IS_STOPPED =				0x51,
	/** 0x52: VICOLib return code: Timeout error due to no response. */
	VICO_VICOLIB_TIMEOUT_ERROR =				0x52,
	/** 0x53: VICOLib return code: Data responded by the device does not fit into given data type. */
	VICO_RESPONSE_OUT_OF_BOUNDS_ERROR =			0x53,
	/** 0x54: VICOLib return code: Given argument(s) not allowed or null. */
	VICO_INVALID_ARGUMENT_ERROR =				0x54,
	/** 0x55: VICOLib return code: Data responded by the device not as expected. */
	VICO_INVALID_RESPONSE_DATA_ERROR =			0x55,
	/** 0xFF: VICOLib return code: Undefined error. */
	VICO_UNDEFINED_ERROR =						0xFF
} VICOStatusType;

/**
 * DPP-related return codes. Range 0x00 - 0x4F is reserved for DPP return codes. Range 0x50 - 0x7F is reserved for VICOLib-specific return codes.
 */
typedef enum
{
	/** 0x00: DPP return code: Request successfully processed. */
	DPP_SUCCESS =								0x00,
	/** 0x01: DPP return code: Error during write request: Written value is out of range. The value was not applied in the device and remains unchanged. The closed valid value for your request will be returned by the function call. */
	DPP_OUT_OF_RANGE_ERROR =					0x01,
	/** 0x02: DPP return code: Error during write request: Requested parameter is read-only and the write request was rejected. The value in the device remains unchanged. */
	DPP_PARAMETER_READ_ONLY_ERROR =				0x02,
	/** 0x03: DPP return code: Error during write request: Requested parameter does not exist in the device. No changes to the device configuration were made. */
	DPP_PARAMETER_NOT_EXISTING_ERROR =			0x03,
	/** 0x04: DPP return code: Invalid command: Request could not be interpreted since the low-level command byte was invalid valid. No changes to the device configuration were made. */
	DPP_INVALID_COMMAND_ERROR =					0x04,
	/** 0x05: DPP return code: Error during request: Parameter currently not accessible (e.g. due to running measurement, power down, temperature sensor not ready). No changes to the device configuration were made. */
	DPP_REQUEST_CURRENTLY_NOT_POSSIBLE_ERROR =	0x05,
	/** 0x06: DPP return code: Error during request: Internal timeout appeared. The value in the device might be changed. */
	DPP_TIMEOUT_ERROR =							0x06,
	/** 0x07: DPP return code: Syntax error, unpermitted combination of requests. No changes to the device configuration were made. */
	DPP_SYNTAX_ERROR_INVALID_REQUEST_ERROR =	0x07,

	/** 0x50: VICOLib return code: No device connection with targeted serial number active. Request could not be sent to the device. */
	DPP_NO_DEVICE_WITH_ID_CONNECTED_ERROR =		0x50,
	/** 0x51: VICOLib return code: VICODaemon is currently not running. See \ref API (VICODaemon). Request could not be sent to the device. */
	DPP_VICODAEMON_IS_STOPPED =					0x51,
	/** 0x52: VICOLib return code: Timeout error due to no response from the device. Make sure the device is powered up and connected to your system. Try to find the device using scanning functions (#scanUsbDevices(), #scanTcpDevices(), #scanUdpDevices()). */
	DPP_VICOLIB_TIMEOUT_ERROR =					0x52,
	/** 0x53: VICOLib return code: Data responded by the device does not fit into given data type. */
	DPP_RESPONSE_OUT_OF_BOUNDS_ERROR =			0x53,
	/** 0x54: VICOLib return code: Given argument(s) not allowed or null. No request was sent to the device. */
	DPP_INVALID_ARGUMENT_ERROR =				0x54,
	/** 0x55: VICOLib return code: Data responded by the device not as expected. */
	DPP_INVALID_RESPONSE_DATA_ERROR =			0x55,

	/** 0x56: VICOLib return code: Given argument value is not in valid range to be sent to the DPP. This might be due to a data type error (e.g., value is too high) or due to an quantization error (e.g., value is not an integer multiple of allowed quantization). Example: This error is returned when calling #setSlowFilterGapTime() with a gapTime value that is not an integer multiple of 12.5ns. <br>
	The value was not sent to the device and remains unchanged. The closed valid value for your request will be returned by the function call. */
	DPP_VICOLIB_OUT_OF_RANGE_ERROR =			0x56,
	/** 0x57: VICOLib return code: Command is not applicable for the targeted device (e.g. DPP datagram sent to VICO-AV). */
	DPP_COMMAND_NOT_SUPPORTED_ERROR =			0x57,

	/** 0xFF: VICOLib return code: Undefined error. */
	DPP_UNDEFINED_ERROR =						0xFF
} DPPStatusType;

/**
 * MCU-related return codes. Range 0x00 - 0x4F is reserved for MCU and MCU bootloader return codes. Range 0x50-0x7F is reserved for MCU-related VICOLib return codes. Range 0x80 - 0x9F is reserved for UART-passthrough-related DPP return codes.
 */
typedef enum
{
	/** 0x00: MCU return code: Request successfully processed. */
	MCU_SUCCESS =								0x00,
	/** 0x01: MCU return code: Command not supported. */
	MCU_COMMAND_NOT_SUPPORTED =					0x01,
	/** 0x02: MCU return code: Wrong datagram CRC checksum. */
	MCU_WRONG_CRC_CHECKSUM =					0x02,
	/** 0x03: MCU return code: Command length mismatch. */
	MCU_COMMAND_LENGTH_MISMATCH =				0x03,
	/** 0x04: MCU return code: Datagram version not supported. */
	MCU_VERSION_NOT_SUPPORTED =					0x04,

	/** 0x10: MCU return code: Error during command processing. */
	MCU_ERROR =									0x10,

	/** 0x20: MCU return code: For #setEEP(), and #getEEP() Length requirement (len) outside the valid range, for #twi() Buffer sizes (ses/res) outside the valid range, for #getIO(), #setIO(), #getADC(), and #setDAC() Port requirement (por) outside the valid range, for #setIMax() Maximum current (itecMax) outside the valid range, #setUMax() Maximum voltage (utecMax) outside the valid range, for #dbgClp() and #dbgClpExt() control loop not in operation (check system states). */
	MCU_LEN_OUT_OF_VALID_RANGE =				0x20,
	/** 0x21: MCU return code: For #setEEP(), and #getEEP() Start address (addStart) outside the valid range, for #getIO(), #setIO(), #getADC(), and #setDAC() Pin requirement (pin) outside the valid range. */
	MCU_ADDRESS_OUT_OF_VALID_RANGE =			0x21,
	/** 0x22: MCU return code: For #setEEP() System is not in EEA mode, for #setIO() Value requirement (val) outside the valid range. */
	MCU_SYSTEM_IS_NOT_IN_EEA_MODE =				0x22,
	/** 0x23: MCU return code: For #twi() most recent acknowledge bit from slave is NACK. */
	MCU_TWI_SLAVE_NACK =						0x23,
	/** 0x24: MCU return code: For #getADC() mutex object is not available. */
	MCU_GETADC_MUTEX_NOT_AVAILABLE =			0x24,

	/** 0x30: MCU bootloader return code: For #flashRead(), #flashWriteSessionStart(), and #swPkgStartApplication() Already in "flash" session, or request not possible because flash write session active. */
	MCU_BL_FLASH_SESSION_ACTIVE =				0x30,
	/** 0x31: MCU bootloader return code: For #flashWriteSessionExit(), #flashWriteSessionReset(), and #flashWriteSessionData() No flash session active. */
	MCU_BL_NO_FLASH_SESSION_ACTIVE =			0x31,
	/** 0x32: MCU bootloader return code: For #flashRead(), and #flashWriteSessionData() Start address (addStart) out of range. */
	MCU_BL_ADDRESS_OUT_OF_VALID_RANGE =			0x32,
	/** 0x33: MCU bootloader return code: For #flashRead(), and #flashWriteSessionData() Start address (addStart) out of range, for #flashRead() Length requirement (len) outside the valid range, for #flashWriteSessionData()  data length not equal to 128 bytes. */
	MCU_BL_LEN_OUT_OF_VALID_RANGE =				0x33,
	/** 0x34: MCU bootloader return code: For #flashWriteSessionData() The data block is not in sequence within the session. */
	MCU_BL_DATA_NOT_IN_SEQUENCE_WITHIN_SESSION =0x34,
	/** 0x35: MCU bootloader return code: For #swPkgStartApplication() Application checksum invalid. */
	MCU_BL_APPLICATION_CHECKSUM_INVALID =		0x35,
	/** 0x36: MCU bootloader return code: Already in application. */
	MCU_BL_ALREADY_IN_APPLICATION =				0x36,

	/** 0x50: VICOLib return code: No device connection with targeted serial number active. Request could not be sent to the device. */
	MCU_NO_DEVICE_WITH_ID_CONNECTED_ERROR =		0x50,
	/** 0x51: VICOLib return code: VICODaemon is currently not running. See \ref API (VICODaemon). Request could not be sent to the device. */
	MCU_VICODAEMON_IS_STOPPED =					0x51,
	/** 0x52: VICOLib return code: Timeout error due to no response from the device. Make sure the device is powered up and connected to your system. Try to find the device using scanning function (#scanUsbDevices()). */
	MCU_VICOLIB_TIMEOUT_ERROR =					0x52,
	/** 0x53: VICOLib return code: Data responded by the device does not fit into given data type. */
	MCU_RESPONSE_OUT_OF_BOUNDS_ERROR =			0x53,
	/** 0x54: VICOLib return code: Given argument(s) not allowed or null. No request was sent to the device. */
	MCU_INVALID_ARGUMENT_ERROR =				0x54,
	/** 0x55: VICOLib return code: Data responded by the device not as expected. */
	MCU_INVALID_RESPONSE_DATA_ERROR =			0x55,

	/** 0x80: DPP passthrough return code: UART timeout error due to no response from the MCU. */
	DPP_UART_TIMEOUT_ERROR =					0x80,
	/** 0x81: DPP passthrough return code: No access to internal memory due to an DPP data handling error. */
	DPP_NO_ACCESS_TO_INTERNAL_MEMORY_ERROR =	0x81,
	/** 0x82: DPP passthrough return code: Incomplete UART request with data and command length mismatch. */
	DPP_INCOMPLETE_UART_COMMAND_ERROR =			0x82,

	/** 0xFF: VICOLib return code: Undefined error. */
	MCU_UNDEFINED_ERROR =						0xFF,
} MCUStatusType;

#endif // TYPES_H
