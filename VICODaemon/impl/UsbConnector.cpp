#include "UsbConnector.h"
#include "vicodaemon.h"

UsbConnector::UsbConnector(FT_HANDLE fthandle, FT_HANDLE fthandleGPIO, QString dataSerNo, QString gpioSerNo, QObject *parent) : QObject(parent)
{
	threadPool = new QThreadPool(this);
	threadPool->setMaxThreadCount(1);
	this->connectionType = INTERFACE_USB;
	this->fthandle = fthandle;
	this->fthandleGPIO = fthandleGPIO;
	this->dataSerNo = dataSerNo;
	this->gpioSerNo = gpioSerNo;
	initializeConnection();
}

UsbConnector::~UsbConnector() {
}

void UsbConnector::initializeConnection() {
	FT4222_STATUS ft4222Status;
	GPIO_Dir gpioDir[4]= {GPIO_INPUT,GPIO_INPUT,GPIO_INPUT,GPIO_INPUT};
	FT_STATUS ftStatus;

	// Open datenstream device
	QByteArray dataBa = dataSerNo.toLocal8Bit();
	const char *dataCh = dataBa.data();
	qDebug() << "[USB]\tOpening datastream device with serialnumber" << (char*)dataCh;
	ftStatus = FT_OpenEx((char*)dataCh,FT_OPEN_BY_SERIAL_NUMBER, &fthandle);
	if (FT_OK != ftStatus)
	{
		qDebug("[USB]\tOpening datastream device failed.");
	}

	// Open GPIO Device
	QByteArray gpioBa = gpioSerNo.toLocal8Bit();
	const char *gpioCh = gpioBa.data();
	qDebug() << "[USB]\tOpening GPIO device with serialnumber" << (char*)gpioCh;
	ftStatus = FT_OpenEx((char*)gpioCh,FT_OPEN_BY_SERIAL_NUMBER, &fthandleGPIO);
	if (FT_OK != ftStatus)
	{
		qDebug("[USB]\tOpening GPIO device failed.");
	}

	ft4222Status = DPP3_init (fthandle);
	if (ft4222Status == FT4222_OK)
	{
		qDebug("[USB]\tDPP3 init ok.");
	}
	else
	{
		qDebug("[USB]\tDPP3 init failed.");
	}

	ft4222Status = FT4222_GPIO_Init(fthandleGPIO, gpioDir);
	if (ft4222Status == FT4222_OK)
	{
		qDebug("[USB]\tGPIO init ok.");
	}
	else
	{
		qDebug("[USB]\tGPIO init failed.");
	}

	// disable suspend feature
	ft4222Status = FT4222_SetSuspendOut(fthandleGPIO, false);
	if (ft4222Status == FT4222_OK)
	{
		qDebug("[USB]\tFT4222_SetSuspendOut ok.");
	}
	else
	{
		qDebug("[USB]\tFT4222_SetSuspendOut failed.");
	}

	//disable wake up feature
	FT4222_SetWakeUpInterrupt(fthandleGPIO, false);
	if (ft4222Status == FT4222_OK)
	{
		qDebug("[USB]\tFT4222_SetWakeUpInterrupt ok.");
	}
	else
	{
		qDebug("[USB]\tFT4222_SetWakeUpInterrupt failed.");
	}
}

void UsbConnector::closeConnection() {
	qDebug() << "[USB]\tClosing connections via FT4222_UnInitialize and FT_Close";
	FT4222_UnInitialize(fthandle);
	FT_Close(fthandle);

	FT4222_UnInitialize(fthandleGPIO);
	FT_Close(fthandleGPIO);
}

InterfaceType UsbConnector::getConnectionType() const {
	return this->connectionType;
}

FT4222_STATUS DPP3_send (FT_HANDLE ftHandle, uint8_t* transmitData, uint16_t size)
{
	FT4222_STATUS ft4222Status;
	uint16_t sizeTransferred = 0;

	// Advanced USB OUT logging
	QString msgOut (QStringLiteral("[DEV]\t[REQ]\t["));
	for (int i = 0; i < size; ++i) {
		msgOut.append(QString::number((quint8)transmitData[i]));
		if (i != size-1) {
			msgOut.append(QStringLiteral(","));
		}
	}
	msgOut.append(QStringLiteral("]"));
	qDebug().noquote() << msgOut;

	ft4222Status = FT4222_SPIMaster_SingleWrite(ftHandle, &transmitData[0], size, &sizeTransferred, true);
	return ft4222Status;
}

FT4222_STATUS DPP3_read_answer(FT_HANDLE ftHandle, uint8_t* receiveData, uint16_t receiveSize, uint16_t sizeReceived)
{
	FT4222_STATUS ft4222Status;
	ft4222Status = FT4222_SPIMaster_SingleRead(ftHandle, &receiveData[0], receiveSize, &sizeReceived, true);
	return ft4222Status;
}


void UsbConnector::sendMsg(const QByteArray& msg, const quint16 receiveSize, const int &timeout) {
	clock_t timeout_ticks = CLOCKS_PER_SEC*timeout; //timing declaration

	QRunnable *runnable = VICORunnable::create([this, timeout_ticks, msg, receiveSize] {
		clock_t start, proc_ticks;
		quint16 sizeReceived = 0;
		quint16 sendSize = msg.size();
		quint8 sendData[sendSize];
		for (int i = 0; i < sendSize; ++i)
		{
			sendData[i] = msg[i];
		}
		quint8 receiveData[receiveSize];
		memset(receiveData, 0, sizeof(receiveData));
		FT4222_STATUS ft4222Status;

		BOOL value = false;
		ft4222Status = FT4222_GPIO_Read(fthandleGPIO, (GPIO_Port)GPIO_PORT0, &value);

		// Send a request with given parameter_id, command_id, and two data bytes to the device
		ft4222Status = DPP3_send ( fthandle,  &sendData[0], sendSize);

		start = clock();
		proc_ticks = clock()- start;
		bool isFirstTime = true;
		while((value == 0) and (proc_ticks <= timeout_ticks)) {
			// Poll per 50ms if first time didn't get the value
			if (! isFirstTime) {
				usleep(50000);
			}
			ft4222Status = FT4222_GPIO_Read(fthandleGPIO, (GPIO_Port)GPIO_PORT0, &value);
			proc_ticks = clock()-start;
			isFirstTime = false;
		}


		if(ft4222Status != FT4222_OK)
		{
			qDebug() << "[USB]\tFT4222_GPIO_Read failed with status" << ft4222Status;
		}
		// Response data is ready
		if (value == 1)
		{
			ft4222Status = DPP3_read_answer (fthandle, &receiveData[0], receiveSize, sizeReceived);
		}
		else
		{
			qDebug() << "[USB]\tTimeout at processing time in ticks:" << proc_ticks;
			return;
		}

		if (FT4222_OK != ft4222Status)
		{
			// spi master read failed
			qDebug() << "[USB]\tMaster read failed " << ft4222Status;
			return;
		}

		// Emit signal msgReceived to signal QueryServer::getVicoReply that an answer is ready to process
		QByteArray answerArray;
		for (int i = 0; i < receiveSize; ++i)
		{
			answerArray.append((const char*)(receiveData + i), sizeof(quint8));
		}
		emit msgReceived(answerArray);
	});
	threadPool->start(runnable);
	threadPool->waitForDone(int(timeout_ticks));
}

FT4222_STATUS UsbConnector::DPP3_init(FT_HANDLE ftHandle) const {
	FT_STATUS ftStatus;
	FT4222_STATUS ft4222Status;
	FT4222_ClockRate clk;

	// Init SPI master with divider 2
	ftStatus =  FT4222_SPIMaster_Init(ftHandle, SPI_IO_SINGLE, CLK_DIV_2, CLK_IDLE_HIGH, CLK_TRAILING, 0x01);
	qDebug() << "[USB]\tFT4222_SPIMaster_Init done" << ftStatus;
	if (FT_OK != ftStatus)
	{
		qDebug() << "[USB]\tInit FT4222 as SPI master device failed!";
		return (FT4222_STATUS)ftStatus;
	}
	ft4222Status = FT4222_SetClock(ftHandle, SYS_CLK_24);
	qDebug() << "[USB]\tFT4222_SetClock done" << ft4222Status;

	if (FT4222_OK != ft4222Status)
	{
		qDebug() << "[USB]\tSet Clock failed";

	}

	ft4222Status = FT4222_GetClock(ftHandle, &clk);
	qDebug() << "[USB]\tFT4222_GetClock done" << ft4222Status;
	if (FT4222_OK != ft4222Status)
	{
		qDebug() << "[USB]\tGet Clock failed";
		return ft4222Status;
	}

	switch (clk)
	{
	case SYS_CLK_60: qDebug() << "[USB]\tSystemclock 60MHz"; break;
	case SYS_CLK_24: qDebug() << "[USB]\tSystemclock 24MHz"; break;
	case SYS_CLK_48: qDebug() << "[USB]\tSystemclock 48MHz"; break;
	case SYS_CLK_80: qDebug() << "[USB]\tSystemclock 80MHz"; break;
	default: qDebug() << "[USB]\tSystemclock Fehler"; break;

	}

	return ft4222Status;
}
