#include "UdpScan.h"

UdpScan::UdpScan(QObject *parent) : QObject(parent) {
	connect(this, &UdpScan::handleFoundIPs, this, &UdpScan::onHandleFoundIPs);
}

void UdpScan::start(QString netadd, quint16 netmask, const bool &useBinding)
{
	QHostAddress hostIpAddress(QHostAddress::Any);
	if(useBinding) {
		hostIpAddress = DeviceRegistry::getInstance().getHostAddress(QHostAddress(netadd).toIPv4Address(), netmask);
	}

	foundIPs.clear();
	socket = new QUdpSocket;
	connect(socket, &QUdpSocket::readyRead, this, &UdpScan::readyToRead);
	if(useBinding && hostIpAddress.isNull()) {
		qWarning() << "[UDP]\tHost IP Address could not be found even though binding was specified to be used";
		hostIpAddress = QHostAddress(QHostAddress::Any);
	}
	if(socket->bind(hostIpAddress, dest_port, QAbstractSocket::ShareAddress)) {
		qInfo().noquote() << "[TCP]\tHost of IP Address" << hostIpAddress.toString() << "was bound to the UDP Socket";
	} else {
		qWarning() << "[UDP]\tError at binding " << socket->errorString();
		return;
	}

	stop_f=false;
	// Each time a scanning is started, remove all previously scanned devices
	DeviceRegistry::getInstance().removeDevicesOfConnection(INTERFACE_ETHERNET_UDP);
	QString myHexString = QLatin1String("FFFFFFFF");
	QByteArray txdata = QByteArray::fromHex(myHexString.toUtf8());
	// https://de.wikipedia.org/wiki/Netzmaske
	// https://de.wikipedia.org/wiki/Broadcast
	// 4294967040 = 255.255.255.0
	quint32 net_mask = (0xFFFFFFFF << (32 - netmask)) & 0xFFFFFFFF;
	// 3232235775 = 192.168.0.255
	quint32 last_ip = QHostAddress(netadd).toIPv4Address() | ~(net_mask);
	QTimer timer;
	timer.setSingleShot(true);
	QEventLoop loop;

	connect(&timer, &QTimer::timeout, &loop, &QEventLoop::quit);

	connect(socket, &QUdpSocket::stateChanged, this, [&](QAbstractSocket::SocketState socketState) {
		qDebug() << "[UDP]\tSocket state changed to" << socketState;
		if(QUdpSocket* udpSocket = dynamic_cast<QUdpSocket*>(sender())) {
			// Can be used for debugging purposes
		}
	});

	// Start the broadcast
	socket->writeDatagram(txdata, QHostAddress(last_ip), dest_port);
	qDebug() << "[UDP]\tSent UDP broadcast to last_ip " << QHostAddress(last_ip).toString() << " at port: " << dest_port;

	timer.start(2000);
	loop.exec();
	timer.stop();
	socket->disconnectFromHost();
	socket->close();
	socket->deleteLater();
	emit handleFoundIPs();
}

void UdpScan::readyToRead()
{
	QByteArray datagram;
	while (socket->hasPendingDatagrams()) {
		datagram.resize(int(socket->pendingDatagramSize()));
		QHostAddress senderAddress;
		quint16 port = 0u;
		socket->readDatagram(datagram.data(), datagram.size(), &senderAddress, &port);
		foundIPs.append(senderAddress);
	}
}

void UdpScan::onHandleFoundIPs() {
	// All IP addresses except localhost are interested to connect to
	QString localHost = QStringLiteral("192.168.0.1");
	QHostAddress localHostAddr = QHostAddress(localHost);
	if (foundIPs.contains(localHostAddr)) {
		foundIPs.removeAll(localHostAddr);
	}

	// Now build up the connections
	for (QHostAddress ip : foundIPs) {
		if (stop_f==true){
			stop_f=false;
			return;
		}
		QUdpSocket* deviceSocket = new QUdpSocket;
		qDebug() << "[UDP]\tConnecting to the host at ip" << ip.toString() << "at port" << dest_port;
		deviceSocket->connectToHost(ip, dest_port);
		if (deviceSocket->waitForConnected(1000)){
			// We handle adding the USB device asynchronously
			QTimer timer;
			timer.setSingleShot(true);
			QEventLoop loop;
			bool connec = false;
			connect(&DeviceRegistry::getInstance(), &DeviceRegistry::deviceFound, &loop, [&] {
				connec = true;
				loop.quit();
			});
			connect( &timer, &QTimer::timeout, &loop, &QEventLoop::quit );
			VicoDevice* newDevice = DeviceRegistry::getInstance().createVICODevice();
			IConnector *ethConnector = new EthConnector(deviceSocket, newDevice);
			DeviceRegistry::getInstance().addDevice(newDevice, ethConnector);
			timer.start(1000);
			loop.exec();
			if (!connec) {
				qDebug() << "[UDP]\tDevice couldn't be connected.";
				timer.stop();
				EthConnector::closeConnection(deviceSocket);
				deviceSocket->deleteLater();
			} else {
				qDebug().noquote() << "[UDP]\tUDP connected to" << ip.toString() << "through host address" << deviceSocket->localAddress().toString();
				DeviceRegistry::getInstance().insertHostAddress(ip.toIPv4Address(), deviceSocket->localAddress().toIPv4Address(), true);
			}
		}
		else {
			qDebug() << "[UDP]\tNo device couldn't be connected.";
			EthConnector::closeConnection(deviceSocket);
			deviceSocket->deleteLater();
		}
	}
}


void UdpScan::stop()
{
	emit scanFinished();
}
