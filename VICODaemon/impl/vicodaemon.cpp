#include "vicodaemon.h"
#include <iostream>

const int VicoDaemon::SHARED_MEMORY_BYTE_NUMBER = 1;
const QString VicoDaemon::SHARED_MEMORY_KEY = QStringLiteral(
			#ifdef Q_OS_WIN
			"Global\\VICODaemonSharedMemoryKey"
			#else
			"/usr/share/VICODaemonSharedMemoryKey"
			#endif
			);
#ifdef Q_OS_WIN
static const QString DEBUG_LIBRARY_NAME = QStringLiteral("Dbghelp");
#endif

static const qint64 MAX_LOG_SIZE = 10000000;
static const wchar_t* SVCNAME;
#ifdef Q_OS_WIN
static const char* LOG_PATH_ENV_VAR = "PROGRAMDATA";
#endif
static const QString LOG_DATETIME_FORMAT = QStringLiteral("yyyy-MM-dd h:mm:ss.zzz");
static const QString LOG_DIR_PATH_SUFFIX = QStringLiteral("KETEK/VICODaemon");
static const QString LOG_NAME = QStringLiteral("VICODaemon");
static const QString LOG_EXTENSION = QStringLiteral(".log");
static const QSet<QtMsgType> DEFAULT_MSG_TYPES {QtMsgType::QtCriticalMsg, QtMsgType::QtFatalMsg};
static const QMap<quint8, QSet<QtMsgType>> ALLOWED_MSGS {
	{0U, {}},
	{1U, DEFAULT_MSG_TYPES},
	{2U, {QtMsgType::QtCriticalMsg, QtMsgType::QtFatalMsg, QtMsgType::QtWarningMsg}},
	{3U, {QtMsgType::QtCriticalMsg, QtMsgType::QtFatalMsg, QtMsgType::QtWarningMsg, QtMsgType::QtInfoMsg}},
	{4U, {QtMsgType::QtCriticalMsg, QtMsgType::QtFatalMsg, QtMsgType::QtWarningMsg, QtMsgType::QtInfoMsg, QtMsgType::QtDebugMsg}}
};

static quint8 logLevel = 1U;
static QMutex logMutex;
static QDir logDir
#ifndef Q_OS_WIN
("/var/log/" + LOG_DIR_PATH_SUFFIX)
#endif
;

VicoDaemon::VicoDaemon(int &argc, char **argv) :
	Service{argc, argv}
{
	QCoreApplication::setApplicationName(QStringLiteral(TARGET));
	QCoreApplication::setApplicationVersion(QStringLiteral(VERSION));
	SVCNAME = const_cast<wchar_t*>(reinterpret_cast<const wchar_t*>(QCoreApplication::applicationName().utf16()));
#ifdef Q_OS_WIN
	QString programDataPath = QString::fromUtf8(qgetenv(LOG_PATH_ENV_VAR));
	logDir = QDir(programDataPath + QStringLiteral("/") + LOG_DIR_PATH_SUFFIX);
#endif
}

bool VicoDaemon::setLogLevel(const quint8 &type) {
	if(ALLOWED_MSGS.contains(type)) {
		logLevel = type;
		return true;
	} else {
		return false;
	}
}

quint8 VicoDaemon::getLogLevel() {
	return logLevel;
}

QtService::Service::CommandResult VicoDaemon::onStart() {
	if(isDaemonRunning()) {
		return CommandResult::Failed;
	}

	qSetMessagePattern(QStringLiteral("%{message}"));
	qInstallMessageHandler(VicoDaemon::daemonMessageHandler);

#ifdef Q_OS_WIN
	signal(SIGABRT, VicoDaemon::signalHandler);
	signal(SIGFPE, VicoDaemon::signalHandler);
	signal(SIGILL, VicoDaemon::signalHandler);
	signal(SIGINT, VicoDaemon::signalHandler);
	signal(SIGSEGV, VicoDaemon::signalHandler);
	signal(SIGTERM, VicoDaemon::signalHandler);
#endif
	server.setup();
	// Setup the device registry.
	server.setUdpScan(&udpscan);
	server.setTcpScan(&tcpscan);
	server.setUsbScan(&usbscan);
	server.setUsbHidScan(&usbhidscan);

	// Now that we have the Device with requested device ID, we can connect it:
	// as soon as it has a msgReceived signal, get the reply by getVicoReply
	server.setSocketOptions(QLocalServer::WorldAccessOption);
	connect(&server, &QueryServer::logMessage, this, &VicoDaemon::logMessage);
	server.listen(QStringLiteral("VICODaemon"));
	qDebug() << "[INTERNAL]\tVICODaemon initialized successfully.";
	return CommandResult::Completed;
}

QtService::Service::CommandResult VicoDaemon::onStop(int &exitCode)
{
	if(daemonSharedMemory) {
		daemonSharedMemory->detach();
		delete daemonSharedMemory;
	}
	qDebug() << "[INTERNAL]\tVICODaemon stopped.";
	server.stopServer();
	exitCode = EXIT_SUCCESS;
	return CommandResult::Completed;
}

bool VicoDaemon::isDaemonRunning() {
	// check if the shared memory is already initialized
	daemonSharedMemory = new QSharedMemory(SHARED_MEMORY_KEY, this);
	daemonSharedMemory->setNativeKey(SHARED_MEMORY_KEY);
	if(!daemonSharedMemory->create(SHARED_MEMORY_BYTE_NUMBER)) {
		qFatal("[INTERNAL]\tAnother VICODaemon instance is already running. Please try again after stopping the current VICODaemon.");
		return true;
	}
	return false;
}

void VicoDaemon::daemonMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &message) {
	if(!ALLOWED_MSGS.value(logLevel, DEFAULT_MSG_TYPES).contains(type)) {
		return;
	}
	QString msg = qFormatLogMessage(type, context, message);

	QString date = QDateTime::currentDateTime().toString(LOG_DATETIME_FORMAT);
	if(QtMsgType::QtDebugMsg == type) {
		msg = QString("%1\t[%2]\t").arg(date, "DEBUG").append(msg);
	} else if(QtMsgType::QtInfoMsg == type) {
		msg = QString("%1\t[%2]\t").arg(date, "INFO").append(msg);
	} else if(QtMsgType::QtWarningMsg == type) {
		msg = QString("%1\t[%2]\t").arg(date, "WARNING").append(msg);
	} else {
		msg = QString("%1\t[%2]\t").arg(date, "ERROR").append(msg);
	}

	if(!logDir.exists()) {
		logDir.mkpath(QStringLiteral("."));
	}

	QMutexLocker locker(&logMutex);

	QFileInfo logFileInfo = QFileInfo(logDir, QString(LOG_NAME).append(LOG_EXTENSION));

	// TODO if outputSize already bigger than the maximum size, maybe handle differently
	qint64 outputSize = msg.toUtf8().size();
	qint64 logSize = (outputSize<0 || outputSize>qint64(MAX_LOG_SIZE))? 0:outputSize;

	if((logFileInfo.size() + logSize) > qint64(MAX_LOG_SIZE)) {
		QFileInfo bakFileInfo = QFileInfo(logDir, QString(LOG_NAME).append(QString::number(1)).append(QString(LOG_EXTENSION)));
		QFile bakFile(bakFileInfo.absoluteFilePath());
		if(bakFileInfo.exists()) {
			bakFile.remove();
		}
		QFile logFile(logFileInfo.absoluteFilePath());
		logFile.rename(bakFileInfo.absoluteFilePath());
	}

	QFile tFile(logFileInfo.absoluteFilePath());
	if (tFile.open(QIODevice::Append | QIODevice::Text)) {
		tFile.write(msg.toUtf8() + "\n");
		tFile.flush();
	}
}
#ifdef Q_OS_WIN
QLibrary* VicoDaemon::loadDebugLibrary() {
	QLibrary *lib = new QLibrary(QStringLiteral("Dbghelp"));
	lib->setLoadHints(QLibrary::ResolveAllSymbolsHint | QLibrary::ExportExternalSymbolsHint);
	if(lib->load()) {
		qInfo() << QStringLiteral("[INTERNAL]\tLibrary") << lib->fileName() << QStringLiteral("was loaded successfully");
		return lib;
	} else {
		qWarning() << QStringLiteral("[INTERNAL]\tLibrary") << lib->fileName() << QStringLiteral("could not be loaded:") << lib->errorString();
		return nullptr;
	}
}

void VicoDaemon::printStackTrace(QLibrary *debugLibrary) {
	if(!debugLibrary) return;
	void *stack[TRACE_MAX_STACK_FRAMES];
	HANDLE process = GetCurrentProcess();
	p_SymInitialize symInit = (p_SymInitialize)debugLibrary->resolve("SymInitialize");
	if(!symInit) return;
	symInit(process, NULL, TRUE);
	WORD numberOfFrames = CaptureStackBackTrace(0, TRACE_MAX_STACK_FRAMES, stack, NULL);
	char buf[sizeof(SYMBOL_INFO)+(TRACE_MAX_FUNCTION_NAME_LENGTH - 1) * sizeof(TCHAR)];
	SYMBOL_INFO* symbol = (SYMBOL_INFO*)buf;
	symbol->MaxNameLen = TRACE_MAX_FUNCTION_NAME_LENGTH;
	symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
	DWORD displacement;
	IMAGEHLP_LINE64 line;
	line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
	for(int i=0;i<numberOfFrames;i++) {
		DWORD64 address = (DWORD64)(stack[i]);
		p_SymFromAddr symFromAddr = (p_SymFromAddr)debugLibrary->resolve("SymFromAddr");
		if(!symFromAddr) return;
		symFromAddr(process, address, NULL, symbol);
		p_SymGetLineFromAddr64 symGetLineFromAddr64 = (p_SymGetLineFromAddr64)debugLibrary->resolve("SymGetLineFromAddr64");
		if(symGetLineFromAddr64 && symGetLineFromAddr64(process, address, &displacement, &line)) {
			qCritical().noquote() << QStringLiteral("[INTERNAL]\tat") << QString::fromStdString(std::string(symbol->Name, symbol->NameLen))
								  << QStringLiteral("in") << QString::fromUtf8(line.FileName) << QStringLiteral(": line: ")
								  << QString::number(line.LineNumber) << QStringLiteral(": address: 0x").append(QString::number(symbol->Address, 16));
		} else {
			qCritical().noquote() << QStringLiteral("[INTERNAL]\t").append(QString::fromStdString(std::string(symbol->Name, symbol->NameLen)))
								  << QStringLiteral(", address 0x").append(QString::number(symbol->Address, 16)).append(QStringLiteral("."));
		}
	}
	qCritical().noquote() << QStringLiteral("[INTERNAL]\tSymGetLineFromAddr64 returned error code: ")
				   .append(QString::number(GetLastError()))
				   .append(QStringLiteral("."));
}
#endif
void VicoDaemon::signalHandler(int signum) {
	qCritical().noquote() << "[INTERNAL]\tInterrupt signal ("<< signum <<") received.";
#ifdef Q_OS_WIN
	QLibrary *debugLibrary = VicoDaemon::loadDebugLibrary();
	VicoDaemon::printStackTrace(debugLibrary);
	if(debugLibrary) delete debugLibrary;
#endif
	exit(signum);
}

void VicoDaemon::logMessage(const QString &message) {
	qDebug() << message;
}
