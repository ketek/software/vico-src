var a00050 =
[
    [ "hvMax", "a00050.html#a29904e9dd9477810aa6b6e245acd65a5", null ],
    [ "hvMin", "a00050.html#ac40095df101a4aa2a32b7dc53df4ac8f", null ],
    [ "mcu3v3Max", "a00050.html#a9f81e64ef839a44acee16b2f12637d01", null ],
    [ "mcu3v3Min", "a00050.html#afa066d79067bb484560060f9252a0bfc", null ],
    [ "p5vMax", "a00050.html#a1351bd414f1dcabc9d740d554b31c6f9", null ],
    [ "p5vMin", "a00050.html#ab569584e1a77b92eb7dc46529ce8a797", null ],
    [ "ref2v5Max", "a00050.html#a8f3eaba39b7fb6c61131d250d6ca3e5b", null ],
    [ "ref2v5Min", "a00050.html#a695308aa2e6f5f2079de44a18afde003", null ],
    [ "therm1Max", "a00050.html#afd70afcb6f3399871ef55cd12b876d01", null ],
    [ "therm2Max", "a00050.html#a0b95107825b9f6b86f126a5e155a6790", null ],
    [ "vInMax", "a00050.html#aa8212a257f9bff4bfcb794011e35892d", null ],
    [ "vInMin", "a00050.html#afbf9fe2625b44e5eb61f94cb15c8d60b", null ]
];