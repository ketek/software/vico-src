#ifndef VICODEVICE_H
#define VICODEVICE_H

#include <QtCore>
#include "UsbConnector.h"
#include "EthConnector.h"
#include "UsbHidConnector.h"
#include "IConnector.h"

/**
 * @brief The VicoDevice class represents a device, which is connected via a specific connector (i.e. USB, USB HID, TCP, UDP).
 */
class VicoDevice : public QObject
{
	Q_OBJECT

public:
	void setConnector(IConnector *connector);
	IConnector* getConnector();

	void setDeviceType(DeviceType devType);
	DeviceType getDeviceType();

	void setDeviceId(QString deviceId);
	QString getDeviceId();

signals:
	void sendingMsg();
	void msgReceived(VicoDevice *device, const QByteArray& msg);
	void disconnected();

public slots:
	void sendMsg(const QByteArray& msg, const quint16 size, const int &timeout = DEFAULT_TIMEOUT);
	void socketDisconnected();

private slots:
	void msgRec(const QByteArray& msg);

private:
	VicoDevice() = delete;
	VicoDevice(QObject *parent = nullptr);
	~VicoDevice();

	IConnector* connector;
	QString deviceId;
	DeviceType deviceType;
	friend class DeviceRegistry;
};

#endif // VICODEVICE_H
