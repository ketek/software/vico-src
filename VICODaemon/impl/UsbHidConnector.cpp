#include "UsbHidConnector.h"
#include "unistd.h"
#include "vicodaemon.h"
#define MAX_STR 255

UsbHidConnector::UsbHidConnector(hid_device *handle, QObject *parent) : QObject(parent)
{
	threadPool = new QThreadPool(this);
	threadPool->setMaxThreadCount(1);
	this->connectionType = INTERFACE_USB_HID;
	this->handle = handle;
	initializeConnection();
}

UsbHidConnector::~UsbHidConnector() {
}

void UsbHidConnector::initializeConnection() {
	int rc;
	wchar_t wstr[MAX_STR];

	// Initialize the hidapi library
	rc = hid_init();
	qDebug() << "[HID]\tInitialized hidapi with rc" << rc;

	// Open the device using the VID, PID,
	// and optionally the Serial number.
	handle = hid_open(0x20BD, 0x0001, NULL);
	if (handle == nullptr) {
		return;
	}
	// Read the Manufacturer String
	rc = hid_get_manufacturer_string(handle, wstr, MAX_STR);
	qDebug() << "[HID]\tManufacturer String:" << wstr << "with rc" << rc;

	// Read the Product String
	rc = hid_get_product_string(handle, wstr, MAX_STR);
	qDebug() << "[HID]\tProduct String:" << wstr << "with rc" << rc;

	// Read the Serial Number String
	rc = hid_get_serial_number_string(handle, wstr, MAX_STR);
	qDebug() << "[HID]\tSerial Number String:" << wstr[0] << "and full:" << wstr << "with rc" << rc;

	// Read Indexed String 1
	rc = hid_get_indexed_string(handle, 1, wstr, MAX_STR);
	qDebug() << "[HID]\tIndexed String 1:" << wstr << "with rc" << rc;

	// Set the hid_read() function to be non-blocking.
	rc = hid_set_nonblocking(handle, 1);
	qDebug() << "[HID]\tSet hidapi communication to non-blocking with rc" << rc;
}

void UsbHidConnector::closeConnection() {
	int rc;
	hid_close(handle);
	rc = hid_exit();
	qDebug() << "[HID]\tHID closed with status" << rc;
}

InterfaceType UsbHidConnector::getConnectionType() const {
	return this->connectionType;
}

void UsbHidConnector::sendMsg(const QByteArray& msg, const quint16 receiveSize, const int &timeout)
{
	int timeout_ticks = CLOCKS_PER_SEC*timeout; //timing declaration

	QRunnable *runnable = VICORunnable::create([this, timeout_ticks, msg, receiveSize] {

	int res;
	int hidReportLen = 64;

	int sendDataLength = msg.size()+1;

	// Advanced HID OUT logging
	QString msgOut (QStringLiteral("[DEV]\t[REQ]\t["));
	for (int i = 0; i < msg.size(); ++i) {
		msgOut.append(QString::number(msg[i]));
		if (i != msg.size()-1) {
			msgOut.append(QStringLiteral(","));
		}
	}
	msgOut.append(QStringLiteral("]"));
	qDebug().noquote() << msgOut;

	if (sendDataLength <= hidReportLen) {
		// Size to send is +1 for USB, as the index byte ocuppies the first position (0x00)
		unsigned char sendData[sendDataLength];
		memset(sendData, 0, sizeof(sendDataLength));
		sendData[0] = 0x0;
		for (int i = 0; i < msg.size(); ++i)
		{
			sendData[i+1] = msg[i];
		}
		// For datagrams shorter than a hidReport (64 bytes)
		res = hid_write(handle, &sendData[0], 65);
	}
	// For datagrams longer than a hidReport (64 bytes)
	else {
		int datagram_size = msg.size();
		int head_array = 0;
		while (datagram_size > 0) {
			if (datagram_size == msg.size()) {
				unsigned char buffer_tx[64] = {0};
				buffer_tx[0] = 0x0;
				for (int i = 0; i< 63; i++) {
					buffer_tx[i+1] = msg[i];
					datagram_size = datagram_size-1;
					head_array++;
				}
				res = hid_write(handle, buffer_tx, 64);
			}
			else {
				if (datagram_size > 63) {
					unsigned char buffer_tx[64] = {0};
					buffer_tx[0] = 0x0;
					for (int i = 0; i<63;i++) {
						buffer_tx[i+1] = msg[head_array];
						datagram_size = datagram_size-1;
						head_array++;
					}
					res = hid_write(handle, buffer_tx, 64);
				}
				//the size's packet of the last iteration mustnt be equal hidReport_len
				else {
					unsigned char buffer_tx[64] = {0};
					buffer_tx[0] = 0x0;
					int i;
					for (i = 0; i<datagram_size;i++) {
						buffer_tx[i+1] = msg[head_array];
						head_array++;
					}
					res = hid_write(handle, buffer_tx, 64);
					datagram_size = datagram_size-i;
				}
			}
		}
	}

	QByteArray answerArray;

	unsigned char receiveData[receiveSize];
	memset(receiveData, 0, sizeof(receiveSize));
	if (receiveSize <= hidReportLen) {

		// Currently called non-blocking
		res = 0;
		int timeout = 0;
		while (res == 0) {
			res = hid_read(handle, &receiveData[0], sizeof(receiveData));
			if (res == 0) {
				timeout++;
				usleep(10000);
			}
			if (res < 0) {
				timeout++;
				usleep(10000);
			}
			// Disconnect as soon as the timeout ticks limit is reached
			if (timeout > (timeout_ticks/10)) {
				return;
			}
		}

		for (int i = 0; i < receiveSize; i++)
		{
			answerArray.append((uint)receiveData[i]);
		}
	}

	else {
		//if datagram size is longer than hid report (64 bytes)
		if (receiveSize > hidReportLen) {
			int datagramLeft = receiveSize;
			// If the remaining datagram is still greater equal hidReportLen, we can get one full
			 while (datagramLeft - hidReportLen >= 0) {
				 memset(receiveData, 0, sizeof(receiveData));

				 // Currently called non-blocking
				 res = 0;
				 int timeout = 0;
				 while (res == 0) {
					 res = hid_read(handle, &receiveData[0], sizeof(receiveData));
					 if (res == 0) {
						 timeout++;
						 usleep(10000);
					 }
					 if (res < 0) {
						 timeout++;
						 usleep(10000);
					 }
					 // Disconnect as soon as the timeout ticks limit is reached
					 if (timeout > (timeout_ticks/10)) {
						 return;
					 }
				 }

				 for (int i = 0; i < hidReportLen; i++)
				 {
					 answerArray.append((uint)receiveData[i]);
				 }
				 datagramLeft -= hidReportLen;
			 }
			 if (datagramLeft > 0) {
				 memset(receiveData, 0, sizeof(receiveData));

				 // Currently called non-blocking
				 res = 0;
				 int timeout = 0;
				 while (res == 0) {
					 res = hid_read(handle, &receiveData[0], sizeof(receiveData));
					 if (res == 0) {
						 timeout++;
						 usleep(10000);
					 }
					 if (res < 0) {
						 timeout++;
						 usleep(10000);
					 }
					 // Disconnect as soon as the timeout ticks limit is reached
					 if (timeout > (timeout_ticks/10)) {
						 return;
					 }
				 }
				 for (int i = 0; i < datagramLeft; i++)
				 {
					 answerArray.append((uint)receiveData[i]);
				 }
			 }
		}
	}

	emit msgReceived(answerArray);
	});
	threadPool->start(runnable);
	threadPool->waitForDone(int(timeout_ticks));
}
