#include <QTextStream>

#include "vicodaemon.h"

/**
 * @brief Entry point for VICODaemon service executable.
 * @param argc counter of CLI arguments.
 * @param argv CLI parameters.
 * @return the return code of the service execution.
 */
int main(int argc, char *argv[])
{
	QTextStream out(stdout);
	out << "Starting service." << Qt::endl;
	out.flush();
	VicoDaemon svc{argc, argv};
	return svc.exec();
}
