#include "vicolib.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

/* Local function prototypes */
static void CHECK_ERROR(int status);

int main() {
    /* Define status variable */
    int status;

    /* Scan USB and Ethernet interfaces (in a range between 192.168.0.2 and 192.168.0.2) for KETEK devices */
    status = scanUsbDevices();
    CHECK_ERROR(status);

    status = scanTcpDevices("192.168.0.2", "192.168.0.2");
    CHECK_ERROR(status);

    UInt8Type numberOfDevices = 0;
    status = getNumberOfDevices( & numberOfDevices);
    CHECK_ERROR(status);
    if (numberOfDevices == 0) {
        printf("No device found. Aborting.\n");
        return -1;
    }

    UInt8Type serialNumberSize = 12;
    CharType serialNumber[serialNumberSize];
    UInt8Type ipAddressSize = 12;
    CharType ipAddress[ipAddressSize];
    InterfaceType deviceInterface;

    /* Display all found devices */
    printf("Successfully found %d devices.\n", numberOfDevices);
    for (int i = 0; i < numberOfDevices; i++) {
        status = getDeviceInfoByIndex(i, serialNumberSize, serialNumber, & deviceInterface);
        CHECK_ERROR(status);
        printf("Device #%d serial number: %s ", i, serialNumber);
        switch (deviceInterface) {
        case INTERFACE_ETHERNET_TCP:
            status = getIPAddress(serialNumber, ipAddressSize, ipAddress);
            CHECK_ERROR(status);
            printf("connected over TCP interface with IP %s.\n", ipAddress);
            break;
        case INTERFACE_ETHERNET_UDP:
            printf("connected over UDP interface.\n");
            break;
        case INTERFACE_UNKNOWN:
            printf("connected over unknown interface.\n");
            break;
        case INTERFACE_USB:
            printf("connected over USB interface.\n");
            break;
        case INTERFACE_USB_HID:
            printf("connected over USB interface (HID).\n");
            break;
        }
    }

    /* In this example just proceed with the first found device */
    status = getDeviceInfoByIndex(0, serialNumberSize, serialNumber, & deviceInterface);
    CHECK_ERROR(status);
    status = getIPAddress(serialNumber, ipAddressSize, ipAddress);
    CHECK_ERROR(status);
    printf("Successfully initialized device with serial number %s and IP address %s.\n", serialNumber, ipAddress);

    return 0;
}

/* Simple error handling function */
static void CHECK_ERROR(int status) {
    if (status != DPP_SUCCESS && status != VICO_SUCCESS && status != MCU_SUCCESS) {
        printf("Error encountered! Status = %#02x, Press ENTER to exit.\n", status);
        getchar();
        exit(status);
    }
}