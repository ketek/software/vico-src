#include "ClientQuery.h"

ClientQuery::ClientQuery(QObject *parent) : QObject(parent), socket(new QLocalSocket(this))
{
	qRegisterMetaType<QLocalSocket::LocalSocketError>();
	connect(socket, &QLocalSocket::readyRead, this, &ClientQuery::onReadyRead, Qt::DirectConnection);
	connect(socket, &QLocalSocket::disconnected, this, &ClientQuery::disconnectedFromClient);
	connect(socket, &QLocalSocket::errorOccurred, this, &ClientQuery::onErrorOccurred);
}

bool ClientQuery::setSocketDescriptor(qintptr socketDescriptor) {
	return socket->setSocketDescriptor(socketDescriptor);
}

void ClientQuery::disconnectFromClient() {
	qDebug() << "ClientQuery disconnected!";
	socket->disconnectFromServer();
}

void ClientQuery::onReadyRead() {
	QJsonParseError parseError;
	const QJsonDocument jsonDoc = QJsonDocument::fromJson(socket->readAll(), &parseError);
	if (parseError.error == QJsonParseError::NoError) {
		// if the data was indeed valid JSON
		if (jsonDoc.isObject()) // and is a JSON object
			jsonReceived(jsonDoc.object()); // parse the JSON
	}
}

void ClientQuery::onErrorOccurred(QLocalSocket::LocalSocketError socketError) {
	qCritical().noquote() << "Error occurred in ClientQuery" << "(Error type:" << int(socketError) << ", Error Message:" << socket->errorString() << ")";
	emit error();
}

void ClientQuery::jsonReceived(const QJsonObject &doc) {
	emit queryReceived(doc);
}

void ClientQuery::answerQuery(const QJsonObject &data) {
	connect(socket, &QLocalSocket::disconnected, socket, &QLocalSocket::deleteLater);

    QByteArray json = QJsonDocument(data).toJson();
    socket->write(qCompress(json, 1));
	socket->disconnectFromServer();
}
