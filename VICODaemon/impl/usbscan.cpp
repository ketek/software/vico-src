#include "usbscan.h"

UsbScan::UsbScan(QObject *parent) : QObject(parent){
}

inline std::string DeviceFlagToString(DWORD flags)
{
	std::string msg;
	msg += (flags & 0x1)? "DEVICE_OPEN" : "DEVICE_CLOSED";
	msg += ", ";
	msg += (flags & 0x2)? "High-speed USB" : "Full-speed USB";
	return msg;
}

void UsbScan::createUsbDevice(QString dataSerNo, QString gpioSerNo)
{
	FT_HANDLE ftHandle = NULL;
	FT_HANDLE ftHandleGPIO = NULL;
	bool connec = false;
	VicoDevice* newDevice = DeviceRegistry::getInstance().createVICODevice();
	// Try to connect to the USB device asynchronously
	QTimer timer;
	timer.setSingleShot(true);
	QEventLoop loop;
	connect(&DeviceRegistry::getInstance(), &DeviceRegistry::deviceFound, &loop, [&] {
		connec = true;
		loop.quit();
		timer.stop();
	});
	connect( &timer, &QTimer::timeout, &loop, &QEventLoop::quit);
	IConnector *usbConnector = new UsbConnector(ftHandle, ftHandleGPIO, dataSerNo, gpioSerNo, newDevice);
	DeviceRegistry::getInstance().addDevice(newDevice, usbConnector);
	timer.start(2000);
	// If the device is not yet connected, wait for the event loop to have the device connected
	if (!connec) {
		loop.exec();
		timer.stop();
	}
	// Check if after the the event loop (or timeout) the device could be connected or not
	if (!connec) {
		qDebug() << "[USB]\tDevice with datastream serial number" << dataSerNo << "and GPIO serial number" << gpioSerNo << "couldn't be connected via USB.";
	}
	else {
		qDebug() << "[USB]\tDevice with datastream serial number" << dataSerNo << "and GPIO serial number" << gpioSerNo << "is connected successfully via USB.";
	}
}

void UsbScan::listFtUsbDevices() {
	FT_STATUS ftStatus = 0;
	DWORD numOfDevices = 0;
	QStringList dataSerNos = {};
	QStringList gpioSerNos = {};

	QString ftDataSerNo = QStringLiteral("");
	QString ftGpioSerNo = QStringLiteral("");
#ifndef Q_OS_WIN
	ftStatus = FT_SetVIDPID(0x20BD, 0x0002);
	if(FT_OK == ftStatus) {
		qDebug() << "[USB]\tThe Vendor ID was successfully set to 0x20BD and the Product ID to 0x0002";
	} else {
		qCritical() << "[USB]\tThe Vendor ID and the Product ID were failed to be set: " << ftStatus;
	}
#endif
	ftStatus = FT_CreateDeviceInfoList(&numOfDevices);
	if (numOfDevices == 0) {
		qDebug() << "[USB]\tNo FT4222 device is found!";
	}
	else {
		qDebug() << "[USB]\tNumber of Devices:" << numOfDevices;
	}

	for(DWORD iDev=0; iDev<numOfDevices; ++iDev)
	{
		FT_DEVICE_LIST_INFO_NODE devInfo;
		memset(&devInfo, 0, sizeof(devInfo));

		ftStatus = FT_GetDeviceInfoDetail(iDev, &devInfo.Flags, &devInfo.Type, &devInfo.ID, &devInfo.LocId,
										devInfo.SerialNumber,
										devInfo.Description,
										&devInfo.ftHandle);

		if (FT_OK == ftStatus)
		{
			qDebug()<<"[USB]\tDev"<< iDev;
			qDebug()<<"[USB]\tFlags="<< devInfo.Flags<< DeviceFlagToString(devInfo.Flags).c_str();
			qDebug()<<"[USB]\tType="<<        devInfo.Type;
			qDebug()<<"[USB]\tID="<<          devInfo.ID;
			qDebug()<<"[USB]\tLocId="<<       devInfo.LocId;
#ifdef Q_OS_WIN
            qDebug()<<"[USB]\tSerialNumber="<<  devInfo.SerialNumber;
#else
            FT_STATUS serialNumberStatus = FT_ListDevices((PVOID)iDev, devInfo.SerialNumber, FT_LIST_BY_INDEX | FT_OPEN_BY_SERIAL_NUMBER);
            if(FT_OK == serialNumberStatus) {
                qDebug()<<"[USB]\tSerialNumber="<<  devInfo.SerialNumber;
            } else {
                qWarning()<<"[USB]\tError getting the serial number:"<<  serialNumberStatus;
            }
#endif
			qDebug()<<"[USB]\tDescription="<<   devInfo.Description;
			qDebug()<<"[USB]\tftHandle="<<    devInfo.ftHandle;

			const std::string serialNumber = devInfo.SerialNumber;
			const std::string desc = devInfo.Description;

			// FT USB devices have the vendor-ID=0x0403 and product-ID=0x601C
			if(devInfo.ID == 0x0403601C && desc == "FT4222 A")
			{
				ftDataSerNo = QString::fromStdString(serialNumber);
			}
			// Complete VICO-DV devices will have the vendor-ID=0x20BD and product-ID=0x0002 (info: VICO-AVs have product-ID=0x0001)
			else if(devInfo.ID == 0x20BD0002 && desc=="EVICO-DV_3.0 A")
			{
				dataSerNos.append(QString::fromStdString(serialNumber));
			}

			// FT USB devices will have the vendor-ID=0x0403 and product-ID=0x601C
			if(devInfo.ID == 0x0403601C && desc == "FT4222 B")
			{
				ftGpioSerNo = QString::fromStdString(serialNumber);
			}
			// Complete VICO-DV devices will have the vendor-ID=0x20BD and product-ID=0x0002 (info: VICO-AVs have product-ID=0x0001)
			else if(devInfo.ID == 0x20BD0002 && desc=="EVICO-DV_3.0 B")
			{
				gpioSerNos.append(QString::fromStdString(serialNumber));
			}
		}
	}

	// For the FT4222 datastream/gpio pair create one USB device
	if (ftGpioSerNo.size() != 0 && ftDataSerNo.size() != 0) {
		createUsbDevice(ftDataSerNo, ftGpioSerNo);
	}

	// For each VICO datastream/gpio pair create an USB device
	for (int i = 0; i < dataSerNos.size(); ++i) {
		QString dataSerNumber = dataSerNos.at(i);
		qDebug() <<"[USB]\tFound datastream device with serial number" << dataSerNumber << "- now retrieving dedicated GPIO serial number.";
		QStringList gpioSerNumbers = gpioSerNos.filter(QRegularExpression(dataSerNumber.left(12)));
		if (gpioSerNumbers.size() == 0) {
			qDebug() << "[USB]\tWarning: No GPIO serial number could be found for the data serial number" << dataSerNumber;
		}
		else if (gpioSerNumbers.size() == 1) {
			qDebug() << "[USB]\tGPIO serial number for device" << dataSerNumber << "could be found and is" << gpioSerNumbers.first();
			createUsbDevice(dataSerNumber, gpioSerNumbers.first());
		}
		else {
			qDebug() << "[USB]\tWarning: More than one GPIO serial number was found for the data serial number" << dataSerNumber << ": " << gpioSerNumbers.join(QStringLiteral(", "));
		}
	}
}

const QList<QString> UsbScan::start()
{
	// Default value to connect to USB
	int USB_WAIT_TO_CONNECT_ms = 2500;
	QDirIterator it(QCoreApplication::applicationDirPath(), QStringList() << QStringLiteral("settings.json"), QDir::Files, QDirIterator::Subdirectories);
	QString settings;
	while (it.hasNext()) {
		settings = it.next();
		qDebug() << settings;
		if (!settings.isEmpty()) {
			QFile file(settings);
			if (!file.open(QIODevice::ReadOnly)) {
				qDebug("[USB]\tCouldn't open settings.json.");
			}
			qDebug("[USB]\tLoaded settings.json.");
			QByteArray byteArray = file.readAll();

			QJsonDocument parametersDoc(QJsonDocument::fromJson(byteArray));
			QJsonObject json = parametersDoc.object();

			int usbWaitToConnect = json.value(QStringLiteral("USB_WAIT_TO_CONNECT_ms")).toInt();
			qDebug() << "[USB]\tSuccessfully read USB_WAIT_TO_CONNECT_ms:" << usbWaitToConnect;
			if (usbWaitToConnect > 50 && usbWaitToConnect < 20000) {
				USB_WAIT_TO_CONNECT_ms = usbWaitToConnect;
				qDebug() << "[USB]\tSuccessfully set USB_WAIT_TO_CONNECT_ms by settings file to :" << USB_WAIT_TO_CONNECT_ms;
			}
		}
	}


	//QThread::msleep(USB_WAIT_TO_CONNECT_ms);
	// Each time a scanning is started, remove all previously scanned devices
	DeviceRegistry::getInstance().removeDevicesOfConnection(INTERFACE_USB);
	//QThread::msleep(USB_WAIT_TO_CONNECT_ms);
	listFtUsbDevices();
	// QueryServer gets the signal
	emit scanFinished();
	return usb_strings;
}

const QList<QString> UsbScan::getUsbDevices()
{
	return usb_strings;
}
