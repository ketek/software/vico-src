#ifndef USBHIDCONNECTOR_H
#define USBHIDCONNECTOR_H

#include <QObject>
#include <QDebug>

#include "hidapi.h"

#include "IConnector.h"
#include "types.h"

/**
 * @brief The UsbHidConnector class creates or closes a connection to a device via HID USB.
 */
class UsbHidConnector : public QObject, public IConnector
{
	Q_OBJECT
	Q_INTERFACES(IConnector)
public:
	explicit UsbHidConnector(hid_device* handle, QObject *parent = nullptr);
	~UsbHidConnector();

	InterfaceType getConnectionType() const override;
	void initializeConnection() override;
	void closeConnection() override;
	hid_device *handle;

signals:
	void msgReceived(const QByteArray& msg) override;


public slots:
	void sendMsg(const QByteArray& msg, const quint16 size, const int &timeout = DEFAULT_TIMEOUT) override;

private:

	InterfaceType connectionType;
	QThreadPool *threadPool;
};

#endif // USBHIDCONNECTOR_H
