#include "DeviceRegistry.h"
#include "vicodaemon.h"
DeviceRegistry::DeviceRegistry(QObject* parent) : QObject(parent) {

}

void DeviceRegistry::addDevice(VicoDevice *newDevice, IConnector *connector) {
	newDevice->setConnector(connector);
	QByteArray q_id;
	// USB HID is only applicable for VICO-AV -> we must send a MCU DG
	if (connector->getConnectionType() == INTERFACE_USB_HID) {
		q_id.resize(5);
		newDevice->setDeviceType(VICO_AV);
		// Prepare MCU DEVINFO2_VICO DG
		q_id[0] = 0x01;
		q_id[1] = 0x1B;
		q_id[2] = 0x05;
		q_id[3] = 0x15;
		q_id[4] = 0x55;
		connect(newDevice, &VicoDevice::msgReceived, this, &DeviceRegistry::getVicoReply);
		newDevice->sendMsg(q_id, 90);
	} else {
		quint8 size = 12;
		q_id.resize(size);
		newDevice->setDeviceType(VICO_DV);
		// Request full mac address in the form 5894A2[01][23][45]
		// Prepare DPP MAC Address DG 4-5
		q_id[0] = PARAMETER_ID_MAC_4_5;
		q_id[1] = 0;
		q_id[2] = 0;
		q_id[3] = 0;
		// Prepare DPP MAC Address DG 2-3
		q_id[4] = PARAMETER_ID_MAC_2_3;
		q_id[5] = 0;
		q_id[6] = 0;
		q_id[7] = 0;
		// Prepare DPP MAC Address DG 0-1
		q_id[8] = PARAMETER_ID_MAC_0_1;
		q_id[9] = 0;
		q_id[10] = 0;
		q_id[11] = 0;
		connect(newDevice, &VicoDevice::msgReceived, this, &DeviceRegistry::getVicoReply);
		newDevice->sendMsg(q_id, size);
	}

}

void DeviceRegistry::removeDevice(quint64 deviceId) {
	if(deviceCollection.contains(deviceId)) {
		qDebug() << "[INTERNAL]\tRemoved device" << QString::number(deviceId, 16).toUpper();
		deviceCollection.removeDevices(deviceId);
		emit deviceRemoved();
	}
}

void DeviceRegistry::removeDevice(VicoDevice *device) {
	if(deviceCollection.contains(device)) {
		qDebug() << "[INTERNAL]\tRemoved device" << device->getDeviceId();
		deviceCollection.removeDevice(device);
		emit deviceRemoved();
	}
}

void DeviceRegistry::removeDeviceWithInterface(VicoDevice *device, InterfaceType interfaceType) {
	if(deviceCollection.contains(device)) {
		qDebug() << "[INTERNAL]\tRemoved device" << device->getDeviceId();
		deviceCollection.removeDeviceWithInterface(device, interfaceType);
		quint64 deviceId = convertDeviceID(device->getDeviceId());
		if(interfaceType == getPreferredInterface(deviceId)) {
			removePreferredInterface(deviceId);
		}
		emit deviceRemoved();
	}
}

VicoDevice *DeviceRegistry::createVICODevice() {
	return new VicoDevice(this);
}

void DeviceRegistry::getVicoReply(VicoDevice *device, const QByteArray& msg) {
	// Connection via TCP/UDP/USB: Awaiting stacked datagram which contains three single mac address datagram answers
	if (msg.size() > 11 && msg[0] == PARAMETER_ID_MAC_4_5 && msg[4] == PARAMETER_ID_MAC_2_3 && msg[8] == PARAMETER_ID_MAC_0_1) {
		quint64 mac0 = msg[2];
		quint64 mac1 = msg[3];
		quint64 mac2 = msg[6];
		quint64 mac3 = msg[7];
		quint64 mac4 = msg[10];
		quint64 mac5 = msg[11];
		quint64 macAddress = 0x00005894A2000000U | (quint64((0xFF&mac4)<<40) | quint64((0xFF&mac5)<<32) | quint64((0xFF&mac2)<<24) | quint64((0xFF&mac3)<<16) | quint64((0xFF&mac0)<<8) | quint64(0xFF&mac1));
		QString macAddrStr = QString::number(macAddress, 16).toUpper();
		qDebug() << "[INTERNAL]\tFound VICO-DV with serialnumber" << macAddrStr;
		device->setDeviceId(macAddrStr);
		deviceCollection.addDevice(device);
	}
	// Connection via HID: Awaiting answer of DEVINFO2_VICO
	else if (msg.size() == 90 && msg[1] == DEVINFO2_VICO_CIX) {
		QString serNo;
		for (int i=69; i<=80; i++) {
			serNo.append(msg[i]);
		}
		// The fall-back serialnumber is used in case the EEPROM doesn't contain a valid hexadecimal serial number
		bool conversionOk;
		QString serNoStr;
		quint64 hex = serNo.toULongLong(&conversionOk, 16);
		if (conversionOk) {
			serNoStr = QString::number(hex, 16).toUpper();
			qDebug() << "[INTERNAL]\tCould successfully convert serialnumber from EEPROM:" << serNoStr;
		}
		else {
			serNoStr = QString::number(0x5894A2FFFFFFU, 16).toUpper();
			qDebug() << "[INTERNAL]\tCouldn't convert serialnumber from EEPROM. Current value" << serNo << "- falling back to" << serNoStr;
		}

		qDebug() << "[INTERNAL]\tFound VICO-AV with serialnumber" << serNoStr;
		device->setDeviceId(serNoStr);        deviceCollection.addDevice(device);
	}
	else {
		qDebug() << "[INTERNAL]\tNo device found with given request.";
	}
	disconnect(device, &VicoDevice::msgReceived, this, &DeviceRegistry::getVicoReply);
	emit deviceFound(device);
}

VicoDevice* DeviceRegistry::getDeviceById(quint64 id) {
	if(deviceCollection.contains(id)) {
		InterfaceType preferredInterface = getPreferredInterface(id);
		VicoDevice  *foundDevice = deviceCollection.getDevice(id, preferredInterface);
		return foundDevice;
	} else {
		return nullptr;
	}
}

DeviceRegistry &DeviceRegistry::getInstance() {
	static DeviceRegistry deviceRegistry(nullptr);
	return deviceRegistry;
}

const QVector<VicoDevice*> DeviceRegistry::getDevices() {
	return deviceCollection.getDevices();
}

const QVector<VicoDevice*> DeviceRegistry::getDevicesByConnection(InterfaceType type) {
	return deviceCollection.getDevices(QVector<InterfaceType>() << type);
}

void DeviceRegistry::removeDevicesOfConnection(InterfaceType type) {
	QVector<VicoDevice*> devices = getDevicesByConnection(type);
	for(VicoDevice *device : qAsConst(devices)) {
		removeDevice(device);
	}
}

const QVector<quint64> DeviceRegistry::getAllSerialNumbers() {
	return deviceCollection.getSerialNumbers();
}

VicoDevice *DeviceRegistry::getTCPConnectedDevice(quint32 ip) {
	return deviceCollection.getTCPConnectedDevice(ip);
}

QHostAddress DeviceRegistry::getDeviceIPFromDaemon(const QString serialNumber, VICOStatusType *statusCode) {
	return deviceCollection.getDeviceIPFromDaemon(serialNumber, statusCode);
}

void DeviceRegistry::readReply() {
	QByteArray byteArray;
	in.startTransaction();
	in >> byteArray;
	if(!in.commitTransaction())
		return;
}

quint64 DeviceRegistry::convertDeviceID(const QString deviceId) {
	return deviceId.toULongLong(nullptr, 16);
}

QString DeviceRegistry::convertDeviceID(const quint64 deviceId) {
	return QString::number(deviceId, 16).toUpper();
}

bool DeviceRegistry::pingDevice(VicoDevice *device) {
	if(device) {
		QByteArray q_id;
		q_id.resize(4);
		q_id[0] = PARAMETER_ID_MAC_0_1;  // Returns the MSBytes
		q_id[1] = 0;    // Read command
		q_id[2] = 0;    // No data
		q_id[3] = 0;    // No data

		QEventLoop eventLoop;
		bool success = false;
		QTimer timer;
		timer.setInterval(PING_TIMEOUT);
		connect(&timer, &QTimer::timeout, &eventLoop, [&] {
			success = false;
			eventLoop.quit();
		});
		connect(device, &VicoDevice::msgReceived, &eventLoop, [&] {
			success = true;
			eventLoop.quit();
		});
		connect(device, &VicoDevice::sendingMsg, &timer, [&] {
			timer.start();
		});
		device->sendMsg(q_id, q_id.size(), PING_TIMEOUT/1000);
		if(timer.remainingTime() != 0) {
			eventLoop.exec();
		}
		timer.stop();
		if(success) {
			qDebug() << "[INTERNAL]\tDevice pinging to" << device->getDeviceId() << "successful.";
		} else {
			qDebug() << "[INTERNAL]\tCouldn't ping device" << device->getDeviceId();
		}
		return success;
	} else {
		return false;
	}
}

void DeviceRegistry::insertHostAddress(const quint32 &deviceIpAddress, const quint32 &hostIpAddress, const bool &weakCache) {
	hostAddresses.insertHostAddress(deviceIpAddress, hostIpAddress, weakCache);
}

QHostAddress DeviceRegistry::getHostAddress(const quint32 &subnet, const int &netmask) const {
	return hostAddresses.getHostAddress(subnet, netmask);
}

QHostAddress DeviceRegistry::getHostAddress(const quint32 &deviceIpAddress) const {
	return hostAddresses.getHostAddress(deviceIpAddress);
}

void DeviceRegistry::removeHostAddress(const quint32 &deviceIpAddress) {
	hostAddresses.removeHostAddress(deviceIpAddress);
}

InterfaceType DeviceRegistry::getPreferredInterface(quint64 deviceId) {
	return preferredInterfaces.value(deviceId, InterfaceType::INTERFACE_UNKNOWN);
}

void DeviceRegistry::setPreferredInterface(quint64 deviceId, InterfaceType preferredInterface) {
	preferredInterfaces.insert(deviceId, preferredInterface);
}

void DeviceRegistry::removePreferredInterface(quint64 deviceId) {
	preferredInterfaces.remove(deviceId);
}

void DeviceRegistry::refreshDeviceConnections() {
	QVector<VicoDevice*> devices = deviceCollection.getDevices();
	for(VicoDevice *device : qAsConst(devices)) {
		// We handle refreshing the device connections asynchronously
		bool deviceAvailable = pingDevice(device);
		if(deviceAvailable) {
			qDebug() << "[INTERNAL]\tDevice pinging to" << device->getDeviceId() << "successful.";
		} else {
			qDebug() << "[INTERNAL]\tCouldn't ping device" << device->getDeviceId() << "- removing device.";
			removeDevice(device);
		}
	}
}

