#include "TcpScan.h"

TcpScan::TcpScan(QObject *parent) : QObject(parent) {
	stop_f=false;
}

void TcpScan::scan(quint32 ip, const bool &useBinding)
{
	// For each retry start from complete scratch again
	QHostAddress hostIpAddress;
	if(useBinding) {
		hostIpAddress = DeviceRegistry::getInstance().getHostAddress(ip);
	}
	for(int i=0;i<AMOUNT_RETRIES;i++) {
		QCoreApplication::processEvents();
		QTcpSocket* socket = new QTcpSocket;
		if(useBinding && hostIpAddress.isNull()) {
			qWarning().noquote() << "[TCP]\tHost IP Address could not be found even though binding was specified to be used";
		} else if(useBinding && socket->bind(hostIpAddress)) {
			qInfo().noquote() << "[TCP]\tHost of IP Address" << hostIpAddress.toString() << "was bound to the TCP Socket";
		} else if(useBinding) {
			qWarning().noquote() << "[TCP]\tHost of IP Address" << hostIpAddress.toString() << "could not be bound to the TCP Socket";
		}
		connect(socket, &QTcpSocket::hostFound, this, [&] {
			qDebug() << "[TCP]\tHost was found.";
		});
		QEventLoop eventLoop;
		connect(socket, &QTcpSocket::connected, &eventLoop, &QEventLoop::quit);
		connect(socket, &QTcpSocket::stateChanged, this, [&](QAbstractSocket::SocketState socketState) {
			qDebug() << "[TCP]\tSocket state changed to" << socketState;
		});
		time_t end_time = time(NULL) + TIMEOUT_SCANNING_PER_IP_S;
		while (time(NULL) < end_time) {
			QTimer::singleShot(TIMEOUT_SOCKET_CONNECT_MS, &eventLoop, &QEventLoop::quit);
			socket->connectToHost(QHostAddress(ip), dest_port);
			qDebug() << "[TCP]\tSocket state" << socket->state();
			qDebug().noquote() << "[TCP]\tConnecting to ip" << QHostAddress(ip).toString() << "on try number" << i;
			eventLoop.exec();
			qDebug() << "[TCP]\tSocket state" << socket->state();
			if (socket->state() == QAbstractSocket::ConnectedState){
				bool connec = false;
				// We handle adding the device asynchronously
				QTimer timer;
				timer.setSingleShot(true);
				QEventLoop loop;
				connect(&DeviceRegistry::getInstance(), &DeviceRegistry::deviceFound, &loop, [&] {
					qDebug() << "[TCP]\tDevice found.";
					connec = true;
					loop.quit();
				});
				connect( &timer, &QTimer::timeout, &loop, &QEventLoop::quit);
				VicoDevice* newDevice = DeviceRegistry::getInstance().createVICODevice();
				IConnector *ethConnector = new EthConnector(socket, newDevice);
				DeviceRegistry::getInstance().addDevice(newDevice, ethConnector);
				timer.start(TIMEOUT_WAIT_FOR_DEVICE_RESPONSE);
				loop.exec();
				if (!connec) {
					qDebug() << "[TCP]\tNot connected.";
					timer.stop();
				}
				else {
					qDebug().noquote() << "[TCP]\tTCP connected to" << QHostAddress(ip).toString() << "through host address" << socket->localAddress().toString();
					DeviceRegistry::getInstance().insertHostAddress(QHostAddress(ip).toIPv4Address(), socket->localAddress().toIPv4Address(), true);
					// If the user has set a value bigger than the default (1) retry value, output this retry-specific info message
					if (AMOUNT_RETRIES > 1) {
						qDebug() << "Success at" << i+1 << "/" << AMOUNT_RETRIES << "retries.";
					}
				}
				return;
			} else {
				EthConnector::closeConnection(socket);
			}
		}
		// For each retry start from complete scratch again
		if (socket->state() != QAbstractSocket::ConnectedState){
			qDebug() << "[TCP]\tThe socket could not be connected due to" << socket->errorString() << socket->state();
			EthConnector::closeConnection(socket);
			socket->deleteLater();
		}
	}
}

QVector<VicoDevice*> TcpScan::reconnectPreviousDevices(QMap<quint32, VicoDevice *> &previousDevices) {
	QVector<VicoDevice*> reconnectedDevices;
	QList<quint32> keys = previousDevices.keys();
	for(const quint32 &ip : qAsConst(keys)) {
		VicoDevice *prevDevice = previousDevices.value(ip, nullptr);
		if (prevDevice == nullptr) {
			qDebug() << "[TCP]\tThere is no previous device existing with ip " <<ip;
		}
		else {
			bool pinged = DeviceRegistry::getInstance().pingDevice(prevDevice);
			if(pinged) {
				reconnectedDevices.append(prevDevice);
			} else {
			}
		}
	}
	return reconnectedDevices;
}

QMap<quint32, VicoDevice *> TcpScan::getPreviousDevices(quint32 firstIP, quint32 lastIP) {
	QMap<quint32, VicoDevice*> previousDevices;
	for(quint32 i=firstIP;i<=lastIP;i++) {
		VicoDevice *prevDevice = DeviceRegistry::getInstance().getTCPConnectedDevice(i);
		if(prevDevice) {
			previousDevices.insert(i, prevDevice);
		}
		else {
		}
	}
	return previousDevices;
}

void TcpScan::removeUnconnectedDevices(QVector<quint32> &ipRange, QMap<quint32, VicoDevice *> &previousDevices, QVector<VicoDevice *> &reconnectedDevices) {
	QList<quint32> keys = previousDevices.keys();
	for(const quint32 &ip : qAsConst(keys)) {
		VicoDevice *prevDevice = previousDevices.value(ip, nullptr);
		if(prevDevice && reconnectedDevices.contains(prevDevice)) {
			ipRange.removeAll(ip);
		} else if(prevDevice){
			DeviceRegistry::getInstance().removeDevice(prevDevice);
		}
		else {
		}
	}
}

void TcpScan::start(QString firstip, QString lastip, const bool &useBinding) {
	// Default value  is 1000ms
	this->TIMEOUT_SOCKET_CONNECT_MS = 1000;
	this->TIMEOUT_SCANNING_PER_IP_S = 5;
	this->AMOUNT_RETRIES = 1;
	QDirIterator it(QCoreApplication::applicationDirPath(), QStringList() << QLatin1String("settings.json"), QDir::Files, QDirIterator::Subdirectories);
	QString settings;
	while (it.hasNext()) {
		settings = it.next();
		qDebug("[TCP]\tFound settings.json.");
		if (!settings.isEmpty()) {
			QFile file(settings);
			if (!file.open(QIODevice::ReadOnly)) {
				qDebug("[TCP]\tCouldn't open settings.json.");
			}
			qDebug("[TCP]\tOpened settings.json.");
			QByteArray byteArray = file.readAll();

			QJsonDocument parametersDoc(QJsonDocument::fromJson(byteArray));
			QJsonObject json = parametersDoc.object();

			int socketTimeoutMin = 50;
			int socketTimeoutMax = 20000;
			int socketTimeout = json.value(QLatin1String("TIMEOUT_SOCKET_CONNECT_MS")).toInt();
			if (socketTimeout >= socketTimeoutMin && socketTimeout <= socketTimeoutMax) {
				qDebug() << "[TCP]\tSuccessfully set TIMEOUT_SOCKET_CONNECT_MS by settings file from" << TIMEOUT_SOCKET_CONNECT_MS << "to" << socketTimeout;
				TIMEOUT_SOCKET_CONNECT_MS = socketTimeout;
			}
			int scanningTimeoutMin = 1;
			int scanningTimeoutMax = 50;
			int scanningTimeout = json.value(QLatin1String("TIMEOUT_SCANNING_PER_IP_S")).toInt();
			if (scanningTimeout >= scanningTimeoutMin && scanningTimeout <= scanningTimeoutMax) {
				qDebug() << "[TCP]\tSuccessfully set TIMEOUT_SCANNING_PER_IP_S by settings file from" << TIMEOUT_SCANNING_PER_IP_S << "to" << scanningTimeout;
				TIMEOUT_SCANNING_PER_IP_S = scanningTimeout;
			}
			int amountRetryMin = 1;
			int amountRetryMax = 10;
			int amountRetries = json.value(QLatin1String("AMOUNT_RETRIES")).toInt();
			if (amountRetries >= amountRetryMin && amountRetries <= amountRetryMax) {
				qDebug() << "[TCP]\tSuccessfully set AMOUNT_RETRIES by settings file from" << AMOUNT_RETRIES << "to" << amountRetries;
				AMOUNT_RETRIES = amountRetries;
			}
		}
	}

	stop_f=false;

	quint32 first_ip = QHostAddress(firstip).toIPv4Address();
	quint32 last_ip = QHostAddress(lastip).toIPv4Address();
	QVector<quint32> ipRange;
	for(quint32 i=first_ip;i<=last_ip;i++) ipRange.append(i);
	QMap<quint32, VicoDevice*> previousDevices = getPreviousDevices(first_ip, last_ip);
	QVector<VicoDevice*> reconnectedDevices = reconnectPreviousDevices(previousDevices);
	removeUnconnectedDevices(ipRange, previousDevices, reconnectedDevices);

	for(const quint32 ip : qAsConst(ipRange)) {
		if (stop_f==true){
			stop_f=false;
			break;
		}
		scan(ip, useBinding);
	}
	emit scanFinished();
}

void TcpScan::stop()
{
	stop_f=true;
}



