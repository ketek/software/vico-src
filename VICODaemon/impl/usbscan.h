#ifndef USBSCAN_H
#define USBSCAN_H

#include <QObject>
#include "DeviceRegistry.h"
#include "ftd2xx.h"
#include "LibFT4222.h"

/**
 * @brief The UsbScan class performs a scan for FT4222 USB-connected devices.
 */
class UsbScan : public QObject
{
	Q_OBJECT
public:
	explicit UsbScan(QObject *parent = nullptr);
	const QList<QString> getUsbDevices();
	const QList<QString> start();

private:
	QList<QString> usb_strings;
	void listFtUsbDevices();
	void createUsbDevice(QString dataSerNo, QString gpioSerNo);

signals:
	void scanFinished();
};

#endif // USBSCAN_H
