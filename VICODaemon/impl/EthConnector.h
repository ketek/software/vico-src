#ifndef ETHCONNECTOR_H
#define ETHCONNECTOR_H

#include <QObject>
#include <QTcpSocket>
#include <QDataStream>
#include <QtNetwork>
#include <QtCore>
#include "IConnector.h"
#include "types.h"

/**
 * @brief The EthConnector class creates or closes a connection to a device via TCP or UDP.
 */
class EthConnector : public QObject, public IConnector
{
	Q_OBJECT
	Q_INTERFACES(IConnector)
public:
	explicit EthConnector(QAbstractSocket* socket, QObject *parent = nullptr);
	~EthConnector();

	InterfaceType getConnectionType() const override;
	void initializeConnection() override;
	void closeConnection() override;
	QHostAddress getPeerAddress();

	QAbstractSocket* getSocket() const;

	static void closeConnection(QAbstractSocket *socket);

signals:
	void msgReceived(const QByteArray& msg) override;
	void disconnected();

private slots:
	void readMsg();

public slots:
	void sendMsg(const QByteArray& msg, const quint16 size, const int &timeout = DEFAULT_TIMEOUT) override;
	void shutdown();

private slots:
	void onSocketDisconnected();

private:
	QAbstractSocket* socket;
	InterfaceType connectionType;
	QDataStream in;
};

#endif // ETHCONNECTOR_H
